# Copyright: (C) 2010 RobotCub Consortium
# Author: Lorenzo Natale
# CopyPolicy: Released under the terms of the GNU GPL v2.0.

if (NOT ICUB_FOUND)

message(STATUS "Using iCub from install")

#set(ICUB_INCLUDE_DIRS "C:/Users/Kate/Desktop/icub-main-master/iCub/include;C:/Program Files/robotology/gsl-1.14/include" CACHE INTERNAL "Include directories needed for iCub")
set(ICUB_MODULE_PATH "C:/Users/Kate/Desktop/icub-main-master/iCub/share/iCub/cmake" CACHE INTERNAL "iCub cmake scripts directory")
set(ICUB_LIBRARIES "iCubDev;ctrlLib;skinDynLib;iKin;iDyn;learningMachine;perceptiveModels;actionPrimitives;optimization" CACHE INTERNAL "List of iCub libraries")
set(ICUB_LINK_FLAGS "/NODEFAULTLIB:libcmt.lib;libcmtd.lib" CACHE INTERNAL "List of iCub linker options")
set(ICUB_LINK_DIRECTORIES "C:/Program Files/robotology/opencv-2.4.9/x64/vc12/lib;C:/Program Files/robotology/opencv-2.4.9/x64/vc12/lib")
set(ICUB_INSTALL_PREFIX "C:/Users/Kate/Desktop/icub-main-master/iCub")

set(ICUB_PLUGIN_MANIFESTS_INSTALL_DIR "share/iCub/plugins")
set(ICUB_MODULES_INSTALL_DIR "share/iCub/modules")
set(ICUB_APPLICATIONS_INSTALL_DIR "share/iCub/applications")
set(ICUB_TEMPLATES_INSTALL_DIR "share/iCub/templates")
set(ICUB_APPLICATIONS_TEMPLATES_INSTALL_DIR "share/iCub/templates/applications")
set(ICUB_MODULES_TEMPLATES_INSTALL_DIR "share/iCub/templates/modules")
set(ICUB_CONTEXTS_INSTALL_DIR "share/iCub/contexts")

# Remove the old file if it exists. This is a temporary fix 
if(EXISTS "C:/Users/Kate/Desktop/icub-main-master/iCub/lib/ICUB/icub-export-install-includes.cmake")
  file(REMOVE "C:/Users/Kate/Desktop/icub-main-master/iCub/lib/ICUB/icub-export-install-includes.cmake")
endif()

include("C:/Users/Kate/Desktop/icub-main-master/iCub/lib/ICUB/icub-export-install.cmake")
include("C:/Users/Kate/Desktop/icub-main-master/iCub/lib/ICUB/icub-export-inst-includes.cmake")

# This is not ideal and should be removed. At the moment
# only needed by OpenCV
link_directories(${ICUB_LINK_DIRECTORIES})

set (ICUB_FOUND TRUE)
endif (NOT ICUB_FOUND)
