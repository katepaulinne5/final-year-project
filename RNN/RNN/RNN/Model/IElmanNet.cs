﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.Neural.Networks;

namespace RNN.Model
{
    public interface IElmanNet
    {
        void Train(ref List<float> errorTrain);
        //float Test();
        List<float> Test(IMLDataSet testingSet, ref List<double> actual,ref List<float> inputs);
        string ToString(bool def = true);
        NetType GetNetType();
        InputType GetInputType();
        void CalculateOutVal(string[] inputs, ref string[] outputs, bool staticInput);

        IMLData CalculateSingleOutVal(double val);

        BasicNetwork GetTrainedNet();

        string NetName
        {
            get;
            set;
        }

        TrainingParams TrainingParameters
        {
            set;
            get;
        }

        string TrainingDataPath
        {
            get;
        }

        
    }
}
