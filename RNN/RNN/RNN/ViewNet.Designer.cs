﻿namespace RNN
{
    partial class ViewNet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbNet = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbNet)).BeginInit();
            this.SuspendLayout();
            // 
            // pbNet
            // 
            this.pbNet.Location = new System.Drawing.Point(12, 13);
            this.pbNet.Name = "pbNet";
            this.pbNet.Size = new System.Drawing.Size(742, 681);
            this.pbNet.TabIndex = 0;
            this.pbNet.TabStop = false;
            this.pbNet.Paint += new System.Windows.Forms.PaintEventHandler(this.pbNet_Paint);
            this.pbNet.Resize += new System.EventHandler(this.pbNet_Resize);
            // 
            // ViewNet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 706);
            this.Controls.Add(this.pbNet);
            this.Name = "ViewNet";
            this.Text = "ViewNet";
            ((System.ComponentModel.ISupportInitialize)(this.pbNet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbNet;
    }
}