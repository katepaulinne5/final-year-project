﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNN.Model
{
    public enum LayerType
    {
        Input,
        Output,
        Hidden,
        Context
    }

    public enum WeightType
    {
        Input,
        Output
    }

    public enum NetType
    {
        ElmanNet,
        ElmanEncog
    }

    public enum InputType
    {
        Sequential,
        Static
    }

    public enum ExperimentType
    {
        Count_5,
        Count_10,
        Count_5_With_Gestures,
        Count_10_With_Gestures
    }
}