﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Encog.Engine.Network.Activation;
using Encog.ML;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.ML.Train;
using Encog.ML.Train.Strategy;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training;
using Encog.Neural.Networks.Training.Anneal;
using Encog.Neural.Networks.Training.Lma;
using Encog.Neural.Networks.Training.Propagation.Back;
using Encog.Neural.Pattern;
using SuperUtils = Encog.Util.NetworkUtil.NetworkUtility;
using SuperUtilsTrainer = Encog.Util.NetworkUtil.TrainerHelper;

namespace RNN.Model
{
    //public struct TrainingParamsEncog
    //{
    //    public int lambda;
    //    public float learningRate;
    //    public float trainingErrorVal;
    //    public float testingErrorVal;
    //    public float validationErrorVal;

    //    public TrainingParamsEncog(int l, float learnRate, float trErr, float tstErr, float validErr)
    //    {
    //        lambda = l;
    //        learningRate = learnRate;
    //        trainingErrorVal = trErr;
    //        testingErrorVal = tstErr;
    //        validationErrorVal = validErr;
    //    }
    //}

    public class ElmanNetEncog:IElmanNet
    {
        private List<Layer> layers = new List<Layer>();
        //private List<int> connections = new List<int>();
        //input -> hidden
        //hidden -> context
        //context -> hidden
        //hidden -> output
        private List<LayerConnection> connections = new List<LayerConnection>();
        private int layerNo;
        private Dictionary<WeightType, List<float>> weights = new Dictionary<WeightType, List<float>>();
        private List<List<float>> spoofInputData = new List<List<float>>();
        private List<List<float>> spoofTargets = new List<List<float>>();
        private List<float> terminationData = new List<float>();
        private string netName;
        private int hiddenNodesNo;
        private int maxIterations = 4000; //make it const later (?)
        private float trainingError = 0.0f;
        private TrainingParams trainingParameters;
        private float iterationError = 0.0f;
        //private TrainingParams trParams = new TrainingParams();

        private BasicNetwork net;
        private BasicNetwork net2;
        private IMLDataSet trainingSet;

        private string trainingDataPath;

        public const NetType netType=NetType.ElmanEncog;
        public InputType netInputType;
        //private static TrainTestNet frm;

        //public event EventHandler<ErrorUpdatedEventArgs> errorUpdated;

        public ElmanNetEncog(BasicNetwork _net, string name)
        {
            net = _net;
            netName = name;
        }

        public ElmanNetEncog(int hiddenNodes, string name, IMLDataSet data, string filePath,InputType inputType, int outputNodes = 1, int inputNodes=1)
        {
            //trainingSet = Generate(100);
            trainingSet = data;
            hiddenNodesNo = hiddenNodes;
            netName = name;
            netInputType = inputType;
            trainingDataPath = filePath;

            ElmanPattern elmanPattern = new ElmanPattern { ActivationFunction = new ActivationSigmoid(), InputNeurons = inputNodes };

            elmanPattern.AddHiddenLayer(hiddenNodesNo);
            elmanPattern.OutputNeurons = outputNodes;
            //elmanPattern.OutputNeurons = 10;
            IMLMethod elmanNet = elmanPattern.Generate();
            net = (BasicNetwork)elmanNet;
            //double error = Train(net, trainingSet);
            
            //Console.WriteLine(@"Neural Network Results:");
            //foreach (IMLDataPair pair in trainingSet)
            //{
               // IMLData output = net.Compute(pair.Input);
                //Console.WriteLine(pair.Input[0] + @", actual=" + output[0] + @",ideal=" + pair.Ideal[0]);
            //}

            //Console.WriteLine(error);

            BasicLayer input, hidden;
            net2 = new BasicNetwork();
            net2.AddLayer(input = new BasicLayer(1));
            net2.AddLayer(hidden = new BasicLayer(hiddenNodesNo));
            net2.AddLayer(new BasicLayer(1));
            input.ContextFedBy = hidden;
            net2.Structure.FinalizeStructure();
            net2.Reset();


        }

        public void Train(/*BasicNetwork network, IMLDataSet trainingSet,*/ref List<float> errorTrain)
        {
            ICalculateScore score = new TrainingSetScore(trainingSet);
            //experiment 1
            IMLTrain trainAlt = new NeuralSimulatedAnnealing(net, score, 512, 2, 20);

            //experiment2
            //IMLTrain trainAlt = new NeuralSimulatedAnnealing(net, score, 1334, 2, 20);

            IMLTrain trainMain;
            //if (Method.Equals("Leven"))
            //{
            //    Console.WriteLine("Using LevenbergMarquardtTraining");
            //    trainMain = new LevenbergMarquardtTraining(network, trainingSet);
            //}
            //else
            //trainMain = new Backpropagation(net, trainingSet, 0.00001, 0.0);//,0.0002,0.1);

            //experiment 1
            //trainMain = new Backpropagation(net, trainingSet, trainingParameters.learningRate, 0.0);
            trainMain = new Backpropagation(net, trainingSet, 0.0001, 0.0);

            //experiment 2
            //trainMain = new Backpropagation(net, trainingSet, 0.0000001, 0.0);

            var stop = new StopTrainingStrategy();
            trainMain.AddStrategy(new Greedy());
            trainMain.AddStrategy(new HybridStrategy(trainAlt));
            trainMain.AddStrategy(stop);
            System.IO.StreamWriter file = new System.IO.StreamWriter("D:/Debug/"+netName+"_TrainingLog"+DateTime.Now.ToString("ddMMyy")+".txt");
            System.IO.StreamWriter file2 = new System.IO.StreamWriter("D:/Debug/" + netName + "_TrainingLog2_" + DateTime.Now.ToString("ddMMyy") + ".txt");
            int epoch = 0;
            while (!stop.ShouldStop())
            //while (trainMain.Error>0.5 || epoch==0)
            {
                //experiment 1
                //trainMain.Iteration(10);
                //experiment 2
                trainMain.Iteration();
                //Console.WriteLine("Training  net, Epoch #" + epoch + " Error:" + trainMain.Error);
                //below cast needs testing

                file.WriteLine("Epoch: "+epoch.ToString()+", Training Error: "+trainMain.Error.ToString());

                errorTrain.Add((float)trainMain.Error);

                //ErrorUpdatedEventArgs errorUpdatedEA = new ErrorUpdatedEventArgs();
                //errorUpdatedEA.ErrorUpdate = errorTrain;
                //OnErrorUpdated(errorUpdatedEA);
                //GetSetError = errorTrain;

                epoch++;
            }

            //file.Close();

            trainMain.FinishTraining();
            //return trainMain.Error;

            foreach (IMLDataPair pair in trainingSet)
            {
                IMLData output = net.Compute(pair.Input);
                for (int i = 0; i < pair.Input.Count; i++)
                {
                    //file.Write(pair.Input[i].ToString());
                    for (int j = 0; j < pair.Ideal.Count; j++)
                    {
                        if (j == pair.Ideal.Count - 1)
                        {
                            file.WriteLine(pair.Input[i].ToString() + @", actual=" + output[j].ToString() + @",ideal=" + pair.Ideal[j].ToString());
                        }
                        else
                        {
                            file.Write(pair.Input[i].ToString() + @", actual=" + output[j].ToString() + @",ideal=" + pair.Ideal[j].ToString() + ", ");
                        }
                    }
                }

                for (int i = 0; i < pair.Input.Count; i++)
                {
                    //file.Write(pair.Input[i].ToString());
                    for (int j = 0; j < pair.Ideal.Count; j++)
                    {
                        if (j == pair.Ideal.Count - 1)
                        {
                            file2.WriteLine(pair.Input[i].ToString() + @", " + output[j].ToString() + @", " + pair.Ideal[j].ToString());
                        }
                        else
                        {
                            file2.WriteLine(pair.Input[i].ToString() + @", " + output[j].ToString() + @", " + pair.Ideal[j].ToString() + ", ");
                        }
                    }
                }
            }
            file.Close();
            file.Close();

        }

        //public virtual void OnErrorUpdated(ErrorUpdatedEventArgs e)
        //{
        //    if (errorUpdated != null)
        //    {
        //        errorUpdated(this, e);
        //    }
        //}

        public List<float> GetSetError
        {
            get;
            set;
        }

        public List<float> Test(IMLDataSet testingSet, ref List<double> actual, ref List<float> ideal)
        {

            List<float> errorList = new List<float>();
            List<float> sumActivHid = new List<float>();

            foreach (IMLDataPair pair in testingSet)
            {
                IMLData output = net.Compute(pair.Input);
                //Console.WriteLine(pair.Input[0] + @", actual=" + output[0] + @",ideal=" + pair.Ideal[0]);
                actual.Add(output[0]);
                ideal.Add((float)pair.Ideal[0]);
            }

            //calculate error on testing set
            float error=(float)net.CalculateError(testingSet);
            errorList.Add(error);
            return errorList;
        }


        public void CalculateOutVal(string[] inputs,ref string[] outputs, bool staticInput)
        {
            //BasicMLDataSet outVal=new BasicMLDataSet()
            //IMLDataPair pair = new IMLDataPair();
            if (staticInput)
            {
                double counter = 0.0;
                double[][] input = new double[1][];
                double[][] ideal = new double[1][];
                input[0] = new double[inputs.Length];
                ideal[0] = new double[inputs.Length];
                for (int i = 0; i < inputs.Length; i++)
                {

                    //if
                    double val = Convert.ToDouble(inputs[i]);
                    input[0][i] = val;
                    //if((i+1)%5==0)
                    if (val == 1)
                    {

                        counter++;
                    }
                    else
                    {
                        input[0][i] = 0;
                    }

                    ideal[0][0] = counter;
                    //net.Compute(input, ideal);
                    
                }
                BasicMLDataSet dataSet = new BasicMLDataSet(input, ideal);
                int j = 0;
                string outputsVal="";
                foreach (IMLDataPair pair in dataSet)
                {
                    IMLData output = net.Compute(pair.Input);
                    outputsVal= output.ToString();
                    j++;

                }
                outputs = outputsVal.Split(',');

            }
            else
            {
                double counter = 0.0;
                for (int i = 0; i < 10; i++)
                {
                    double[][] input = new double[1][];
                    double[][] ideal = new double[1][];
                    input[0] = new double[1];
                    ideal[0] = new double[1];

                    //if((i+1)%5==0)
                    if (i == 4 || i == 5)
                    {
                        input[0][0] = 1;
                        counter++;
                    }
                    else
                    {
                        input[0][0] = 0;
                    }

                    ideal[0][0] = counter;
                    //net.Compute(input, ideal);
                    BasicMLDataSet dataSet = new BasicMLDataSet(input, ideal);
                    int j = 0;
                    foreach (IMLDataPair pair in dataSet)
                    {
                        IMLData output = net.Compute(pair.Input);
                        outputs[i] = output.ToString();
                        j++;

                    }
                }
            }
            
            //net.Compute()
        }

        public IMLData CalculateSingleOutVal(double val)
        {
            //BasicMLDataSet outVal=new BasicMLDataSet()
            //IMLDataPair pair = new IMLDataPair();
            double counter = 0.0;
            IMLData output=null;

                double[][] input = new double[1][];
                double[][] ideal = new double[1][];
                input[0] = new double[1];
                ideal[0] = new double[1];

                input[0][0] = val;
                ideal[0][0] = 0;
                //net.Compute(input, ideal);
                BasicMLDataSet dataSet = new BasicMLDataSet(input, ideal);
                foreach (IMLDataPair pair in dataSet)
                {
                    output = net.Compute(pair.Input);
                }
                net.Compute(input[0], ideal[0]);
            return output;
        }

        private void CreateData()
        {
            for (int i = 0; i < 3; i++)
            {
                List<float> vals = new List<float>();
                for (int j = 0; j < 6; j++)
                {
                    //if ((i == 0 && j == 0) || (i == 1 && j == 3) || (i == 2 && j == 5) || (i == 3 && j == 2) || (i == 4 && j == 0))
                    if ((i == 0 && j == 3) || (i == 1 && j == 5) || (i == 2 && j == 2))
                    {
                        vals.Add(1.0f);
                    }
                    else
                    {
                        vals.Add(0.0f);
                    }
                }
                spoofInputData.Add(vals);
                spoofTargets.Add(vals);
                //vals.Clear();
            }


            terminationData.Add(1.0f);
            for (int i = 1; i < 6; i++)
            {
                terminationData.Add(0.0f);
            }
        }

        public NetType GetNetType()
        {
            return netType;
        }

        public InputType GetInputType()
        {
            return netInputType;
        }
        public string GetNetName()
        {
            return netName;
        }

        public BasicNetwork GetTrainedNet()
        {
            return net;
        }

        public string NetName
        {
            //remove after tested!
            set { netName = value; }
            get { return netName; }
        }

        public TrainingParams TrainingParameters
        {
            set { trainingParameters = value; }
            get { return trainingParameters; }
        }

        public string TrainingDataPath
        {
            //set { trainingDataPath = value; }
            get { return trainingDataPath; }
        }

        //needs changing
        public string ToString(bool def = true)
        {
            
            if (def)
            {
                return "Neural Network - " + netName;
            }
            else
            {
                string netDescription = "";

                netDescription += "Neural Network - " + netName + ":" + Environment.NewLine + Environment.NewLine;
                netDescription += "Layers: " + Environment.NewLine + Environment.NewLine;

                for (int i = 0; i < net.LayerCount; i++)
                {
                    //netDescription += layers.ElementAt(i).LayerName + ":" + Environment.NewLine;
                    //netDescription += "Number of nodes: " + layers.ElementAt(i).GetNodes.Count + Environment.NewLine;
                    //netDescription += "Weights/Connections to this layer (per node): " + layers.ElementAt(i).GetNodes.ElementAt(0).GetWeights.Count + Environment.NewLine + Environment.NewLine;
                    //netDescription += net.Structure.Layers.ElementAt(i).ToString();            
                }
                return netDescription;
            }
        }
    }
}
