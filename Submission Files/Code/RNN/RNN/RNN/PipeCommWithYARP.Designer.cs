﻿namespace RNN
{
    partial class PipeCommWithYARP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartPipeServer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMsg = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnReceive = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnTogglePipeComm = new System.Windows.Forms.Button();
            this.lblState = new System.Windows.Forms.Label();
            this.btnGetNetOutput = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbCommToolbox = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbExperiments = new System.Windows.Forms.ComboBox();
            this.rbtnSeq = new System.Windows.Forms.RadioButton();
            this.rbtnStatic = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.gbCommToolbox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStartPipeServer
            // 
            this.btnStartPipeServer.Location = new System.Drawing.Point(53, 76);
            this.btnStartPipeServer.Name = "btnStartPipeServer";
            this.btnStartPipeServer.Size = new System.Drawing.Size(129, 33);
            this.btnStartPipeServer.TabIndex = 0;
            this.btnStartPipeServer.Text = "Start Server";
            this.btnStartPipeServer.UseVisualStyleBackColor = true;
            this.btnStartPipeServer.Click += new System.EventHandler(this.btnStartPipeServer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Network output:";
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(11, 224);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(37, 17);
            this.lblMsg.TabIndex = 2;
            this.lblMsg.Text = "msg";
            // 
            // txtMsg
            // 
            this.txtMsg.Location = new System.Drawing.Point(139, 183);
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(50, 22);
            this.txtMsg.TabIndex = 3;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(197, 2);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 4;
            this.btnSend.Text = "send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Visible = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnReceive
            // 
            this.btnReceive.Location = new System.Drawing.Point(105, 2);
            this.btnReceive.Name = "btnReceive";
            this.btnReceive.Size = new System.Drawing.Size(75, 23);
            this.btnReceive.TabIndex = 5;
            this.btnReceive.Text = "receive";
            this.btnReceive.UseVisualStyleBackColor = true;
            this.btnReceive.Visible = false;
            this.btnReceive.Click += new System.EventHandler(this.btnReceive_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(202, 76);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(138, 33);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Stop Server";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnTogglePipeComm
            // 
            this.btnTogglePipeComm.Location = new System.Drawing.Point(205, 178);
            this.btnTogglePipeComm.Name = "btnTogglePipeComm";
            this.btnTogglePipeComm.Size = new System.Drawing.Size(135, 33);
            this.btnTogglePipeComm.TabIndex = 7;
            this.btnTogglePipeComm.Text = "Send Command";
            this.btnTogglePipeComm.UseVisualStyleBackColor = true;
            this.btnTogglePipeComm.Click += new System.EventHandler(this.btnTogglePipeComm_Click);
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblState.Location = new System.Drawing.Point(11, 33);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(249, 17);
            this.lblState.TabIndex = 8;
            this.lblState.Text = "Pipe Communication Not Running";
            // 
            // btnGetNetOutput
            // 
            this.btnGetNetOutput.Location = new System.Drawing.Point(205, 217);
            this.btnGetNetOutput.Name = "btnGetNetOutput";
            this.btnGetNetOutput.Size = new System.Drawing.Size(135, 33);
            this.btnGetNetOutput.TabIndex = 9;
            this.btnGetNetOutput.Text = "Stop Experiment";
            this.btnGetNetOutput.UseVisualStyleBackColor = true;
            this.btnGetNetOutput.Click += new System.EventHandler(this.btnGetNetOutput_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnStartPipeServer);
            this.groupBox1.Controls.Add(this.btnStop);
            this.groupBox1.Controls.Add(this.lblState);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 131);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server Toolbox";
            // 
            // gbCommToolbox
            // 
            this.gbCommToolbox.Controls.Add(this.groupBox2);
            this.gbCommToolbox.Controls.Add(this.btnTogglePipeComm);
            this.gbCommToolbox.Controls.Add(this.btnGetNetOutput);
            this.gbCommToolbox.Controls.Add(this.txtMsg);
            this.gbCommToolbox.Controls.Add(this.label1);
            this.gbCommToolbox.Controls.Add(this.lblMsg);
            this.gbCommToolbox.Enabled = false;
            this.gbCommToolbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCommToolbox.Location = new System.Drawing.Point(12, 188);
            this.gbCommToolbox.Name = "gbCommToolbox";
            this.gbCommToolbox.Size = new System.Drawing.Size(346, 256);
            this.gbCommToolbox.TabIndex = 11;
            this.gbCommToolbox.TabStop = false;
            this.gbCommToolbox.Text = "Pipe Communication Toolbox";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbExperiments);
            this.groupBox2.Controls.Add(this.rbtnSeq);
            this.groupBox2.Controls.Add(this.rbtnStatic);
            this.groupBox2.Location = new System.Drawing.Point(14, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 132);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Simulation Toolbox";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Experiment:";
            // 
            // cbExperiments
            // 
            this.cbExperiments.FormattingEnabled = true;
            this.cbExperiments.Location = new System.Drawing.Point(125, 80);
            this.cbExperiments.Name = "cbExperiments";
            this.cbExperiments.Size = new System.Drawing.Size(184, 24);
            this.cbExperiments.TabIndex = 12;
            this.cbExperiments.SelectedIndexChanged += new System.EventHandler(this.cbExperiment_SelectedIndexChanged);
            this.cbExperiments.DropDownClosed += new System.EventHandler(this.experiments_DropDownClosed);
            // 
            // rbtnSeq
            // 
            this.rbtnSeq.AutoSize = true;
            this.rbtnSeq.Checked = true;
            this.rbtnSeq.Location = new System.Drawing.Point(6, 31);
            this.rbtnSeq.Name = "rbtnSeq";
            this.rbtnSeq.Size = new System.Drawing.Size(147, 21);
            this.rbtnSeq.TabIndex = 10;
            this.rbtnSeq.TabStop = true;
            this.rbtnSeq.Text = "Sequential Input";
            this.rbtnSeq.UseVisualStyleBackColor = true;
            // 
            // rbtnStatic
            // 
            this.rbtnStatic.AutoSize = true;
            this.rbtnStatic.Location = new System.Drawing.Point(171, 31);
            this.rbtnStatic.Name = "rbtnStatic";
            this.rbtnStatic.Size = new System.Drawing.Size(111, 21);
            this.rbtnStatic.TabIndex = 11;
            this.rbtnStatic.Text = "Static Input";
            this.rbtnStatic.UseVisualStyleBackColor = true;
            // 
            // PipeCommWithYARP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 460);
            this.Controls.Add(this.gbCommToolbox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnReceive);
            this.Controls.Add(this.btnSend);
            this.Name = "PipeCommWithYARP";
            this.Text = "PipeCommWithYARP";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbCommToolbox.ResumeLayout(false);
            this.gbCommToolbox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStartPipeServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnReceive;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnTogglePipeComm;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Button btnGetNetOutput;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbCommToolbox;
        private System.Windows.Forms.RadioButton rbtnStatic;
        private System.Windows.Forms.RadioButton rbtnSeq;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbExperiments;
    }
}