﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RNN.Model;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using System.IO;
using Encog.Neural.Networks;
//using Microsoft.Win32;

namespace RNN
{

    public partial class Menu : Form
    {
        List<ElmanNet> networksTMP = new List<ElmanNet>();
        List<IElmanNet> iNetworksTMP = new List<IElmanNet>();
        //used to highlighed nets with green/red when test error is below threshold
        private Dictionary<bool, List<ElmanNet>> testResultsFlag = new Dictionary<bool, List<ElmanNet>>();
        private ElmanNet selectedNet;
        private IElmanNet iSelectedNet;
        //public ObservableCollection<ElmanNet> observableNets=new ObservableCollection<ElmanNet>();

        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern bool LockWindowUpdate(IntPtr hWndLock);

        private const string YARP_Proc = "yarpserver";
        private const string ICub_Proc = "iCub_SIM";
        private const string YarpRobotInterface_Proc = "yarprobotinterface";
        private const string iKinCartesianSolver_Proc="iKinCartesianSolver";


        private bool disableCoulours = true;
        private bool isCheckedCol = false;

        public Menu()
        {

            InitializeComponent();
            rbColourToggle.Checked = false;
            SimulatorPathManager.RetrieveFromRegistry();
            //string[] strArray=Registry.CurrentUser.GetSubKeyNames();
            //strArray = Registry.CurrentUser.OpenSubKey("SOFTWARE").GetSubKeyNames();

            if(disableCoulours)
            {
                btnDelete.BackColor = Color.Silver;
            }

            CheckProcessesRunning(YARP_Proc, lblYarpServer,btnManageYarp);
            CheckProcessesRunning(ICub_Proc, lblICubSim,btnManageICub);
            CheckProcessesRunning(YarpRobotInterface_Proc, lblYARPRobotInterface, btnManageYARPRobotInterface);
            CheckProcessesRunning(iKinCartesianSolver_Proc, lblIKinCartesianSolver);
            //lblYarpServer.Size.Width = 219;

            rtxtNetDetails.ReadOnly = true;
            //ContextMenu blankContextMenu = new ContextMenu();
            //txtNetDetails.ContextMenu = blankContextMenu;
            //txtNetDetails.Cursor = DefaultBackColor;

            //networksTMP.Add(1);
            //networksTMP.Add(2);
            //networksTMP.Add(3);

            for(int i=0;i<networksTMP.Count;i++)
            {
                //clbNetworks.Items.Add(networksTMP.ElementAt(i));
            }

            List<float> valsTmp = new List<float>();
            valsTmp.Add(0.5f);
            valsTmp.Add(0.3f);
            valsTmp.Add(0.8f);
            List<Layer> layersInput = new List<Layer>();
            Layer l1 = new Layer(2, "Input");
            layersInput.Add(l1); 
            //test network components
            Layer l = new Layer(10,"Hidden",valsTmp,layersInput);
            //l.getActivationForNodes();
            ElmanNet net = new ElmanNet(4,10,"Elman Net 1");
            networksTMP.Add(net);
            iNetworksTMP.Add(net);
            ElmanNet net1 = new ElmanNet(4, 10, "Elman Net 2");
            networksTMP.Add(net1);
            iNetworksTMP.Add(net1);
            for (int i = 0; i < networksTMP.Count; i++)
            {
                //observableNets.Add(networksTMP.ElementAt(i));
                clbNetworks.Items.Add(networksTMP.ElementAt(i).ToString());
            }
            //net.Train();
        }

        private bool CheckProcessesRunning(string processName, Label lbl, Button btn=null)
        {
            bool found = false;
            if (!disableCoulours)
            {
                lbl.BackColor = Color.Red;
                lbl.ForeColor = Color.White;
            }
            else
            {
                lbl.BackColor = SystemColors.Control;
                lbl.ForeColor = Color.Black;
            }
            foreach(Process runningProcesses in Process.GetProcesses())
            {
                if(runningProcesses.ProcessName.Contains(processName))
                {
                    if (!disableCoulours)
                    {
                        lbl.BackColor = Color.Green;
                        lbl.ForeColor = Color.White;
                    }
                    else
                    {
                        lbl.BackColor = SystemColors.Control;
                        lbl.ForeColor = Color.Black;
                    }
                    lbl.Text=lbl.Text.Replace("Not Running", "Running");
                    found = true;
                    if(btn!=null)
                    {
                        btn.Text = "Stop";
                    }
                }
            }

            if(!found && !lbl.Text.Contains("Not")/*&& lbl.BackColor!=Color.Red*/)
            {
                if (!disableCoulours)
                {
                    lbl.BackColor = Color.Red;
                    lbl.ForeColor = Color.White;
                }
                else
                {
                    lbl.BackColor = SystemColors.Control;
                    lbl.ForeColor = Color.Black;
                }
                lbl.Text=lbl.Text.Replace("Running", "Not Running");
                if (btn != null)
                {
                    btn.Text = "Start";
                }
            }

            return found;
        }

        private void clbNetworks_MouseClick(object sender, MouseEventArgs e)
        {  
            for (int i = 0; i < clbNetworks.Items.Count; i++)
            {
                if(clbNetworks.GetItemCheckState(i)==CheckState.Checked)
                {
                    //this should be gotten from control rather than list - change later
                    //displayNetworkDetails(networksTMP.ElementAt(i));
                    //iNetworksTMP.ElementAt(i).ToString();
                    displayNetworkDetails(iNetworksTMP.ElementAt(i));
                }


            }
        }

        private void displayNetworkDetails(ElmanNet net)
        {
            LockWindowUpdate(rtxtNetDetails.Handle);
            rtxtNetDetails.Clear();
            //txtNetDetails.AppendText("Neural Network - " + net.NetName +":"+ Environment.NewLine);
            rtxtNetDetails.AppendText(net.ToString(false));
            rtxtNetDetails.SelectionStart = 0;
            rtxtNetDetails.SelectionLength = 1;
            rtxtNetDetails.ScrollToCaret();
            LockWindowUpdate(IntPtr.Zero);
        }

        private void displayNetworkDetails(IElmanNet net)
        {
            LockWindowUpdate(rtxtNetDetails.Handle);
            rtxtNetDetails.Clear();
            //txtNetDetails.AppendText("Neural Network - " + net.NetName +":"+ Environment.NewLine);
            rtxtNetDetails.AppendText(net.ToString(false));
            rtxtNetDetails.SelectionStart = 0;
            rtxtNetDetails.SelectionLength = 1;
            rtxtNetDetails.ScrollToCaret();
            LockWindowUpdate(IntPtr.Zero);
        }

        private void clbNetworks_Click(object sender, EventArgs e)
        {
            bool oneSelected = false;
            //needs a check to disable multiselection
            for (int i = 0; i < clbNetworks.Items.Count; i++)
            {
                clbNetworks.SetItemCheckState(i, CheckState.Unchecked);
                if (clbNetworks.GetItemRectangle(i).Contains(clbNetworks.PointToClient(MousePosition)))
                {
                    switch (clbNetworks.GetItemCheckState(i))
                    {
                        case CheckState.Checked:
                            clbNetworks.SetItemCheckState(i, CheckState.Unchecked);
                            break;
                        case CheckState.Indeterminate:
                        case CheckState.Unchecked:
                            clbNetworks.SetItemCheckState(i, CheckState.Checked);
                            //selectedNet = networksTMP.ElementAt(i);
                            iSelectedNet = iNetworksTMP.ElementAt(i);
                            pbNet.Refresh();
                            break;
                    }

                }

                if(clbNetworks.GetItemCheckState(i)==CheckState.Checked)
                {
                    oneSelected = true;
                }

            }

            if(!oneSelected)
            {
                selectedNet = null;
            }
        }

        private void rtxtNetDetails_SelectionChanged(object sender, EventArgs e)
        {
            rtxtNetDetails.SelectionLength = 0;
            //sender.Handled = true;
        }

        private void rtxtNetDetails_MouseClick(object sender, MouseEventArgs e)
        {
            HideCaret(rtxtNetDetails.Handle);
        }

        private void tsmCreateNewNet_Click(object sender, EventArgs e)
        {
            using (CreateNewNet frmCreateNet = new CreateNewNet())
            {
                if (frmCreateNet.ShowDialog() == DialogResult.OK)
                {
                    string netName = frmCreateNet.NetName;
                    int nodesNo = frmCreateNet.HiddenNodesNo;
                    NetType networkType = frmCreateNet.NetworkType;
                    ElmanNet netTmp;
                    ElmanNetEncog netTmpEncog;
                    if (networkType == NetType.ElmanNet)
                    {
                        netTmp = new ElmanNet(4, nodesNo, netName, frmCreateNet.Inputs, frmCreateNet.Outputs);
                        clbNetworks.Items.Add(netTmp.ToString());
                        networksTMP.Add(netTmp);
                        iNetworksTMP.Add(netTmp);
                    }
                    else
                    {
                        IMLDataSet dataSet;
                        if (frmCreateNet.InputType == InputType.Sequential)
                        {
                            dataSet = Generate(100, frmCreateNet.FilePath);
                            netTmpEncog = new ElmanNetEncog(nodesNo, netName, dataSet, frmCreateNet.FilePath,InputType.Sequential, 1, 1);
                        }
                        else
                        {
                            if (frmCreateNet.ContSeq)
                            {
                                dataSet = Generate(100, frmCreateNet.FilePath, frmCreateNet.InputNodesNo, frmCreateNet.OutputNodesNo, frmCreateNet.ContSeq);
                            }
                            else
                            {
                                dataSet = Generate(100, frmCreateNet.FilePath, frmCreateNet.InputNodesNo, frmCreateNet.OutputNodesNo);
                            }
                            netTmpEncog = new ElmanNetEncog(nodesNo, netName, dataSet, frmCreateNet.FilePath, InputType.Static, frmCreateNet.OutputNodesNo, frmCreateNet.InputNodesNo);
                        }
                        clbNetworks.Items.Add(netTmpEncog.ToString());
                        //networksTMP.Add(netTmpEncog);
                        iNetworksTMP.Add(netTmpEncog);
                    }
                    
                    //observableNets.Add(netTmp);
                }
            }
        }

        private void tsmSaveNet_Click(object sender, EventArgs e)
        {
            if (iSelectedNet != null)
            {
                if (iSelectedNet.GetNetType() == NetType.ElmanEncog)
                {
                    FileInfo netFile = new FileInfo("D:\\Debug\\ElmanNet_" + iSelectedNet.NetName);
                    Encog.Persist.EncogDirectoryPersistence.SaveObject(netFile, (BasicNetwork)iSelectedNet.GetTrainedNet());
                }
            }
        }

        private void tsmLoadNet_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadNetDialog = new OpenFileDialog();
            //loadNetDialog.Filter = "*.eg";
            loadNetDialog.Title = "Select Encog network file";
            loadNetDialog.RestoreDirectory = true;
            
            if(loadNetDialog.ShowDialog()==DialogResult.OK)
            {
                BasicNetwork network;
                string nameFile=loadNetDialog.SafeFileName;
                loadNetDialog.OpenFile();
                string directoryPath = Path.GetDirectoryName(nameFile+".*");
                nameFile = loadNetDialog.FileName;
                FileInfo netFile = new FileInfo(nameFile);
                network = (BasicNetwork)(Encog.Persist.EncogDirectoryPersistence.LoadObject(netFile));
                ElmanNetEncog netTmp = new ElmanNetEncog(network, loadNetDialog.SafeFileName.Substring(loadNetDialog.SafeFileName.LastIndexOf('_') + 1));
                clbNetworks.Items.Add(netTmp.ToString());
                iNetworksTMP.Add(netTmp);
            }
        }

        private void tsmRunWithYARP_Click(object sender, EventArgs e)
        {
            using (PipeCommWithYARP frmPipeComm = new PipeCommWithYARP(iSelectedNet))
            {
                frmPipeComm.ShowDialog();
                //selectedNet=clbNetworks.Item
                //frmTrainTestNet.netUpdated += new EventHandler<NetUpdatedEventArgs>(_netUpdated);
                //if (frmPipeComm.ShowDialog() == DialogResult.OK)
                //{
                    //get returned values
                //}
            }
        }

        private void tsmManageAppPaths_Click(object sender, EventArgs e)
        {
            SimPathManager frmManagePaths = new SimPathManager();
            frmManagePaths.Show();
        }

        private void btnManageNet_Click(object sender, EventArgs e)
        {
            if (iSelectedNet != null)
            {
                using (ManageNet frmManageNet = new ManageNet(iSelectedNet))
                {
                    //selectedNet=clbNetworks.Item
                    //frmTrainTestNet.netUpdated += new EventHandler<NetUpdatedEventArgs>(_netUpdated);
                    frmManageNet.netUpdated += new EventHandler<EncogNetUpdatedEventArgs>(_encogNetUpdated);
                    if (frmManageNet.ShowDialog() == DialogResult.OK)
                    {
                        //string netName = frmCreateNet.NetName;
                        //int nodesNo = frmCreateNet.HiddenNodesNo;
                        //ElmanNet netTmp = new ElmanNet(4, nodesNo, netName);
                        //clbNetworks.Items.Add(netTmp.ToString());
                        //networksTMP.Add(netTmp);
                    }
                }
            }
            else
            {
                //display tooltip saying to select the net
                ttInfo.Show("A network must be selected from the list", btnManageNet, 3000);
            }
        }

        private void btnTrainTestNet_Click(object sender, EventArgs e)
        {
            if (iSelectedNet != null)
            {
                //using (TrainTestNet frmTrainTestNet = new TrainTestNet(selectedNet))
                IMLDataSet dataSet = Generate(100, "D:\\Debug\\NetTrainData5x10x10.csv");
                using (TrainTestNet frmTrainTestNet = new TrainTestNet(iSelectedNet, dataSet))
                {
                    //selectedNet=clbNetworks.Item
                    //frmTrainTestNet.netUpdated += new EventHandler<NetUpdatedEventArgs>(_netUpdated);
                    frmTrainTestNet.encogNetUpdated += new EventHandler<EncogNetUpdatedEventArgs>(_encogNetUpdated);
                    if (frmTrainTestNet.ShowDialog() == DialogResult.OK)
                    {
                        //string netName = frmCreateNet.NetName;
                        //int nodesNo = frmCreateNet.HiddenNodesNo;
                        //ElmanNet netTmp = new ElmanNet(4, nodesNo, netName);
                        //clbNetworks.Items.Add(netTmp.ToString());
                        //networksTMP.Add(netTmp);
                    }
                }
            }
            else
            {
                //display tooltip saying to select the net
                ttInfo.Show("A network must be selected from the list", btnTrainTestNet, 3000);
            }
        }

        private void _netUpdated(object sender, NetUpdatedEventArgs e)
        {
            if(e!=null && e.Net!=null)
            {
                //selected net
                networksTMP[networksTMP.IndexOf(selectedNet)] = e.Net;
                LockWindowUpdate(clbNetworks.Handle);
                clbNetworks.Items.Clear();
                for (int i = 0; i < networksTMP.Count; i++)
                {
                    clbNetworks.Items.Add(networksTMP.ElementAt(i).ToString());
                }
                clbNetworks.SetItemCheckState(networksTMP.IndexOf(selectedNet), CheckState.Checked);

                //selected net
                //iNetworksTMP[iNetworksTMP.IndexOf(selectedNet)] = e.Net;
                //LockWindowUpdate(clbNetworks.Handle);
                //clbNetworks.Items.Clear();
                //for (int i = 0; i < iNetworksTMP.Count; i++)
                //{
                //    clbNetworks.Items.Add(iNetworksTMP.ElementAt(i).ToString());
                //}
                //clbNetworks.SetItemCheckState(iNetworksTMP.IndexOf(selectedNet), CheckState.Checked);

                LockWindowUpdate(IntPtr.Zero);
            }
        }

        private void _encogNetUpdated(object sender, EncogNetUpdatedEventArgs e)
        {
            if (e != null && e.Net != null)
            {
                //selected net
                //networksTMP[networksTMP.IndexOf(selectedNet)] = e.Net;
                //LockWindowUpdate(clbNetworks.Handle);
                //clbNetworks.Items.Clear();
                //for (int i = 0; i < networksTMP.Count; i++)
                //{
                //    clbNetworks.Items.Add(networksTMP.ElementAt(i).ToString());
                //}
                //clbNetworks.SetItemCheckState(networksTMP.IndexOf(selectedNet), CheckState.Checked);

                //selected net
                iNetworksTMP[iNetworksTMP.IndexOf(iSelectedNet)] = e.Net;
                LockWindowUpdate(clbNetworks.Handle);
                clbNetworks.Items.Clear();
                for (int i = 0; i < iNetworksTMP.Count; i++)
                {
                    clbNetworks.Items.Add(iNetworksTMP.ElementAt(i).ToString());
                }
                clbNetworks.SetItemCheckState(iNetworksTMP.IndexOf(iSelectedNet), CheckState.Checked);

                LockWindowUpdate(IntPtr.Zero);
            }
        }

        private void btnCheckState_Click(object sender, EventArgs e)
        {
            CheckProcessesRunning(YARP_Proc, lblYarpServer, btnManageYarp);
            CheckProcessesRunning(ICub_Proc, lblICubSim, btnManageICub);
            CheckProcessesRunning(YarpRobotInterface_Proc, lblYARPRobotInterface,btnManageYARPRobotInterface);
        }


        private void btnViewNet_Click(object sender, EventArgs e)
        {
            if (selectedNet != null)
            {
                using (ViewNet frmViewNet = new ViewNet(selectedNet))
                {
                    //selectedNet=clbNetworks.Item
                    //frmTrainTestNet.netUpdated += new EventHandler<NetUpdatedEventArgs>(_netUpdated);
                    if (frmViewNet.ShowDialog() == DialogResult.OK)
                    {
                        //string netName = frmCreateNet.NetName;
                        //int nodesNo = frmCreateNet.HiddenNodesNo;
                        //ElmanNet netTmp = new ElmanNet(4, nodesNo, netName);
                        //clbNetworks.Items.Add(netTmp.ToString());
                        //networksTMP.Add(netTmp);
                    }
                }
            }
            else
            {
                //display tooltip saying to select the net
                ttInfo.Show("A network must be selected from the list", btnViewNet, 3000);
            }
        }    

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (iSelectedNet != null)
            {
                DialogResult confirmResult = MessageBox.Show("Are you sure you want to delete: " + iSelectedNet.NetName+"?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (confirmResult == DialogResult.Yes)
                {
                    //networksTMP.RemoveAt(networksTMP.IndexOf(selectedNet));
                    //LockWindowUpdate(clbNetworks.Handle);
                    //clbNetworks.Items.Clear();
                    //for (int i = 0; i < networksTMP.Count; i++)
                    //{
                    //    clbNetworks.Items.Add(networksTMP.ElementAt(i).ToString());
                    //}
                    clbNetworks.SetItemCheckState(iNetworksTMP.IndexOf(iSelectedNet), CheckState.Checked);

                    iNetworksTMP.RemoveAt(iNetworksTMP.IndexOf(iSelectedNet));
                    LockWindowUpdate(clbNetworks.Handle);
                    clbNetworks.Items.Clear();
                    for (int i = 0; i < iNetworksTMP.Count; i++)
                    {
                        clbNetworks.Items.Add(iNetworksTMP.ElementAt(i).ToString());
                    }
                    
                    LockWindowUpdate(IntPtr.Zero);
                }
            }
            else
            {
                //display tooltip asking to select network to delete from the list
                ttInfo.Show("A network must be selected from the list", btnDelete,3000);
            }
        }

        private void btnManageYarp_Click(object sender, EventArgs e)
        {
            SetButtonToggleText(btnManageYarp, YARP_Proc, lblYarpServer);
            CheckProcessesRunning(YARP_Proc, lblYarpServer);
        }

        private void btnManageICub_Click(object sender, EventArgs e)
        {
            SetButtonToggleText(btnManageICub, ICub_Proc, lblICubSim);
            CheckProcessesRunning(ICub_Proc, lblICubSim);
        }

        private void btnManageYARPRobotInterface_Click(object sender, EventArgs e)
        {
            SetButtonToggleText(btnManageYARPRobotInterface, YarpRobotInterface_Proc, lblYARPRobotInterface);
            CheckProcessesRunning(YarpRobotInterface_Proc, lblYARPRobotInterface);
        }

        private void btnManageCartesianSolver_Click(object sender, EventArgs e)
        {
            SetButtonToggleText(btnManageCartesianSolver, iKinCartesianSolver_Proc, lblIKinCartesianSolver);
            CheckProcessesRunning(iKinCartesianSolver_Proc, lblIKinCartesianSolver);
        }

        private void SetButtonToggleText(Button btn, string _proc, Label _lbl)
        {
            if (CheckProcessesRunning(_proc, _lbl))
            {
                //stop process
                btn.Text = "Start";
                foreach(Process proc in Process.GetProcesses())
                {
                    if(proc.ProcessName.Contains(_proc))
                    {
                        proc.Kill();
                        proc.WaitForExit();
                    }
                }
                //CheckProcessesRunning(_proc, _lbl);
            }
            else
            {
                //start process
                btn.Text = "Stop";
                ProcessStartInfo startInfo = new ProcessStartInfo();
                //if (_proc.Contains("yarp"))
                //{
                //    startInfo.FileName = SimulatorPathManager.GetSetPath_YARP;
                //    startInfo.Arguments = SimulatorPathManager.GetSetLineArgs_YARP;
                //}
                //else
                //{
                //    startInfo.FileName = SimulatorPathManager.GetSetPath_ICub;
                //}

                switch(_proc)
                {
                    case YARP_Proc:
                        startInfo.FileName = SimulatorPathManager.GetSetPath_YARP;
                        startInfo.Arguments = SimulatorPathManager.GetSetLineArgs_YARP;
                        break;
                    case ICub_Proc:
                        startInfo.FileName = SimulatorPathManager.GetSetPath_ICub;
                        break;
                    case YarpRobotInterface_Proc:
                        startInfo.FileName = SimulatorPathManager.GetSetPath_YarpRobotInterface;
                        startInfo.Arguments = SimulatorPathManager.GetSetLineArgs_YarpRobotInterface;
                        break;
                    case iKinCartesianSolver_Proc:
                        startInfo.FileName = SimulatorPathManager.GetSetPath_IKinCartesianSolver;
                        startInfo.Arguments = SimulatorPathManager.GetSetLineArgs_IKinCartesianSolver;
                        break;
                }
                Process.Start(startInfo);
               //CheckProcessesRunning(_proc, _lbl);
            }
            
        }

        private void DrawNet(PaintEventArgs e)
        {
            //var graph = new AdjacencyGraph<int, TaggedEdge<int, string>>();
            Dictionary<LayerType, List<Node>> graphData = new Dictionary<LayerType, List<Node>>();

            if(iSelectedNet.GetNetType()==NetType.ElmanNet)
            {
                selectedNet = (ElmanNet)iSelectedNet;            }
            else

            {
                selectedNet = null;
            }
            graphData = selectedNet.getDataForDrawing();
            int counter = 0;
            List<Point> trackNodes = new List<Point>();
            List<Point> prevNodes = new List<Point>();
            Point p1 = new Point();
            Point p2 = new Point();
            Point p3 = new Point();
            Point p4 = new Point();
            int offset = 0;
            foreach (KeyValuePair<LayerType, List<Node>> keyPair in graphData)
            {

                int layerRectSizeX = 22;
                int layerRectSizeY = 21;
                Point startL = new Point();
                int y = 20;
                int x = 40 + 200 * counter;
                if (keyPair.Key == LayerType.Context)
                {
                    y = 20 + 30 * 6 + 30;
                    x = 40;
                    counter--;
                }
                else if (trackNodes.Count > 0)
                {
                    prevNodes = trackNodes.ToList();
                    trackNodes.Clear();
                }
                if (keyPair.Key != LayerType.Input && keyPair.Key != LayerType.Context)
                {
                    offset += 60;
                    y += offset;
                }
                else
                {
                    offset = 0;
                }

                for (int i = 0; i < keyPair.Value.Count; i++)
                {
                    //graph.AddVertex(i);
                    DrawCircle(x, y, e);


                    if (keyPair.Key != LayerType.Context && keyPair.Key != LayerType.Input)
                    {
                        for (int j = 0; j < prevNodes.Count; j++)
                        {
                            DrawLine(prevNodes.ElementAt(j), new Point(x, y + 5), e);
                        }
                        if (keyPair.Key == LayerType.Hidden && i == keyPair.Value.Count - 1)
                        {
                            p1.X = x + 5;
                            p1.Y = y + 15;//y + 15+9*30;
                            p2.X = p1.X;
                            p2.Y = p4.Y + 20;
                            p3.X = p4.X;
                            p3.Y = p2.Y;
                            DrawArrow(p1, p2, p3, p4, e);
                        }

                    }
                    if (keyPair.Key == LayerType.Context)
                    {
                        p4.X = x + 6;
                        p4.Y = y + 15;
                    }
                    trackNodes.Add(new Point(x + 10, y + 5));

                    if (i == 0)
                    {
                        startL.X = x - 6;
                        startL.Y = y - 6;
                        layerRectSizeY += 30;
                    }
                    else if (i == keyPair.Value.Count - 1)
                    {
                        DrawLayerRect(startL.X, startL.Y, layerRectSizeX, layerRectSizeY, e);
                    }
                    else
                    {
                        layerRectSizeY += 30;
                    }

                    y += 30;


                }
                counter++;

            }

        }

        private void DrawCircle(int x, int y, PaintEventArgs e)
        {
            int size = 10;
            //Graphics gr = CreateGraphics();
            Rectangle rect = new Rectangle(x, y, size, size);
            e.Graphics.DrawEllipse(new Pen(Color.Blue), rect);
        }

        private void DrawLine(Point point1, Point point2, PaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(Color.Purple), point1, point2);
        }

        private void DrawLayerRect(int x, int y, int sizeX, int sizeY, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.Black), x, y, sizeX, sizeY);
        }

        private void DrawArrow(Point p1, Point p2, Point p3, Point p4, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 4);
            e.Graphics.DrawLine(pen, p1, p2);
            e.Graphics.DrawLine(pen, p2, p3);
            pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            e.Graphics.DrawLine(pen, p3, p4);

        }

        private void pbNet_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (iSelectedNet != null)
            {
                if (iSelectedNet.GetNetType() == NetType.ElmanNet)
                {
                    selectedNet = (ElmanNet)iSelectedNet;
                }
                else
                {
                    selectedNet = null;
                }
            }
            if (selectedNet != null)
            {
                DrawNet(e);
            }
        }

        private void pbNet_Resize(object sender, EventArgs e)
        {
            pbNet.Invalidate();
        }

        private void menu_Load(object sender, EventArgs e)
        {

        }

        private void rbColourToggle_CheckedChanged(object sender, EventArgs e)
        {
            isCheckedCol = rbColourToggle.Checked;
        }

        private void rbColourToggle_Click(object sender, EventArgs e)
        {
            if (rbColourToggle.Checked && !isCheckedCol)
            {
                disableCoulours = true ;
                rbColourToggle.Checked = false;
                btnDelete.BackColor = Color.Silver;
            }
            else
            {
                
                disableCoulours = false;
                rbColourToggle.Checked = true;
                isCheckedCol = false;
                btnDelete.BackColor = Color.FromArgb(255, 128, 128);
            }
            CheckProcessesRunning(YARP_Proc, lblYarpServer, btnManageYarp);
            CheckProcessesRunning(ICub_Proc, lblICubSim, btnManageICub);
            CheckProcessesRunning(YarpRobotInterface_Proc, lblYARPRobotInterface);
            CheckProcessesRunning(iKinCartesianSolver_Proc, lblIKinCartesianSolver);
            
        }

        private static BasicMLDataSet Generate(int count, string filePath, int inputNodes=1, int outputNodes=1, bool cont=false)
        {
            List<List<float>> inputTmp2 = new List<List<float>>();
            List<List<float>> outputTmp2 = new List<List<float>>();

            //readCSV("D:\\Debug\\NetTrainData5x10x10.csv", ref inputTmp2, ref outputTmp2);
            readCSV(filePath, ref inputTmp2, ref outputTmp2);

            double[][] data8 = new double[inputTmp2.Count][];
            double[][] idealData8 = new double[outputTmp2.Count][];

            double[][] data9;
            double[][] idealData9;

            if (cont)
            {
                if (inputNodes == 1)
                {
                    for (int i = 0; i < inputTmp2.Count; i++)
                    {
                        data8[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData8[i] = new double[outputTmp2.ElementAt(i).Count];
                        for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                        {
                            data8[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                            if (j == inputTmp2.ElementAt(i).Count - 1)
                            {
                                idealData8[i][0] = outputTmp2.ElementAt(i).ElementAt(0);
                            }
                        }
                    }
                }
                else
                {
                    data8 = new double[inputTmp2.Count][];
                    idealData8 = new double[outputTmp2.Count][];
                    for (int i = 0; i < inputTmp2.Count; i++)
                    {
                        data8[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData8[i] = new double[inputTmp2.ElementAt(i).Count];
                        for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                        {
                            data8[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                            idealData8[i][j] = outputTmp2.ElementAt(i).ElementAt(0);

                        }
                    }
                }

                int size = data8.Length;
                if (outputNodes == 1)
                {
                    data8 = SplitDataIntoRows(data8, out idealData8);

                    //int size = data8.Length;
                    data9 = new double[size * 100][];
                    idealData9 = new double[size * 100][];

                    for (int it = 0; it < 10; it++)
                    {
                        for (int i = 0; i < data8.Length; i++)
                        {
                            data9[i + 10 * size * it] = new double[1];
                            idealData9[i + 10 * size * it] = new double[1];
                            for (int j = 0; j < data8[i].Length; j++)
                            {
                                data9[i + 10 * size * it][j] = data8[i][j];
                                if (j == data8[i].Length - 1)
                                {
                                    idealData9[i + 10 * size * it][0] = idealData8[i][0];
                                }
                            }
                        }
                    }
                }
                else
                {
                    int counterIdeal = 0;
                    data9 = new double[inputTmp2.Count][];
                    idealData9 = new double[inputTmp2.Count][];
                    for (int i = 0; i < inputTmp2.Count; i++)
                    {
                        data9[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData9[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData9[i] = new double[outputNodes];

                        for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                        {
                            data9[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                            //if (j == inputTmp2a.ElementAt(i).Count - 1)
                            //{
                            if (inputTmp2.ElementAt(i).ElementAt(j) == 1)
                            {
                                counterIdeal++;
                            }
                            else
                            {
                                counterIdeal = 0;
                            }
                            //idealData8b[i][j] = outputTmp2a.ElementAt(i).ElementAt(j);
                            if (j < outputNodes)
                            {
                                idealData9[i][j] = counterIdeal;
                            }
                            //}
                            if (j >= inputTmp2.ElementAt(i).Count - 1)
                            {
                                counterIdeal = 0;
                            }
                        }
                    }
                }
            }
            else
            {
                
                if (inputNodes == 1)
                {
                    for (int i = 0; i < inputTmp2.Count; i++)
                    {
                        data8[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData8[i] = new double[outputTmp2.ElementAt(i).Count];
                        for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                        {
                            data8[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                            if (j == inputTmp2.ElementAt(i).Count - 1)
                            {
                                idealData8[i][0] = outputTmp2.ElementAt(i).ElementAt(0);
                            }
                        }
                    }
                }
                else
                {
                    data8 = new double[inputTmp2.Count][];
                    idealData8 = new double[outputTmp2.Count][];
                    for (int i = 0; i < inputTmp2.Count; i++)
                    {
                        data8[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData8[i] = new double[inputTmp2.ElementAt(i).Count];
                        for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                        {
                            data8[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                            idealData8[i][j] = outputTmp2.ElementAt(i).ElementAt(0);

                        }
                    }
                }

                int size = data8.Length;
                
                if (outputNodes == 1)
                {
                    data8 = SplitDataIntoRows(data8, out idealData8);

                    //int size = data8.Length;
                    data9 = new double[size * 100][];
                    idealData9 = new double[size * 100][];

                    for (int it = 0; it < 10; it++)
                    {
                        for (int i = 0; i < data8.Length; i++)
                        {
                            data9[i + 10 * size * it] = new double[1];
                            idealData9[i + 10 * size * it] = new double[1];
                            for (int j = 0; j < data8[i].Length; j++)
                            {
                                data9[i + 10 * size * it][j] = data8[i][j];
                                if (j == data8[i].Length - 1)
                                {
                                    idealData9[i + 10 * size * it][0] = idealData8[i][0];
                                }
                            }
                        }
                    }
                }
                else
                {
                    int counterIdeal = 0;
                    data9 = new double[inputTmp2.Count][];
                    idealData9 = new double[inputTmp2.Count][];
                    for (int i = 0; i < inputTmp2.Count; i++)
                    {
                        data9[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData9[i] = new double[inputTmp2.ElementAt(i).Count];
                        idealData9[i] = new double[outputNodes];

                        for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                        {
                            data9[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                            //if (j == inputTmp2a.ElementAt(i).Count - 1)
                            //{
                            if (inputTmp2.ElementAt(i).ElementAt(j) == 1)
                            {
                                counterIdeal++;
                            }
                            //idealData8b[i][j] = outputTmp2a.ElementAt(i).ElementAt(j);
                            if (j < outputNodes)
                            {
                                idealData9[i][j] = counterIdeal;
                            }
                            //}
                            if (j >= inputTmp2.ElementAt(i).Count - 1)
                            {
                                counterIdeal = 0;
                            }
                        }
                    }
                }
            }
            //return new BasicMLDataSet(input, ideal);
            //return new BasicMLDataSet(data2, idealData2);
            //return new BasicMLDataSet(data3, idealData3);
            //return new BasicMLDataSet(data5, idealData5);
            //return new BasicMLDataSet(data6, idealData6);
            return new BasicMLDataSet(data9, idealData9);
        }

        private static void readCSV(string path, ref List<List<float>> inputVals, ref List<List<float>> outputTargets)
        {
            //List<float> inputVals = new List<float>();
            //List<float> outputTargets = new List<float>();

            FileStream fs = File.OpenRead(path);
            using (StreamReader reader = new StreamReader(fs))
            {
                for (int l = 0; l < File.ReadAllLines(path).Count(); l++)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');

                    List<float> inputValsTmp = new List<float>();
                    List<float> outputValsTmp = new List<float>();

                    int counter = 1;
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (counter == 1)
                        {
                            if (!string.IsNullOrEmpty(values[i]))
                            {
                                float val = float.Parse(values.ElementAt(i));
                                if (val != null)
                                {
                                    inputValsTmp.Add(val);
                                }
                                else
                                {
                                    counter++;
                                }
                            }
                            else
                            {
                                counter++;
                            }
                        }
                        else
                        {
                            float val = float.Parse(values.ElementAt(i));
                            if (val != null)
                            {
                                outputValsTmp.Add(val);
                            }
                        }
                    }
                    inputVals.Add(inputValsTmp);
                    outputTargets.Add(outputValsTmp);
                }
            }
            //return inputVals;
        }

        private static double[][] SplitDataIntoRows(double[][] data, out double[][] idealData)
        {
            double[][] dataInput = new double[data.Length * data[0].Length][];
            idealData = new double[data.Length * data[0].Length][];

            for (int i = 0; i < data.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < data[i].Length; j++)
                {
                    dataInput[data[i].Length * i + j] = new double[1];
                    dataInput[data[i].Length * i + j][0] = data[i][j];
                    if (data[i][j] == 1)
                    {
                        counter++;
                    }
                    idealData[data[i].Length * i + j] = new double[1];
                    idealData[data[i].Length * i + j][0] = counter;
                }
            }
            return dataInput;
        }
  
    }
}
