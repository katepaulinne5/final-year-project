﻿namespace RNN
{
    partial class SimPathManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtYARPPath = new System.Windows.Forms.TextBox();
            this.txtICubPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtYARPLineArgs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtYarpRobotInterfacePath = new System.Windows.Forms.TextBox();
            this.txtIKinCartesianSolverPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIKinCartesianSolverLineArgs = new System.Windows.Forms.TextBox();
            this.txtYarpRobotInterfaceLineArgs = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(496, 251);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(137, 26);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save to Registry";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(639, 251);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 26);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(131, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "YARP Path:";
            // 
            // txtYARPPath
            // 
            this.txtYARPPath.Location = new System.Drawing.Point(246, 37);
            this.txtYARPPath.Name = "txtYARPPath";
            this.txtYARPPath.Size = new System.Drawing.Size(478, 22);
            this.txtYARPPath.TabIndex = 3;
            // 
            // txtICubPath
            // 
            this.txtICubPath.Location = new System.Drawing.Point(246, 65);
            this.txtICubPath.Name = "txtICubPath";
            this.txtICubPath.Size = new System.Drawing.Size(478, 22);
            this.txtICubPath.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(140, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "ICub Path:";
            // 
            // txtYARPLineArgs
            // 
            this.txtYARPLineArgs.Location = new System.Drawing.Point(246, 149);
            this.txtYARPLineArgs.Name = "txtYARPLineArgs";
            this.txtYARPLineArgs.Size = new System.Drawing.Size(478, 22);
            this.txtYARPLineArgs.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(100, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "YARP Line Args:";
            // 
            // txtYarpRobotInterfacePath
            // 
            this.txtYarpRobotInterfacePath.Location = new System.Drawing.Point(246, 93);
            this.txtYarpRobotInterfacePath.Name = "txtYarpRobotInterfacePath";
            this.txtYarpRobotInterfacePath.Size = new System.Drawing.Size(478, 22);
            this.txtYarpRobotInterfacePath.TabIndex = 8;
            // 
            // txtIKinCartesianSolverPath
            // 
            this.txtIKinCartesianSolverPath.Location = new System.Drawing.Point(246, 121);
            this.txtIKinCartesianSolverPath.Name = "txtIKinCartesianSolverPath";
            this.txtIKinCartesianSolverPath.Size = new System.Drawing.Size(478, 22);
            this.txtIKinCartesianSolverPath.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "iKin Cartesian Solver Path:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "yarprobotinterface Path:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "iKin CartesianSolver Line Args:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(192, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "yarprobotinterface Line Args:";
            // 
            // txtIKinCartesianSolverLineArgs
            // 
            this.txtIKinCartesianSolverLineArgs.Location = new System.Drawing.Point(246, 205);
            this.txtIKinCartesianSolverLineArgs.Name = "txtIKinCartesianSolverLineArgs";
            this.txtIKinCartesianSolverLineArgs.Size = new System.Drawing.Size(478, 22);
            this.txtIKinCartesianSolverLineArgs.TabIndex = 14;
            // 
            // txtYarpRobotInterfaceLineArgs
            // 
            this.txtYarpRobotInterfaceLineArgs.Location = new System.Drawing.Point(246, 177);
            this.txtYarpRobotInterfaceLineArgs.Name = "txtYarpRobotInterfaceLineArgs";
            this.txtYarpRobotInterfaceLineArgs.Size = new System.Drawing.Size(478, 22);
            this.txtYarpRobotInterfaceLineArgs.TabIndex = 15;
            // 
            // SimPathManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 299);
            this.Controls.Add(this.txtYarpRobotInterfaceLineArgs);
            this.Controls.Add(this.txtIKinCartesianSolverLineArgs);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtIKinCartesianSolverPath);
            this.Controls.Add(this.txtYarpRobotInterfacePath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtYARPLineArgs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtICubPath);
            this.Controls.Add(this.txtYARPPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Name = "SimPathManager";
            this.Text = "SimPathManager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtYARPPath;
        private System.Windows.Forms.TextBox txtICubPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtYARPLineArgs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtYarpRobotInterfacePath;
        private System.Windows.Forms.TextBox txtIKinCartesianSolverPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIKinCartesianSolverLineArgs;
        private System.Windows.Forms.TextBox txtYarpRobotInterfaceLineArgs;
    }
}