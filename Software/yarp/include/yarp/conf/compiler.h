
// This is a generated file. Do not edit!

#ifndef YARP_COMPILER_DETECTION_H
#define YARP_COMPILER_DETECTION_H

/**
 * @{
 * @name Compiler Identification Macros
 *
 * Macro useful to identify current compiler and its version.
 */

/**
 * @def YARP_COMPILER_IS_Comeau
 */

/**
 * @def YARP_COMPILER_IS_Intel
 */

/**
 * @def YARP_COMPILER_IS_PathScale
 */

/**
 * @def YARP_COMPILER_IS_Embarcadero
 */

/**
 * @def YARP_COMPILER_IS_Borland
 */

/**
 * @def YARP_COMPILER_IS_Watcom
 */

/**
 * @def YARP_COMPILER_IS_OpenWatcom
 */

/**
 * @def YARP_COMPILER_IS_SunPro
 */

/**
 * @def YARP_COMPILER_IS_HP
 */

/**
 * @def YARP_COMPILER_IS_Compaq
 */

/**
 * @def YARP_COMPILER_IS_zOS
 */

/**
 * @def YARP_COMPILER_IS_XL
 */

/**
 * @def YARP_COMPILER_IS_VisualAge
 */

/**
 * @def YARP_COMPILER_IS_PGI
 */

/**
 * @def YARP_COMPILER_IS_Cray
 */

/**
 * @def YARP_COMPILER_IS_TI
 */

/**
 * @def YARP_COMPILER_IS_Fujitsu
 */

/**
 * @def YARP_COMPILER_IS_SCO
 */

/**
 * @def YARP_COMPILER_IS_AppleClang
 */

/**
 * @def YARP_COMPILER_IS_Clang
 */

/**
 * @def YARP_COMPILER_IS_GNU
 */

/**
 * @def YARP_COMPILER_IS_MSVC
 */

/**
 * @def YARP_COMPILER_IS_ADSP
 */

/**
 * @def YARP_COMPILER_IS_IAR
 */

/**
 * @def YARP_COMPILER_IS_ARMCC
 */

/**
 * @def YARP_COMPILER_IS_MIPSpro
 */

/**
 * @def YARP_COMPILER_IS_GNU
 */

/**
 * @def YARP_COMPILER_VERSION_MAJOR
 *
 * Current compiler major version number, if defined.
 *
 * @see YARP_COMPILER_VERSION_MINOR, YARP_COMPILER_VERSION_PATCH
 */

/**
 * @def YARP_COMPILER_VERSION_MINOR
 *
 * Current compiler minor version number, if defined.
 *
 * @see YARP_COMPILER_VERSION_MAJOR, YARP_COMPILER_VERSION_PATCH
 */

/**
 * @def YARP_COMPILER_VERSION_PATCH
 *
 * Current compiler patch version number, if defined.
 *
 * @see YARP_COMPILER_VERSION_MAJOR, YARP_COMPILER_VERSION_MINOR
 */

/**
 * @}
 */

/**
 * @{
 * @name Compiler Supported Features
 *
 * Macros useful to check if current compiler and build flags combination
 * supports specific features.
 */

/**
 * @def YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS
 *
 * 1 if the current c++ compiler and build flags combination supports aggregate
 * default initializers, as defined in
 * [N3605](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3605.html).
 */

/**
 * @def YARP_COMPILER_CXX_ALIAS_TEMPLATES
 *
 * 1 if the current c++ compiler and build flags combination supports template
 * aliases, as defined in
 * [N2258](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2258.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_ALIGNAS
 *
 * 1 if the current c++ compiler and build flags combination supports alignment
 * control `alignas`, as defined in
 * [N2341](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2341.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_ALIGNOF
 *
 * 1 if the current c++ compiler and build flags combination supports alignment
 * control `alignof`, as defined in
 * [N2341](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2341.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_ATTRIBUTES
 *
 * 1 if the current c++ compiler and build flags combination supports generic
 * attributes, as defined in
 * [N2761](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2761.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED
 *
 * 1 if the current c++ compiler and build flags combination supports the
 * `[[deprecated]]` attribute, as defined in
 * [N3760](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3760.html).
 */

/**
 * @def YARP_COMPILER_CXX_AUTO_TYPE
 *
 * 1 if the current c++ compiler and build flags combination supports automatic
 * type deduction, as defined in
 * [N1984](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1984.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_BINARY_LITERALS
 *
 * 1 if the current c++ compiler and build flags combination supports binary
 * literals, as defined in
 * [N3472](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3472.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_CONSTEXPR
 *
 * 1 if the current c++ compiler and build flags combination supports constant
 * expressions, as defined in
 * [N2235](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2235.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS
 *
 * 1 if the current c++ compiler and build flags combination supports contextual
 * conversions, as defined in
 * [N3323](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3323.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_DECLTYPE
 *
 * 1 if the current c++ compiler and build flags combination supports decltype,
 * as defined in
 * [N2343](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2343.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_DECLTYPE_AUTO
 *
 * 1 if the current c++ compiler and build flags combination supports
 * `decltype(auto)` semantics, as defined in
 * [N3638](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3638.html).
 */

/**
 * @def YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES
 *
 * 1 if the current c++ compiler and build flags combination supports decltype
 * on incomplete return types, as defined in
 * [N3276](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3276.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS
 *
 * 1 if the current c++ compiler and build flags combination supports default
 * template arguments for function templates, as defined in DR226.
 */

/**
 * @def YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS
 *
 * 1 if the current c++ compiler and build flags combination supports
 * defaulted functions, as defined in
 * [N2346](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2346.htm).
 *
 * @see YARP_DEFAULTED_FUNCTION, YARP_COMPILER_CXX_DELETED_FUNCTIONS
 */

/**
 * @def YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS
 *
 * 1 if the current c++ compiler and build flags combination supports
 * defaulted move initializers, as defined in
 * [N3053](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3053.html).
 */

/**
 * @def YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS
 *
 * 1 if the current c++ compiler and build flags combination supports delegating
 * constructors, as defined in
 * [N1986](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1986.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_DELETED_FUNCTIONS
 *
 * 1 if the current c++ compiler and build flags combination supports deleted
 * functions, as defined in
 * [N2346](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2346.htm).
 *
 * @see YARP_DELETED_FUNCTION, YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS
 */

/**
 * @def YARP_COMPILER_CXX_DIGIT_SEPARATORS
 *
 * 1 if the current c++ compiler and build flags combination supports digit
 * separators, as defined in
 * [N3781](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3781.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS
 *
 * 1 if the current c++ compiler and build flags combination supports enum
 * forward declarations, as defined in
 * [N2764](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2764.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS
 *
 * 1 if the current c++ compiler and build flags combination supports explicit
 * conversion operators, as defined in
 * [N2437](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2437.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS
 *
 * 1 if the current c++ compiler and build flags combination supports extended
 * friend declarations, as defined in
 * [N1791](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1791.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_EXTERN_TEMPLATES
 *
 * 1 if the current c++ compiler and build flags combination supports extern
 * templates, as defined in
 * [N1987](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1987.htm).
 */

/**
 * @def YARP_COMPILER_CXX_FINAL
 *
 * 1 if the current c++ compiler and build flags combination supports the
 * override control `final` keyword, as defined in
 * [N2928](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2009/n2928.htm),
 * [N3206](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3206.htm)
 * and [N3272](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3272.htm).
 *
 * @see YARP_FINAL, YARP_COMPILER_CXX_OVERRIDE
 */

/**
 * @def YARP_COMPILER_CXX_FUNC_IDENTIFIER
 *
 * 1 if the current c++ compiler and build flags combination supports predefined
 * `__func__` identifier, as defined in
 * [N2340](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2340.htm).
 */

/**
 * @def YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS
 *
 * 1 if the current c++ compiler and build flags combination supports
 * initializer lists, as defined in
 * [N2672](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2672.htm).
 */

/**
 * @def YARP_COMPILER_CXX_GENERIC_LAMBDAS
 *
 * 1 if the current c++ compiler and build flags combination supports generic
 * lambdas, as defined in
 * [N3649](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3649.html).
 */

/**
 * @def YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS
 *
 * 1 if the current c++ compiler and build flags combination supports inheriting
 * constructors, as defined in
 * [N2540](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2540.htm).
 */

/**
 * @def YARP_COMPILER_CXX_INLINE_NAMESPACES
 *
 * 1 if the current c++ compiler and build flags combination supports inline
 * namespaces, as defined in
 * [N2535](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2535.htm).
 */

/**
 * @def YARP_COMPILER_CXX_LAMBDAS
 *
 * 1 if the current c++ compiler and build flags combination supports lambda
 * functions, as defined in
 * [N2927](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2009/n2927.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES
 *
 * 1 if the current c++ compiler and build flags combination supports lambda
 * init captures, as defined in
 * [N3648](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3648.html).
 */

/**
 * @def YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS
 *
 * 1 if the current c++ compiler and build flags combination supports local and
 * unnamed types as template arguments, as defined in
 * [N2657](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2657.htm).
 */

/**
 * @def YARP_COMPILER_CXX_LONG_LONG_TYPE
 *
 * 1 if the current c++ compiler and build flags combination supports
 * `long long` type, as defined in
 * [N1811](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1811.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_NOEXCEPT
 *
 * 1 if the current c++ compiler and build flags combination supports
 * exception specifications, as defined in
 * [N3050](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3050.html).
 *
 * @see YARP_NOEXCEPT, YARP_NOEXCEPT_EXPR
 */

/**
 * @def YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT
 *
 * 1 if the current c++ compiler and build flags combination supports non-static
 * data member initialization, as defined in
 * [N2756](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2756.htm).
 */

/**
 * @def YARP_COMPILER_CXX_NULLPTR
 *
 * 1 if the current c++ compiler and build flags combination supports null
 * pointer, as defined in
 * [N2431](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2431.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_OVERRIDE
 *
 * 1 if the current c++ compiler and build flags combination supports the
 * override control `override` keyword, as defined in
 * [N2928](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2009/n2928.htm),
 * [N3206](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3206.htm)
 * and [N3272](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3272.htm).
 *
 * @see YARP_OVERRIDE, YARP_COMPILER_CXX_FINAL
 */

/**
 * @def YARP_COMPILER_CXX_RANGE_FOR
 *
 * 1 if the current c++ compiler and build flags combination supports range
 * based for, as defined in
 * [N2930](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2009/n2930.html).
 */

/**
 * @def YARP_COMPILER_CXX_RAW_STRING_LITERALS
 *
 * 1 if the current c++ compiler and build flags combination supports raw string
 * literals, as defined in
 * [N2442](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2442.htm).
 */

/**
 * @def YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS
 *
 * 1 if the current c++ compiler and build flags combination supports reference
 * qualified functions, as defined in
 * [N2439](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2439.htm).
 */

/**
 * @def YARP_COMPILER_CXX_RELAXED_CONSTEXPR
 *
 * 1 if the current c++ compiler and build flags combination supports relaxed
 * constexpr, as defined in
 * [N3652](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3652.html).
 */

/**
 * @def YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION
 *
 * 1 if the current c++ compiler and build flags combination supports return
 * type deduction on normal functions, as defined in
 * [N3386](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3386.html).
 */

/**
 * @def YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS
 *
 * 1 if the current c++ compiler and build flags combination supports right
 * angle bracket parsing, as defined in
 * [N1757](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1757.html).
 */

/**
 * @def YARP_COMPILER_CXX_RVALUE_REFERENCES
 *
 * 1 if the current c++ compiler and build flags combination supports r-value
 * references, as defined in
 * [N2118](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2118.html).
 */

/**
 * @def YARP_COMPILER_CXX_SIZEOF_MEMBER
 *
 * 1 if the current c++ compiler and build flags combination supports size of
 * non-static data members, as defined in
 * [N2253](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2253.html).
 */

/**
 * @def YARP_COMPILER_CXX_STATIC_ASSERT
 *
 * 1 if the current c++ compiler and build flags combination supports static
 * asserts, as defined in
 * [N1720](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1720.html).
 */

/**
 * @def YARP_COMPILER_CXX_STRONG_ENUMS
 *
 * 1 if the current c++ compiler and build flags combination supports strongly
 * typed enums, as defined in
 * [N2347](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2347.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS
 *
 * 1 if the current c++ compiler and build flags combination supports template
 * template parameters, as defined in ISO/IEC 14882:1998.
 */

/**
 * @def YARP_COMPILER_CXX_THREAD_LOCAL
 *
 * 1 if the current c++ compiler and build flags combination supports
 * thread-local variables, as defined in
 * [N2659](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2659.htm).
 */

/**
 * @def YARP_COMPILER_CXX_TRAILING_RETURN_TYPES
 *
 * 1 if the current c++ compiler and build flags combination supports automatic
 * function return type, as defined in
 * [N2541](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2541.htm).
 */

/**
 * @def YARP_COMPILER_CXX_UNICODE_LITERALS
 *
 * 1 if the current c++ compiler and build flags combination supports unicode
 * string literals, as defined in
 * [N2443](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2442.htm).
 */

/**
 * @def YARP_COMPILER_CXX_UNIFORM_INITIALIZATION
 *
 * 1 if the current c++ compiler and build flags combination supports uniform
 * initialization, as defined in
 * [N2640](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2640.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_UNRESTRICTED_UNIONS
 *
 * 1 if the current c++ compiler and build flags combination supports uniform
 * initialization, as defined in
 * [N2544](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2544.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_USER_LITERALS
 *
 * 1 if the current c++ compiler and build flags combination supports
 * user-defined literals, as defined in
 * [N2765](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2765.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_VARIABLE_TEMPLATES
 *
 * 1 if the current c++ compiler and build flags combination supports
 * user-defined literals, as defined in
 * [N3651](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3651.pdf).
 */

/**
 * @def YARP_COMPILER_CXX_VARIADIC_MACROS
 *
 * 1 if the current c++ compiler and build flags combination supports variadic
 * macros, as defined in
 * [N1653](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1653.htm).
 */

/**
 * @def YARP_COMPILER_CXX_VARIADIC_TEMPLATES
 *
 * 1 if the current c++ compiler and build flags combination supports variadic
 * templates, as defined in
 * [N2242](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2242.pdf).
 */

/**
 * @}
 */

/**
 * @{
 * @name Alignment Macros
 */

/**
 * @def YARP_ALIGNAS
 *
 * Expands to the standard `alignas` decorator if supported, or a
 * compiler-specific decorator such as `__attribute__ ((__aligned__))` used by
 * GNU compilers.
 *
 * @see YARP_COMPILER_CXX_ALIGNAS, YARP_ALIGNOF
 */

/**
 * @def YARP_ALIGNOF
 *
 * Expands to the standard `alignof` decorator if supported, or a
 * compiler-specific decorator such as `__alignof__` used by GNU compilers.
 *
 * @see YARP_COMPILER_CXX_ALIGNOF, YARP_ALIGNAS
 */

/**
 * @}
 */

/**
 * @{
 * @name Deprecated Functions Macros
 */

/**
 * @def YARP_DEPRECATED
 *
 * Expands to either the standard `[[deprecated]]` attribute or a
 * compiler-specific decorator such as `__attribute__((__deprecated__))` used by
 * GNU compilers.
 *
 * @see YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED YARP_DEPRECATED_MSG
 */

/**
 * @def YARP_DEPRECATED_MSG
 *
 * Expands to either the standard `[[deprecated]]` attribute or a
 * compiler-specific decorator such as `__attribute__((__deprecated__))` used by
 * GNU compilers.
 *
 * @see YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED YARP_DEPRECATED
 */

/**
 * @}
 */

/**
 * @{
 * @name Constant Expressions Macros
 */

/**
 * @def YARP_CONSTEXPR
 *
 * Expands to `constexpr` if constant expressions are supported by the
 * current c++ compiler and build flags combination, defined empty elsewhere.
 *
 * @see YARP_COMPILER_CXX_CONSTEXPR
 */

/**
 * @}
 */

/**
 * @{
 * @name Deleted Functions Macros
 */

/**
 * @def YARP_DELETED_FUNCTION
 *
 * Expands to `= delete` if deleted functions are supported by the the current
 * c++ compiler and build flags combination, defined empty elsewhere.
 *
 * @see YARP_COMPILER_CXX_DELETED_FUNCTIONS
 */

/**
 * @}
 */

/**
 * @{
 * @name Extern Templates Macros
 */

/**
 * @def YARP_EXTERN_TEMPLATE
 *
 * Expands to `exter` if extern templates are supported by the the current
 * c++ compiler and build flags combination, defined empty elsewhere.
 *
 * @see YARP_COMPILER_CXX_EXTERN_TEMPLATES
 */

/**
 * @}
 */

/**
 * @{
 * @name Override Control Macros
 */

/**
 * @def YARP_OVERRIDE
 *
 * Expands to the override control `override` keyword if this is supported by
 * the current c++ compiler and build flags combination, defined empty
 * elsewhere.
 *
 * @see YARP_COMPILER_CXX_OVERRIDE, YARP_FINAL
 */

/**
 * @def YARP_FINAL
 *
 * Expands to the override control `final` keyword if this is supported by
 * the current c++ compiler and build flags combination, defined empty
 * elsewhere.
 *
 * @see YARP_COMPILER_CXX_FINAL, YARP_OVERRIDE
 */

/**
 * @}
 */

/**
 * @{
 * @name Exception Specifications Macros
 */

/**
 * @def YARP_NOEXCEPT
 *
 * Expands to `noexcept` if exception specifications are supported by the
 * current c++ compiler and build flags combination, defined empty elsewhere.
 *
 * @see YARP_COMPILER_CXX_NOEXCEPT, YARP_NOEXCEPT_EXPR
 */

/**
 * @def YARP_NOEXCEPT_EXPR
 *
 * Expands to `noexcept(X)` if exception specifications are supported by the
 * current c++ compiler and build flags combination, defined empty elsewhere.
 *
 * @see YARP_COMPILER_CXX_NOEXCEPT, YARP_NOEXCEPT
 */

/**
 * @}
 */

/**
 * @{
 * @name Null Pointer Macros
 */

/**
 * @def YARP_NULLPTR
 *
 * Expands to either the standard `nullptr` or to `0` elsewhere.
 *
 * @see YARP_COMPILER_CXX_NULLPTR
 */

/**
 * @}
 */

/**
 * @{
 * @name Static Assert Macros
 */

/**
 * @def YARP_STATIC_ASSERT
 *
 * Expands to `static_assert(X, #X)` if static asserts are supported by the
 * current c++ compiler and build flags combination, to a compatibility
 * implementation otherwise.
 *
 * @warning This macro cannot be used everywhere like a `static_assert`, see
 *          [CMake#16185](https://gitlab.kitware.com/cmake/cmake/issues/16185)).
 *          Moreover, in order to be portable, if the `X` argument is an
 *          expression, it must be enclosed in parenthesis. For example:
 * @code
 * YARP_STATIC_ASSERT((42 < 0));
 * @endcode
 *
 * @see YARP_COMPILER_CXX_STATIC_ASSERT YARP_STATIC_ASSERT_MSG
 */

/**
 * @def YARP_STATIC_ASSERT_MSG
 *
 * Expands to `static_assert(X, MSG)` if static asserts are supported by the
 * current c++ compiler and build flags combination, to a compatibility
 * implementation otherwise.
 *
 * @warning This macro cannot be used everywhere like a `static_assert`, see
 *          [CMake#16185](https://gitlab.kitware.com/cmake/cmake/issues/16185)).
 *          Moreover, in order to be portable, if the `X` argument is an
 *          expression, it must be enclosed in parenthesis. For example:
 * @code
 * YARP_STATIC_ASSERT_MSG((42 < 0), "This is wrong");
 * @endcode
 *
 * @see YARP_COMPILER_CXX_STATIC_ASSERT YARP_STATIC_ASSERT
 */

/**
 * @}
 */

/**
 * @{
 * @name Thread-Local Variables Macros
 */

/**
 * @def YARP_THREAD_LOCAL
 *
 * Expands to either the standard `thread_loca` attribute, to a
 * compiler-specific decorator such as `__thread` used by GNU compilers, or
 * defined empty if not supported by the current c++ compiler and build flags
 * combination.
 *
 * @see YARP_COMPILER_CXX_THREAD_LOCAL
 */

/**
 * @}
 */


#ifdef __cplusplus
# define YARP_COMPILER_IS_Comeau 0
# define YARP_COMPILER_IS_Intel 0
# define YARP_COMPILER_IS_PathScale 0
# define YARP_COMPILER_IS_Embarcadero 0
# define YARP_COMPILER_IS_Borland 0
# define YARP_COMPILER_IS_Watcom 0
# define YARP_COMPILER_IS_OpenWatcom 0
# define YARP_COMPILER_IS_SunPro 0
# define YARP_COMPILER_IS_HP 0
# define YARP_COMPILER_IS_Compaq 0
# define YARP_COMPILER_IS_zOS 0
# define YARP_COMPILER_IS_XL 0
# define YARP_COMPILER_IS_VisualAge 0
# define YARP_COMPILER_IS_PGI 0
# define YARP_COMPILER_IS_Cray 0
# define YARP_COMPILER_IS_TI 0
# define YARP_COMPILER_IS_Fujitsu 0
# define YARP_COMPILER_IS_SCO 0
# define YARP_COMPILER_IS_AppleClang 0
# define YARP_COMPILER_IS_Clang 0
# define YARP_COMPILER_IS_GNU 0
# define YARP_COMPILER_IS_MSVC 0
# define YARP_COMPILER_IS_ADSP 0
# define YARP_COMPILER_IS_IAR 0
# define YARP_COMPILER_IS_ARMCC 0
# define YARP_COMPILER_IS_MIPSpro 0

#if defined(__COMO__)
# undef YARP_COMPILER_IS_Comeau
# define YARP_COMPILER_IS_Comeau 1

#elif defined(__INTEL_COMPILER) || defined(__ICC)
# undef YARP_COMPILER_IS_Intel
# define YARP_COMPILER_IS_Intel 1

#elif defined(__PATHCC__)
# undef YARP_COMPILER_IS_PathScale
# define YARP_COMPILER_IS_PathScale 1

#elif defined(__BORLANDC__) && defined(__CODEGEARC_VERSION__)
# undef YARP_COMPILER_IS_Embarcadero
# define YARP_COMPILER_IS_Embarcadero 1

#elif defined(__BORLANDC__)
# undef YARP_COMPILER_IS_Borland
# define YARP_COMPILER_IS_Borland 1

#elif defined(__WATCOMC__) && __WATCOMC__ < 1200
# undef YARP_COMPILER_IS_Watcom
# define YARP_COMPILER_IS_Watcom 1

#elif defined(__WATCOMC__)
# undef YARP_COMPILER_IS_OpenWatcom
# define YARP_COMPILER_IS_OpenWatcom 1

#elif defined(__SUNPRO_CC)
# undef YARP_COMPILER_IS_SunPro
# define YARP_COMPILER_IS_SunPro 1

#elif defined(__HP_aCC)
# undef YARP_COMPILER_IS_HP
# define YARP_COMPILER_IS_HP 1

#elif defined(__DECCXX)
# undef YARP_COMPILER_IS_Compaq
# define YARP_COMPILER_IS_Compaq 1

#elif defined(__IBMCPP__) && defined(__COMPILER_VER__)
# undef YARP_COMPILER_IS_zOS
# define YARP_COMPILER_IS_zOS 1

#elif defined(__IBMCPP__) && !defined(__COMPILER_VER__) && __IBMCPP__ >= 800
# undef YARP_COMPILER_IS_XL
# define YARP_COMPILER_IS_XL 1

#elif defined(__IBMCPP__) && !defined(__COMPILER_VER__) && __IBMCPP__ < 800
# undef YARP_COMPILER_IS_VisualAge
# define YARP_COMPILER_IS_VisualAge 1

#elif defined(__PGI)
# undef YARP_COMPILER_IS_PGI
# define YARP_COMPILER_IS_PGI 1

#elif defined(_CRAYC)
# undef YARP_COMPILER_IS_Cray
# define YARP_COMPILER_IS_Cray 1

#elif defined(__TI_COMPILER_VERSION__)
# undef YARP_COMPILER_IS_TI
# define YARP_COMPILER_IS_TI 1

#elif defined(__FUJITSU) || defined(__FCC_VERSION) || defined(__fcc_version)
# undef YARP_COMPILER_IS_Fujitsu
# define YARP_COMPILER_IS_Fujitsu 1

#elif defined(__SCO_VERSION__)
# undef YARP_COMPILER_IS_SCO
# define YARP_COMPILER_IS_SCO 1

#elif defined(__clang__) && defined(__apple_build_version__)
# undef YARP_COMPILER_IS_AppleClang
# define YARP_COMPILER_IS_AppleClang 1

#elif defined(__clang__)
# undef YARP_COMPILER_IS_Clang
# define YARP_COMPILER_IS_Clang 1

#elif defined(__GNUC__)
# undef YARP_COMPILER_IS_GNU
# define YARP_COMPILER_IS_GNU 1

#elif defined(_MSC_VER)
# undef YARP_COMPILER_IS_MSVC
# define YARP_COMPILER_IS_MSVC 1

#elif defined(__VISUALDSPVERSION__) || defined(__ADSPBLACKFIN__) || defined(__ADSPTS__) || defined(__ADSP21000__)
# undef YARP_COMPILER_IS_ADSP
# define YARP_COMPILER_IS_ADSP 1

#elif defined(__IAR_SYSTEMS_ICC__ ) || defined(__IAR_SYSTEMS_ICC)
# undef YARP_COMPILER_IS_IAR
# define YARP_COMPILER_IS_IAR 1

#elif defined(__ARMCC_VERSION)
# undef YARP_COMPILER_IS_ARMCC
# define YARP_COMPILER_IS_ARMCC 1

#elif defined(_SGI_COMPILER_VERSION) || defined(_COMPILER_VERSION)
# undef YARP_COMPILER_IS_MIPSpro
# define YARP_COMPILER_IS_MIPSpro 1


#endif

#  if YARP_COMPILER_IS_GNU

#    if !((__GNUC__ * 100 + __GNUC_MINOR__) >= 404)
#      error Unsupported compiler version
#    endif

# define YARP_COMPILER_VERSION_MAJOR (__GNUC__)
# if defined(__GNUC_MINOR__)
#  define YARP_COMPILER_VERSION_MINOR (__GNUC_MINOR__)
# endif
# if defined(__GNUC_PATCHLEVEL__)
#  define YARP_COMPILER_VERSION_PATCH (__GNUC_PATCHLEVEL__)
# endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 500 && __cplusplus >= 201402L
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 408 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_ALIGNAS 1
#    else
#      define YARP_COMPILER_CXX_ALIGNAS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 408 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_ALIGNOF 1
#    else
#      define YARP_COMPILER_CXX_ALIGNOF 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 408 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_ATTRIBUTES 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_AUTO_TYPE 1
#    else
#      define YARP_COMPILER_CXX_AUTO_TYPE 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_BINARY_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_BINARY_LITERALS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_CONSTEXPR 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_DECLTYPE 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 0
#    endif

#    if ((__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__) >= 40801) && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 1
#    else
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 405 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_FINAL 1
#    else
#      define YARP_COMPILER_CXX_FINAL 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 1
#    else
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 408 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 1
#    else
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 405 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_LAMBDAS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 1
#    else
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 405 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 1
#    else
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_NOEXCEPT 1
#    else
#      define YARP_COMPILER_CXX_NOEXCEPT 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 1
#    else
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_NULLPTR 1
#    else
#      define YARP_COMPILER_CXX_NULLPTR 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_OVERRIDE 1
#    else
#      define YARP_COMPILER_CXX_OVERRIDE 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_RANGE_FOR 1
#    else
#      define YARP_COMPILER_CXX_RANGE_FOR 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 405 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 0
#    endif

#    if ((__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__) >= 40801) && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 500 && __cplusplus >= 201402L
#      define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 409 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 1
#    else
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 1
#    else
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 1
#    else
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 1
#    else
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_STATIC_ASSERT 1
#    else
#      define YARP_COMPILER_CXX_STATIC_ASSERT 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_STRONG_ENUMS 1
#    else
#      define YARP_COMPILER_CXX_STRONG_ENUMS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && __cplusplus
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 1
#    else
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 408 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_THREAD_LOCAL 1
#    else
#      define YARP_COMPILER_CXX_THREAD_LOCAL 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 1
#    else
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 406 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 1
#    else
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_USER_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_USER_LITERALS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 500 && __cplusplus >= 201402L
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 0
#    endif

#    if (__GNUC__ * 100 + __GNUC_MINOR__) >= 404 && (__cplusplus >= 201103L || (defined(__GXX_EXPERIMENTAL_CXX0X__) && __GXX_EXPERIMENTAL_CXX0X__))
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 0
#    endif

#  elif YARP_COMPILER_IS_Clang

#    if !(((__clang_major__ * 100) + __clang_minor__) >= 304)
#      error Unsupported compiler version
#    endif

# define YARP_COMPILER_VERSION_MAJOR (__clang_major__)
# define YARP_COMPILER_VERSION_MINOR (__clang_minor__)
# define YARP_COMPILER_VERSION_PATCH (__clang_patchlevel__)
# if defined(_MSC_VER)
   /* _MSC_VER = VVRR */
#  define YARP_SIMULATE_VERSION_MAJOR (_MSC_VER / 100)
#  define YARP_SIMULATE_VERSION_MINOR (_MSC_VER % 100)
# endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_aggregate_nsdmi)
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_alias_templates)
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_alignas)
#      define YARP_COMPILER_CXX_ALIGNAS 1
#    else
#      define YARP_COMPILER_CXX_ALIGNAS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_alignas)
#      define YARP_COMPILER_CXX_ALIGNOF 1
#    else
#      define YARP_COMPILER_CXX_ALIGNOF 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_attributes)
#      define YARP_COMPILER_CXX_ATTRIBUTES 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_auto_type)
#      define YARP_COMPILER_CXX_AUTO_TYPE 1
#    else
#      define YARP_COMPILER_CXX_AUTO_TYPE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_binary_literals)
#      define YARP_COMPILER_CXX_BINARY_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_BINARY_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_constexpr)
#      define YARP_COMPILER_CXX_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_CONSTEXPR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_contextual_conversions)
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_decltype)
#      define YARP_COMPILER_CXX_DECLTYPE 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_decltype_incomplete_return_types)
#      define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_default_function_template_args)
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_defaulted_functions)
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_defaulted_functions)
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_delegating_constructors)
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_deleted_functions)
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 1
#    else
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_explicit_conversions)
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_override_control)
#      define YARP_COMPILER_CXX_FINAL 1
#    else
#      define YARP_COMPILER_CXX_FINAL 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 1
#    else
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_generalized_initializers)
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_inheriting_constructors)
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 1
#    else
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_lambdas)
#      define YARP_COMPILER_CXX_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_LAMBDAS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_init_captures)
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 1
#    else
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_local_type_template_args)
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 1
#    else
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_noexcept)
#      define YARP_COMPILER_CXX_NOEXCEPT 1
#    else
#      define YARP_COMPILER_CXX_NOEXCEPT 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_nonstatic_member_init)
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 1
#    else
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_nullptr)
#      define YARP_COMPILER_CXX_NULLPTR 1
#    else
#      define YARP_COMPILER_CXX_NULLPTR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_override_control)
#      define YARP_COMPILER_CXX_OVERRIDE 1
#    else
#      define YARP_COMPILER_CXX_OVERRIDE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_range_for)
#      define YARP_COMPILER_CXX_RANGE_FOR 1
#    else
#      define YARP_COMPILER_CXX_RANGE_FOR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_raw_string_literals)
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_reference_qualified_functions)
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_relaxed_constexpr)
#      define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_return_type_deduction)
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 1
#    else
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 1
#    else
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_rvalue_references)
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 1
#    else
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 1
#    else
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_static_assert)
#      define YARP_COMPILER_CXX_STATIC_ASSERT 1
#    else
#      define YARP_COMPILER_CXX_STATIC_ASSERT 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_strong_enums)
#      define YARP_COMPILER_CXX_STRONG_ENUMS 1
#    else
#      define YARP_COMPILER_CXX_STRONG_ENUMS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 199711L
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 1
#    else
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_thread_local)
#      define YARP_COMPILER_CXX_THREAD_LOCAL 1
#    else
#      define YARP_COMPILER_CXX_THREAD_LOCAL 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_trailing_return)
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_unicode_literals)
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_generalized_initializers)
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 1
#    else
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_unrestricted_unions)
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 1
#    else
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_user_literals)
#      define YARP_COMPILER_CXX_USER_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_USER_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_variable_templates)
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 304 && __has_feature(cxx_variadic_templates)
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 0
#    endif

#  elif YARP_COMPILER_IS_AppleClang

#    if !(((__clang_major__ * 100) + __clang_minor__) >= 400)
#      error Unsupported compiler version
#    endif

# define YARP_COMPILER_VERSION_MAJOR (__clang_major__)
# define YARP_COMPILER_VERSION_MINOR (__clang_minor__)
# define YARP_COMPILER_VERSION_PATCH (__clang_patchlevel__)
# if defined(_MSC_VER)
   /* _MSC_VER = VVRR */
#  define YARP_SIMULATE_VERSION_MAJOR (_MSC_VER / 100)
#  define YARP_SIMULATE_VERSION_MINOR (_MSC_VER % 100)
# endif
# define YARP_COMPILER_VERSION_TWEAK (__apple_build_version__)

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_aggregate_nsdmi)
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_alias_templates)
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_alignas)
#      define YARP_COMPILER_CXX_ALIGNAS 1
#    else
#      define YARP_COMPILER_CXX_ALIGNAS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_alignas)
#      define YARP_COMPILER_CXX_ALIGNOF 1
#    else
#      define YARP_COMPILER_CXX_ALIGNOF 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_attributes)
#      define YARP_COMPILER_CXX_ATTRIBUTES 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 501 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_auto_type)
#      define YARP_COMPILER_CXX_AUTO_TYPE 1
#    else
#      define YARP_COMPILER_CXX_AUTO_TYPE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_binary_literals)
#      define YARP_COMPILER_CXX_BINARY_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_BINARY_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_constexpr)
#      define YARP_COMPILER_CXX_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_CONSTEXPR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_contextual_conversions)
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_decltype)
#      define YARP_COMPILER_CXX_DECLTYPE 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 501 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_decltype_incomplete_return_types)
#      define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_default_function_template_args)
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_defaulted_functions)
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_defaulted_functions)
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_delegating_constructors)
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_deleted_functions)
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 501 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 1
#    else
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_explicit_conversions)
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_override_control)
#      define YARP_COMPILER_CXX_FINAL 1
#    else
#      define YARP_COMPILER_CXX_FINAL 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 1
#    else
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_generalized_initializers)
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 501 && __cplusplus > 201103L
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_inheriting_constructors)
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 1
#    else
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_lambdas)
#      define YARP_COMPILER_CXX_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_LAMBDAS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_init_captures)
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 1
#    else
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_local_type_template_args)
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 1
#    else
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_noexcept)
#      define YARP_COMPILER_CXX_NOEXCEPT 1
#    else
#      define YARP_COMPILER_CXX_NOEXCEPT 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_nonstatic_member_init)
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 1
#    else
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_nullptr)
#      define YARP_COMPILER_CXX_NULLPTR 1
#    else
#      define YARP_COMPILER_CXX_NULLPTR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_override_control)
#      define YARP_COMPILER_CXX_OVERRIDE 1
#    else
#      define YARP_COMPILER_CXX_OVERRIDE 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_range_for)
#      define YARP_COMPILER_CXX_RANGE_FOR 1
#    else
#      define YARP_COMPILER_CXX_RANGE_FOR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_raw_string_literals)
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_reference_qualified_functions)
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_relaxed_constexpr)
#      define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_return_type_deduction)
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 1
#    else
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 1
#    else
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_rvalue_references)
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 1
#    else
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 1
#    else
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_static_assert)
#      define YARP_COMPILER_CXX_STATIC_ASSERT 1
#    else
#      define YARP_COMPILER_CXX_STATIC_ASSERT 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_strong_enums)
#      define YARP_COMPILER_CXX_STRONG_ENUMS 1
#    else
#      define YARP_COMPILER_CXX_STRONG_ENUMS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 199711L
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 1
#    else
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_thread_local)
#      define YARP_COMPILER_CXX_THREAD_LOCAL 1
#    else
#      define YARP_COMPILER_CXX_THREAD_LOCAL 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_trailing_return)
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_unicode_literals)
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_generalized_initializers)
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 1
#    else
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_unrestricted_unions)
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 1
#    else
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_user_literals)
#      define YARP_COMPILER_CXX_USER_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_USER_LITERALS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_variable_templates)
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __cplusplus >= 201103L
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 0
#    endif

#    if ((__clang_major__ * 100) + __clang_minor__) >= 400 && __has_feature(cxx_variadic_templates)
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 0
#    endif

#  elif YARP_COMPILER_IS_MSVC

#    if !(_MSC_VER >= 1600)
#      error Unsupported compiler version
#    endif

  /* _MSC_VER = VVRR */
# define YARP_COMPILER_VERSION_MAJOR (_MSC_VER / 100)
# define YARP_COMPILER_VERSION_MINOR (_MSC_VER % 100)
# if defined(_MSC_FULL_VER)
#  if _MSC_VER >= 1400
    /* _MSC_FULL_VER = VVRRPPPPP */
#   define YARP_COMPILER_VERSION_PATCH (_MSC_FULL_VER % 100000)
#  else
    /* _MSC_FULL_VER = VVRRPPPP */
#   define YARP_COMPILER_VERSION_PATCH (_MSC_FULL_VER % 10000)
#  endif
# endif
# if defined(_MSC_BUILD)
#  define YARP_COMPILER_VERSION_TWEAK (_MSC_BUILD)
# endif

#    if _MSC_FULL_VER >= 190024406
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_AGGREGATE_DEFAULT_INITIALIZERS 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_ALIAS_TEMPLATES 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_ALIGNAS 1
#    else
#      define YARP_COMPILER_CXX_ALIGNAS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_ALIGNOF 1
#    else
#      define YARP_COMPILER_CXX_ALIGNOF 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_ATTRIBUTES 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTES 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 1
#    else
#      define YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_AUTO_TYPE 1
#    else
#      define YARP_COMPILER_CXX_AUTO_TYPE 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_BINARY_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_BINARY_LITERALS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_CONSTEXPR 1
#    else
#      define YARP_COMPILER_CXX_CONSTEXPR 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_CONTEXTUAL_CONVERSIONS 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_DECLTYPE 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 1
#    else
#      define YARP_COMPILER_CXX_DECLTYPE_AUTO 0
#    endif

#    define YARP_COMPILER_CXX_DECLTYPE_INCOMPLETE_RETURN_TYPES 0

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULT_FUNCTION_TEMPLATE_ARGS 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_FUNCTIONS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_DEFAULTED_MOVE_INITIALIZERS 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_DELEGATING_CONSTRUCTORS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_DELETED_FUNCTIONS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 1
#    else
#      define YARP_COMPILER_CXX_DIGIT_SEPARATORS 0
#    endif

#    if _MSC_VER >= 1700
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_ENUM_FORWARD_DECLARATIONS 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 1
#    else
#      define YARP_COMPILER_CXX_EXPLICIT_CONVERSIONS 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 1
#    else
#      define YARP_COMPILER_CXX_EXTENDED_FRIEND_DECLARATIONS 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_EXTERN_TEMPLATES 0
#    endif

#    if _MSC_VER >= 1700
#      define YARP_COMPILER_CXX_FINAL 1
#    else
#      define YARP_COMPILER_CXX_FINAL 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 1
#    else
#      define YARP_COMPILER_CXX_FUNC_IDENTIFIER 0
#    endif

#    if _MSC_FULL_VER >= 180030723
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 1
#    else
#      define YARP_COMPILER_CXX_GENERALIZED_INITIALIZERS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_GENERIC_LAMBDAS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 1
#    else
#      define YARP_COMPILER_CXX_INHERITING_CONSTRUCTORS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 1
#    else
#      define YARP_COMPILER_CXX_INLINE_NAMESPACES 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_LAMBDAS 1
#    else
#      define YARP_COMPILER_CXX_LAMBDAS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 1
#    else
#      define YARP_COMPILER_CXX_LAMBDA_INIT_CAPTURES 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 1
#    else
#      define YARP_COMPILER_CXX_LOCAL_TYPE_TEMPLATE_ARGS 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 1
#    else
#      define YARP_COMPILER_CXX_LONG_LONG_TYPE 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_NOEXCEPT 1
#    else
#      define YARP_COMPILER_CXX_NOEXCEPT 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 1
#    else
#      define YARP_COMPILER_CXX_NONSTATIC_MEMBER_INIT 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_NULLPTR 1
#    else
#      define YARP_COMPILER_CXX_NULLPTR 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_OVERRIDE 1
#    else
#      define YARP_COMPILER_CXX_OVERRIDE 0
#    endif

#    if _MSC_VER >= 1700
#      define YARP_COMPILER_CXX_RANGE_FOR 1
#    else
#      define YARP_COMPILER_CXX_RANGE_FOR 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_RAW_STRING_LITERALS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 1
#    else
#      define YARP_COMPILER_CXX_REFERENCE_QUALIFIED_FUNCTIONS 0
#    endif

#    define YARP_COMPILER_CXX_RELAXED_CONSTEXPR 0

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 1
#    else
#      define YARP_COMPILER_CXX_RETURN_TYPE_DEDUCTION 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 1
#    else
#      define YARP_COMPILER_CXX_RIGHT_ANGLE_BRACKETS 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 1
#    else
#      define YARP_COMPILER_CXX_RVALUE_REFERENCES 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 1
#    else
#      define YARP_COMPILER_CXX_SIZEOF_MEMBER 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_STATIC_ASSERT 1
#    else
#      define YARP_COMPILER_CXX_STATIC_ASSERT 0
#    endif

#    if _MSC_VER >= 1700
#      define YARP_COMPILER_CXX_STRONG_ENUMS 1
#    else
#      define YARP_COMPILER_CXX_STRONG_ENUMS 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 1
#    else
#      define YARP_COMPILER_CXX_TEMPLATE_TEMPLATE_PARAMETERS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_THREAD_LOCAL 1
#    else
#      define YARP_COMPILER_CXX_THREAD_LOCAL 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 1
#    else
#      define YARP_COMPILER_CXX_TRAILING_RETURN_TYPES 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_UNICODE_LITERALS 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 1
#    else
#      define YARP_COMPILER_CXX_UNIFORM_INITIALIZATION 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 1
#    else
#      define YARP_COMPILER_CXX_UNRESTRICTED_UNIONS 0
#    endif

#    if _MSC_VER >= 1900
#      define YARP_COMPILER_CXX_USER_LITERALS 1
#    else
#      define YARP_COMPILER_CXX_USER_LITERALS 0
#    endif

#    if _MSC_FULL_VER >= 190023918
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIABLE_TEMPLATES 0
#    endif

#    if _MSC_VER >= 1600
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_MACROS 0
#    endif

#    if _MSC_VER >= 1800
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 1
#    else
#      define YARP_COMPILER_CXX_VARIADIC_TEMPLATES 0
#    endif

#  else
#    error Unsupported compiler
#  endif

#  if YARP_COMPILER_CXX_ALIGNAS
#    define YARP_ALIGNAS(X) alignas(X)
#  elif YARP_COMPILER_IS_GNU || YARP_COMPILER_IS_Clang || YARP_COMPILER_IS_AppleClang
#    define YARP_ALIGNAS(X) __attribute__ ((__aligned__(X)))
#  elif YARP_COMPILER_IS_MSVC
#    define YARP_ALIGNAS(X) __declspec(align(X))
#  else
#    define YARP_ALIGNAS(X)
#  endif


#  if YARP_COMPILER_CXX_ALIGNOF
#    define YARP_ALIGNOF(X) alignof(X)
#  elif YARP_COMPILER_IS_GNU || YARP_COMPILER_IS_Clang || YARP_COMPILER_IS_AppleClang
#    define YARP_ALIGNOF(X) __alignof__(X)
#  elif YARP_COMPILER_IS_MSVC
#    define YARP_ALIGNOF(X) __alignof(X)
#  endif


#  ifndef YARP_DEPRECATED
#    if YARP_COMPILER_CXX_ATTRIBUTE_DEPRECATED
#      define YARP_DEPRECATED [[deprecated]]
#      define YARP_DEPRECATED_MSG(MSG) [[deprecated(MSG)]]
#    elif YARP_COMPILER_IS_GNU || YARP_COMPILER_IS_Clang
#      define YARP_DEPRECATED __attribute__((__deprecated__))
#      define YARP_DEPRECATED_MSG(MSG) __attribute__((__deprecated__(MSG)))
#    elif YARP_COMPILER_IS_MSVC
#      define YARP_DEPRECATED __declspec(deprecated)
#      define YARP_DEPRECATED_MSG(MSG) __declspec(deprecated(MSG))
#    else
#      define YARP_DEPRECATED
#      define YARP_DEPRECATED_MSG(MSG)
#    endif
#  endif


#  if YARP_COMPILER_CXX_CONSTEXPR
#    define YARP_CONSTEXPR constexpr
#  else
#    define YARP_CONSTEXPR
#  endif


#  if YARP_COMPILER_CXX_DELETED_FUNCTIONS
#    define YARP_DELETED_FUNCTION = delete
#  else
#    define YARP_DELETED_FUNCTION
#  endif


#  if YARP_COMPILER_CXX_EXTERN_TEMPLATES
#    define YARP_EXTERN_TEMPLATE extern
#  else
#    define YARP_EXTERN_TEMPLATE
#  endif


#  if YARP_COMPILER_CXX_FINAL
#    define YARP_FINAL final
#  else
#    define YARP_FINAL
#  endif


#  if YARP_COMPILER_CXX_NOEXCEPT
#    define YARP_NOEXCEPT noexcept
#    define YARP_NOEXCEPT_EXPR(X) noexcept(X)
#  else
#    define YARP_NOEXCEPT
#    define YARP_NOEXCEPT_EXPR(X)
#  endif


#  if YARP_COMPILER_CXX_NULLPTR
#    define YARP_NULLPTR nullptr
#  else
#    define YARP_NULLPTR 0
#  endif


#  if YARP_COMPILER_CXX_OVERRIDE
#    define YARP_OVERRIDE override
#  else
#    define YARP_OVERRIDE
#  endif

#  if YARP_COMPILER_CXX_STATIC_ASSERT
#    define YARP_STATIC_ASSERT(X) static_assert(X, #X)
#    define YARP_STATIC_ASSERT_MSG(X, MSG) static_assert(X, MSG)
#  else
template<bool> struct YARPStaticAssert;
template<> struct YARPStaticAssert<true>{};
#    define YARP_STATIC_ASSERT(X) sizeof(YARPStaticAssert<X>)
#    define YARP_STATIC_ASSERT_MSG(X, MSG) sizeof(YARPStaticAssert<X>)
#  endif


#  if YARP_COMPILER_CXX_THREAD_LOCAL
#    define YARP_THREAD_LOCAL thread_local
#  elif YARP_COMPILER_IS_GNU || YARP_COMPILER_IS_Clang || YARP_COMPILER_IS_AppleClang
#    define YARP_THREAD_LOCAL __thread
#  elif YARP_COMPILER_IS_MSVC
#    define YARP_THREAD_LOCAL __declspec(thread)
#  else
// YARP_THREAD_LOCAL not defined for this configuration.
#  endif

#endif

#endif
