﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNN.Model
{
    public class Node
    {
        private bool activation = false;
        private float activationVal;
        private float nodeVal;
        private float lambda = 6;
        private List<Weight> weights = new List<Weight>();
        private List<float> weightsVals = new List<float>();


        public Node(bool activeNode)
        {
            if (activeNode)
            {
                activation = true;
            }
        }

        public Node(bool activeNode, List<Layer> inputLayers)
        {
            if (activeNode)
            {
                activation = true;
            }

            for (int i = 0; i < inputLayers.Count; i++)
            {
                for (int j = 0; j < inputLayers.ElementAt(i).GetNodes.Count; j++)
                {
                    Weight tmpWeight = new Weight(inputLayers.ElementAt(i).GetNodes.ElementAt(j), 0.0f);
                    weights.Add(tmpWeight);
                    weightsVals.Add(tmpWeight.WeightVal);
                }
            }
        }

        public void activateNode(List<float> inputValues, List<Layer> inputLayers)
        {
            //input values has to be a list containing all values including context and input layer
            if (activation)
            {
                float weightedSum = 0.0f;
                //for (int i = 0; i < inputValues.Count; i++)
                //{
                //    weightedSum = inputValues.ElementAt(i) * weights.ElementAt(i).Weight;

                //}
                //for(int j=0; j<inputValues)
                int counter = 0;

                for (int i = 0; i < inputLayers.Count; i++)
                {
                    for (int j = 0; j < inputLayers.ElementAt(i).GetNodes.Count; j++)
                    {
                        weightedSum += inputLayers.ElementAt(i).GetNodes.ElementAt(j).nodeVal * weights.ElementAt(counter).WeightVal;
                        counter++;
                    }
                }
                activationVal += calculateActivationValue(weightedSum);
                nodeVal = activationVal;
            }
        }

        private float calculateActivationValue(float x)
        {
            float activationValTmp;
            //activation equation
            double val = Convert.ToDouble(x);
            return activationValTmp = (float)(1 / (1 + Math.Exp(-lambda * val)));
        }

        public float calculateActivationDerivative(float x)
        {
            return x * (1 - x);
        }

        public float NodeValue
        {
            get { return nodeVal; }
            set { nodeVal = value; }
        }

        public float GetActivationVal
        {
            get { return activationVal; }
        }

        public List<Weight> GetWeights
        {
            get { return weights; }
        }

        public List<float> WeightsVals
        {
            get { return weightsVals; }
        }
    }
}
