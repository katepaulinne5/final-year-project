//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public class yarp {
  public static void typedReaderMissingCallback() {
    yarpPINVOKE.typedReaderMissingCallback();
  }

  public static int PAD_BYTES(int len, int pad) {
    int ret = yarpPINVOKE.PAD_BYTES(len, pad);
    return ret;
  }

  public new static bool read(ImageRgb dest, string src) {
    bool ret = yarpPINVOKE.read__SWIG_0(ImageRgb.getCPtr(dest), src);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool read(SWIGTYPE_p_yarp__sig__ImageOfT_yarp__sig__PixelBgr_t dest, string src) {
    bool ret = yarpPINVOKE.read__SWIG_1(SWIGTYPE_p_yarp__sig__ImageOfT_yarp__sig__PixelBgr_t.getCPtr(dest), src);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool read(ImageRgba dest, string src) {
    bool ret = yarpPINVOKE.read__SWIG_2(ImageRgba.getCPtr(dest), src);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool read(ImageMono dest, string src) {
    bool ret = yarpPINVOKE.read__SWIG_3(ImageMono.getCPtr(dest), src);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool read(ImageFloat dest, string src) {
    bool ret = yarpPINVOKE.read__SWIG_4(ImageFloat.getCPtr(dest), src);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool write(ImageRgb src, string dest) {
    bool ret = yarpPINVOKE.write__SWIG_0(ImageRgb.getCPtr(src), dest);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool write(SWIGTYPE_p_yarp__sig__ImageOfT_yarp__sig__PixelBgr_t src, string dest) {
    bool ret = yarpPINVOKE.write__SWIG_1(SWIGTYPE_p_yarp__sig__ImageOfT_yarp__sig__PixelBgr_t.getCPtr(src), dest);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool write(ImageRgba src, string dest) {
    bool ret = yarpPINVOKE.write__SWIG_2(ImageRgba.getCPtr(src), dest);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool write(ImageMono src, string dest) {
    bool ret = yarpPINVOKE.write__SWIG_3(ImageMono.getCPtr(src), dest);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool write(ImageFloat src, string dest) {
    bool ret = yarpPINVOKE.write__SWIG_4(ImageFloat.getCPtr(src), dest);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public new static bool write(Image src, string dest) {
    bool ret = yarpPINVOKE.write__SWIG_5(Image.getCPtr(src), dest);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static bool submatrix(Matrix arg0, Matrix arg1, int r1, int r2, int c1, int c2) {
    bool ret = yarpPINVOKE.submatrix(Matrix.getCPtr(arg0), Matrix.getCPtr(arg1), r1, r2, c1, c2);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static bool removeCols(Matrix arg0, Matrix arg1, int first_col, int how_many) {
    bool ret = yarpPINVOKE.removeCols(Matrix.getCPtr(arg0), Matrix.getCPtr(arg1), first_col, how_many);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static bool removeRows(Matrix arg0, Matrix arg1, int first_row, int how_many) {
    bool ret = yarpPINVOKE.removeRows(Matrix.getCPtr(arg0), Matrix.getCPtr(arg1), first_row, how_many);
    if (yarpPINVOKE.SWIGPendingException.Pending) throw yarpPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static readonly int YARP_CONSTSTRING_IS_STD_STRING = yarpPINVOKE.YARP_CONSTSTRING_IS_STD_STRING_get();
  public static readonly int BOTTLE_TAG_INT = yarpPINVOKE.BOTTLE_TAG_INT_get();
  public static readonly int BOTTLE_TAG_VOCAB = yarpPINVOKE.BOTTLE_TAG_VOCAB_get();
  public static readonly int BOTTLE_TAG_DOUBLE = yarpPINVOKE.BOTTLE_TAG_DOUBLE_get();
  public static readonly int BOTTLE_TAG_STRING = yarpPINVOKE.BOTTLE_TAG_STRING_get();
  public static readonly int BOTTLE_TAG_BLOB = yarpPINVOKE.BOTTLE_TAG_BLOB_get();
  public static readonly int BOTTLE_TAG_INT64 = yarpPINVOKE.BOTTLE_TAG_INT64_get();
  public static readonly int BOTTLE_TAG_LIST = yarpPINVOKE.BOTTLE_TAG_LIST_get();
  public static readonly int BOTTLE_TAG_DICT = yarpPINVOKE.BOTTLE_TAG_DICT_get();
  public static readonly int FORMAT_NULL = yarpPINVOKE.FORMAT_NULL_get();
  public static readonly int FORMAT_ANY = yarpPINVOKE.FORMAT_ANY_get();
  public static readonly int FORMAT_PGM = yarpPINVOKE.FORMAT_PGM_get();
  public static readonly int FORMAT_PPM = yarpPINVOKE.FORMAT_PPM_get();
  public static readonly int FORMAT_NUMERIC = yarpPINVOKE.FORMAT_NUMERIC_get();

  public static readonly int VOCAB_BRIGHTNESS = yarpPINVOKE.VOCAB_BRIGHTNESS_get();
  public static readonly int VOCAB_EXPOSURE = yarpPINVOKE.VOCAB_EXPOSURE_get();
  public static readonly int VOCAB_SHARPNESS = yarpPINVOKE.VOCAB_SHARPNESS_get();
  public static readonly int VOCAB_WHITE = yarpPINVOKE.VOCAB_WHITE_get();
  public static readonly int VOCAB_HUE = yarpPINVOKE.VOCAB_HUE_get();
  public static readonly int VOCAB_SATURATION = yarpPINVOKE.VOCAB_SATURATION_get();
  public static readonly int VOCAB_GAMMA = yarpPINVOKE.VOCAB_GAMMA_get();
  public static readonly int VOCAB_SHUTTER = yarpPINVOKE.VOCAB_SHUTTER_get();
  public static readonly int VOCAB_GAIN = yarpPINVOKE.VOCAB_GAIN_get();
  public static readonly int VOCAB_IRIS = yarpPINVOKE.VOCAB_IRIS_get();
  public static readonly int VOCAB_SET = yarpPINVOKE.VOCAB_SET_get();
  public static readonly int VOCAB_GET = yarpPINVOKE.VOCAB_GET_get();
  public static readonly int VOCAB_IS = yarpPINVOKE.VOCAB_IS_get();
  public static readonly int VOCAB_WIDTH = yarpPINVOKE.VOCAB_WIDTH_get();
  public static readonly int VOCAB_HEIGHT = yarpPINVOKE.VOCAB_HEIGHT_get();
  public static readonly int VOCAB_DRHASFEA = yarpPINVOKE.VOCAB_DRHASFEA_get();
  public static readonly int VOCAB_DRSETVAL = yarpPINVOKE.VOCAB_DRSETVAL_get();
  public static readonly int VOCAB_DRGETVAL = yarpPINVOKE.VOCAB_DRGETVAL_get();
  public static readonly int VOCAB_DRHASACT = yarpPINVOKE.VOCAB_DRHASACT_get();
  public static readonly int VOCAB_DRSETACT = yarpPINVOKE.VOCAB_DRSETACT_get();
  public static readonly int VOCAB_DRGETACT = yarpPINVOKE.VOCAB_DRGETACT_get();
  public static readonly int VOCAB_DRHASMAN = yarpPINVOKE.VOCAB_DRHASMAN_get();
  public static readonly int VOCAB_DRHASAUT = yarpPINVOKE.VOCAB_DRHASAUT_get();
  public static readonly int VOCAB_DRHASONP = yarpPINVOKE.VOCAB_DRHASONP_get();
  public static readonly int VOCAB_DRSETMOD = yarpPINVOKE.VOCAB_DRSETMOD_get();
  public static readonly int VOCAB_DRGETMOD = yarpPINVOKE.VOCAB_DRGETMOD_get();
  public static readonly int VOCAB_DRSETONP = yarpPINVOKE.VOCAB_DRSETONP_get();
  public static readonly int VOCAB_DRGETMSK = yarpPINVOKE.VOCAB_DRGETMSK_get();
  public static readonly int VOCAB_DRGETVMD = yarpPINVOKE.VOCAB_DRGETVMD_get();
  public static readonly int VOCAB_DRSETVMD = yarpPINVOKE.VOCAB_DRSETVMD_get();
  public static readonly int VOCAB_DRGETFPM = yarpPINVOKE.VOCAB_DRGETFPM_get();
  public static readonly int VOCAB_DRGETFPS = yarpPINVOKE.VOCAB_DRGETFPS_get();
  public static readonly int VOCAB_DRSETFPS = yarpPINVOKE.VOCAB_DRSETFPS_get();
  public static readonly int VOCAB_DRGETISO = yarpPINVOKE.VOCAB_DRGETISO_get();
  public static readonly int VOCAB_DRSETISO = yarpPINVOKE.VOCAB_DRSETISO_get();
  public static readonly int VOCAB_DRGETCCM = yarpPINVOKE.VOCAB_DRGETCCM_get();
  public static readonly int VOCAB_DRGETCOD = yarpPINVOKE.VOCAB_DRGETCOD_get();
  public static readonly int VOCAB_DRSETCOD = yarpPINVOKE.VOCAB_DRSETCOD_get();
  public static readonly int VOCAB_DRSETWHB = yarpPINVOKE.VOCAB_DRSETWHB_get();
  public static readonly int VOCAB_DRGETWHB = yarpPINVOKE.VOCAB_DRGETWHB_get();
  public static readonly int VOCAB_DRGETF7M = yarpPINVOKE.VOCAB_DRGETF7M_get();
  public static readonly int VOCAB_DRGETWF7 = yarpPINVOKE.VOCAB_DRGETWF7_get();
  public static readonly int VOCAB_DRSETWF7 = yarpPINVOKE.VOCAB_DRSETWF7_get();
  public static readonly int VOCAB_DRSETOPM = yarpPINVOKE.VOCAB_DRSETOPM_get();
  public static readonly int VOCAB_DRGETOPM = yarpPINVOKE.VOCAB_DRGETOPM_get();
  public static readonly int VOCAB_DRSETTXM = yarpPINVOKE.VOCAB_DRSETTXM_get();
  public static readonly int VOCAB_DRGETTXM = yarpPINVOKE.VOCAB_DRGETTXM_get();
  public static readonly int VOCAB_DRSETBCS = yarpPINVOKE.VOCAB_DRSETBCS_get();
  public static readonly int VOCAB_DRSETDEF = yarpPINVOKE.VOCAB_DRSETDEF_get();
  public static readonly int VOCAB_DRSETRST = yarpPINVOKE.VOCAB_DRSETRST_get();
  public static readonly int VOCAB_DRSETPWR = yarpPINVOKE.VOCAB_DRSETPWR_get();
  public static readonly int VOCAB_DRSETCAP = yarpPINVOKE.VOCAB_DRSETCAP_get();
  public static readonly int VOCAB_DRSETBPP = yarpPINVOKE.VOCAB_DRSETBPP_get();
  public static readonly int VOCAB_DRGETBPP = yarpPINVOKE.VOCAB_DRGETBPP_get();
  public static readonly int VOCAB_CALIBRATE_JOINT = yarpPINVOKE.VOCAB_CALIBRATE_JOINT_get();
  public static readonly int VOCAB_CALIBRATE_JOINT_PARAMS = yarpPINVOKE.VOCAB_CALIBRATE_JOINT_PARAMS_get();
  public static readonly int VOCAB_CALIBRATE = yarpPINVOKE.VOCAB_CALIBRATE_get();
  public static readonly int VOCAB_ABORTCALIB = yarpPINVOKE.VOCAB_ABORTCALIB_get();
  public static readonly int VOCAB_ABORTPARK = yarpPINVOKE.VOCAB_ABORTPARK_get();
  public static readonly int VOCAB_CALIBRATE_DONE = yarpPINVOKE.VOCAB_CALIBRATE_DONE_get();
  public static readonly int VOCAB_PARK = yarpPINVOKE.VOCAB_PARK_get();
  public static readonly int VOCAB_FAILED = yarpPINVOKE.VOCAB_FAILED_get();
  public static readonly int VOCAB_OK = yarpPINVOKE.VOCAB_OK_get();
  public static readonly int VOCAB_OFFSET = yarpPINVOKE.VOCAB_OFFSET_get();
  public static readonly int VOCAB_PID = yarpPINVOKE.VOCAB_PID_get();
  public static readonly int VOCAB_PIDS = yarpPINVOKE.VOCAB_PIDS_get();
  public static readonly int VOCAB_REF = yarpPINVOKE.VOCAB_REF_get();
  public static readonly int VOCAB_REFS = yarpPINVOKE.VOCAB_REFS_get();
  public static readonly int VOCAB_REFG = yarpPINVOKE.VOCAB_REFG_get();
  public static readonly int VOCAB_LIM = yarpPINVOKE.VOCAB_LIM_get();
  public static readonly int VOCAB_LIMS = yarpPINVOKE.VOCAB_LIMS_get();
  public static readonly int VOCAB_RESET = yarpPINVOKE.VOCAB_RESET_get();
  public static readonly int VOCAB_DISABLE = yarpPINVOKE.VOCAB_DISABLE_get();
  public static readonly int VOCAB_ENABLE = yarpPINVOKE.VOCAB_ENABLE_get();
  public static readonly int VOCAB_ERR = yarpPINVOKE.VOCAB_ERR_get();
  public static readonly int VOCAB_ERRS = yarpPINVOKE.VOCAB_ERRS_get();
  public static readonly int VOCAB_OUTPUT = yarpPINVOKE.VOCAB_OUTPUT_get();
  public static readonly int VOCAB_OUTPUTS = yarpPINVOKE.VOCAB_OUTPUTS_get();
  public static readonly int VOCAB_REFERENCE = yarpPINVOKE.VOCAB_REFERENCE_get();
  public static readonly int VOCAB_REFERENCES = yarpPINVOKE.VOCAB_REFERENCES_get();
  public static readonly int VOCAB_AXES = yarpPINVOKE.VOCAB_AXES_get();
  public static readonly int VOCAB_MOTION_DONE = yarpPINVOKE.VOCAB_MOTION_DONE_get();
  public static readonly int VOCAB_MOTION_DONES = yarpPINVOKE.VOCAB_MOTION_DONES_get();
  public static readonly int VOCAB_POSITION_MODE = yarpPINVOKE.VOCAB_POSITION_MODE_get();
  public static readonly int VOCAB_POSITION_MOVE = yarpPINVOKE.VOCAB_POSITION_MOVE_get();
  public static readonly int VOCAB_POSITION_MOVES = yarpPINVOKE.VOCAB_POSITION_MOVES_get();
  public static readonly int VOCAB_RELATIVE_MOVE = yarpPINVOKE.VOCAB_RELATIVE_MOVE_get();
  public static readonly int VOCAB_RELATIVE_MOVES = yarpPINVOKE.VOCAB_RELATIVE_MOVES_get();
  public static readonly int VOCAB_REF_SPEED = yarpPINVOKE.VOCAB_REF_SPEED_get();
  public static readonly int VOCAB_REF_SPEEDS = yarpPINVOKE.VOCAB_REF_SPEEDS_get();
  public static readonly int VOCAB_REF_ACCELERATION = yarpPINVOKE.VOCAB_REF_ACCELERATION_get();
  public static readonly int VOCAB_REF_ACCELERATIONS = yarpPINVOKE.VOCAB_REF_ACCELERATIONS_get();
  public static readonly int VOCAB_STOP = yarpPINVOKE.VOCAB_STOP_get();
  public static readonly int VOCAB_STOPS = yarpPINVOKE.VOCAB_STOPS_get();
  public static readonly int VOCAB_VELOCITY_MODE = yarpPINVOKE.VOCAB_VELOCITY_MODE_get();
  public static readonly int VOCAB_VELOCITY_MOVE = yarpPINVOKE.VOCAB_VELOCITY_MOVE_get();
  public static readonly int VOCAB_VELOCITY_MOVES = yarpPINVOKE.VOCAB_VELOCITY_MOVES_get();
  public static readonly int VOCAB_AMP_ENABLE = yarpPINVOKE.VOCAB_AMP_ENABLE_get();
  public static readonly int VOCAB_AMP_DISABLE = yarpPINVOKE.VOCAB_AMP_DISABLE_get();
  public static readonly int VOCAB_AMP_STATUS = yarpPINVOKE.VOCAB_AMP_STATUS_get();
  public static readonly int VOCAB_AMP_STATUS_SINGLE = yarpPINVOKE.VOCAB_AMP_STATUS_SINGLE_get();
  public static readonly int VOCAB_AMP_CURRENT = yarpPINVOKE.VOCAB_AMP_CURRENT_get();
  public static readonly int VOCAB_AMP_CURRENTS = yarpPINVOKE.VOCAB_AMP_CURRENTS_get();
  public static readonly int VOCAB_AMP_MAXCURRENT = yarpPINVOKE.VOCAB_AMP_MAXCURRENT_get();
  public static readonly int VOCAB_AMP_NOMINAL_CURRENT = yarpPINVOKE.VOCAB_AMP_NOMINAL_CURRENT_get();
  public static readonly int VOCAB_AMP_PEAK_CURRENT = yarpPINVOKE.VOCAB_AMP_PEAK_CURRENT_get();
  public static readonly int VOCAB_AMP_PWM = yarpPINVOKE.VOCAB_AMP_PWM_get();
  public static readonly int VOCAB_AMP_PWM_LIMIT = yarpPINVOKE.VOCAB_AMP_PWM_LIMIT_get();
  public static readonly int VOCAB_AMP_VOLTAGE_SUPPLY = yarpPINVOKE.VOCAB_AMP_VOLTAGE_SUPPLY_get();
  public static readonly int VOCAB_LIMITS = yarpPINVOKE.VOCAB_LIMITS_get();
  public static readonly int VOCAB_OPENLOOP_MODE = yarpPINVOKE.VOCAB_OPENLOOP_MODE_get();
  public static readonly int VOCAB_INFO_NAME = yarpPINVOKE.VOCAB_INFO_NAME_get();
  public static readonly int VOCAB_INFO_TYPE = yarpPINVOKE.VOCAB_INFO_TYPE_get();
  public static readonly int VOCAB_TIMESTAMP = yarpPINVOKE.VOCAB_TIMESTAMP_get();
  public static readonly int VOCAB_TORQUE = yarpPINVOKE.VOCAB_TORQUE_get();
  public static readonly int VOCAB_TORQUE_MODE = yarpPINVOKE.VOCAB_TORQUE_MODE_get();
  public static readonly int VOCAB_TRQS = yarpPINVOKE.VOCAB_TRQS_get();
  public static readonly int VOCAB_TRQ = yarpPINVOKE.VOCAB_TRQ_get();
  public static readonly int VOCAB_BEMF = yarpPINVOKE.VOCAB_BEMF_get();
  public static readonly int VOCAB_MOTOR_PARAMS = yarpPINVOKE.VOCAB_MOTOR_PARAMS_get();
  public static readonly int VOCAB_RANGES = yarpPINVOKE.VOCAB_RANGES_get();
  public static readonly int VOCAB_RANGE = yarpPINVOKE.VOCAB_RANGE_get();
  public static readonly int VOCAB_IMP_PARAM = yarpPINVOKE.VOCAB_IMP_PARAM_get();
  public static readonly int VOCAB_IMP_OFFSET = yarpPINVOKE.VOCAB_IMP_OFFSET_get();
  public static readonly int VOCAB_TORQUES_DIRECTS = yarpPINVOKE.VOCAB_TORQUES_DIRECTS_get();
  public static readonly int VOCAB_TORQUES_DIRECT = yarpPINVOKE.VOCAB_TORQUES_DIRECT_get();
  public static readonly int VOCAB_TORQUES_DIRECT_GROUP = yarpPINVOKE.VOCAB_TORQUES_DIRECT_GROUP_get();
  public static readonly int VOCAB_PROTOCOL_VERSION = yarpPINVOKE.VOCAB_PROTOCOL_VERSION_get();
  public static readonly int VOCAB_E_RESET = yarpPINVOKE.VOCAB_E_RESET_get();
  public static readonly int VOCAB_E_RESETS = yarpPINVOKE.VOCAB_E_RESETS_get();
  public static readonly int VOCAB_ENCODER = yarpPINVOKE.VOCAB_ENCODER_get();
  public static readonly int VOCAB_ENCODERS = yarpPINVOKE.VOCAB_ENCODERS_get();
  public static readonly int VOCAB_ENCODER_SPEED = yarpPINVOKE.VOCAB_ENCODER_SPEED_get();
  public static readonly int VOCAB_ENCODER_SPEEDS = yarpPINVOKE.VOCAB_ENCODER_SPEEDS_get();
  public static readonly int VOCAB_ENCODER_ACCELERATION = yarpPINVOKE.VOCAB_ENCODER_ACCELERATION_get();
  public static readonly int VOCAB_ENCODER_ACCELERATIONS = yarpPINVOKE.VOCAB_ENCODER_ACCELERATIONS_get();
  public static readonly int VOCAB_REMOTE_CALIBRATOR_INTERFACE = yarpPINVOKE.VOCAB_REMOTE_CALIBRATOR_INTERFACE_get();
  public static readonly int VOCAB_IS_CALIBRATOR_PRESENT = yarpPINVOKE.VOCAB_IS_CALIBRATOR_PRESENT_get();
  public static readonly int VOCAB_CALIBRATE_SINGLE_JOINT = yarpPINVOKE.VOCAB_CALIBRATE_SINGLE_JOINT_get();
  public static readonly int VOCAB_CALIBRATE_WHOLE_PART = yarpPINVOKE.VOCAB_CALIBRATE_WHOLE_PART_get();
  public static readonly int VOCAB_HOMING_SINGLE_JOINT = yarpPINVOKE.VOCAB_HOMING_SINGLE_JOINT_get();
  public static readonly int VOCAB_HOMING_WHOLE_PART = yarpPINVOKE.VOCAB_HOMING_WHOLE_PART_get();
  public static readonly int VOCAB_PARK_SINGLE_JOINT = yarpPINVOKE.VOCAB_PARK_SINGLE_JOINT_get();
  public static readonly int VOCAB_PARK_WHOLE_PART = yarpPINVOKE.VOCAB_PARK_WHOLE_PART_get();
  public static readonly int VOCAB_QUIT_CALIBRATE = yarpPINVOKE.VOCAB_QUIT_CALIBRATE_get();
  public static readonly int VOCAB_QUIT_PARK = yarpPINVOKE.VOCAB_QUIT_PARK_get();
  public static readonly int VOCAB_ICONTROLMODE = yarpPINVOKE.VOCAB_ICONTROLMODE_get();
  public static readonly int VOCAB_CM_CONTROL_MODE = yarpPINVOKE.VOCAB_CM_CONTROL_MODE_get();
  public static readonly int VOCAB_CM_CONTROL_MODE_GROUP = yarpPINVOKE.VOCAB_CM_CONTROL_MODE_GROUP_get();
  public static readonly int VOCAB_CM_CONTROL_MODES = yarpPINVOKE.VOCAB_CM_CONTROL_MODES_get();
  public static readonly int VOCAB_CM_IDLE = yarpPINVOKE.VOCAB_CM_IDLE_get();
  public static readonly int VOCAB_CM_TORQUE = yarpPINVOKE.VOCAB_CM_TORQUE_get();
  public static readonly int VOCAB_CM_POSITION = yarpPINVOKE.VOCAB_CM_POSITION_get();
  public static readonly int VOCAB_CM_POSITION_DIRECT = yarpPINVOKE.VOCAB_CM_POSITION_DIRECT_get();
  public static readonly int VOCAB_CM_VELOCITY = yarpPINVOKE.VOCAB_CM_VELOCITY_get();
  public static readonly int VOCAB_CM_OPENLOOP = yarpPINVOKE.VOCAB_CM_OPENLOOP_get();
  public static readonly int VOCAB_CM_IMPEDANCE_POS = yarpPINVOKE.VOCAB_CM_IMPEDANCE_POS_get();
  public static readonly int VOCAB_CM_IMPEDANCE_VEL = yarpPINVOKE.VOCAB_CM_IMPEDANCE_VEL_get();
  public static readonly int VOCAB_CM_UNKNOWN = yarpPINVOKE.VOCAB_CM_UNKNOWN_get();
  public static readonly int VOCAB_IMPEDANCE = yarpPINVOKE.VOCAB_IMPEDANCE_get();
  public static readonly int VOCAB_POSITION = yarpPINVOKE.VOCAB_POSITION_get();
  public static readonly int VOCAB_VELOCITY = yarpPINVOKE.VOCAB_VELOCITY_get();
  public static readonly int VOCAB_IOPENLOOP = yarpPINVOKE.VOCAB_IOPENLOOP_get();
  public static readonly int VOCAB_OPENLOOP_INTERFACE = yarpPINVOKE.VOCAB_OPENLOOP_INTERFACE_get();
  public static readonly int VOCAB_OPENLOOP_REF_OUTPUT = yarpPINVOKE.VOCAB_OPENLOOP_REF_OUTPUT_get();
  public static readonly int VOCAB_OPENLOOP_REF_OUTPUTS = yarpPINVOKE.VOCAB_OPENLOOP_REF_OUTPUTS_get();
  public static readonly int VOCAB_OPENLOOP_PWM_OUTPUT = yarpPINVOKE.VOCAB_OPENLOOP_PWM_OUTPUT_get();
  public static readonly int VOCAB_OPENLOOP_PWM_OUTPUTS = yarpPINVOKE.VOCAB_OPENLOOP_PWM_OUTPUTS_get();
}
