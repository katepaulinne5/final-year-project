//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public class CalibrationParameters : global::System.IDisposable {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal CalibrationParameters(global::System.IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(CalibrationParameters obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~CalibrationParameters() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          yarpPINVOKE.delete_CalibrationParameters(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
    }
  }

  public uint type {
    set {
      yarpPINVOKE.CalibrationParameters_type_set(swigCPtr, value);
    } 
    get {
      uint ret = yarpPINVOKE.CalibrationParameters_type_get(swigCPtr);
      return ret;
    } 
  }

  public double param1 {
    set {
      yarpPINVOKE.CalibrationParameters_param1_set(swigCPtr, value);
    } 
    get {
      double ret = yarpPINVOKE.CalibrationParameters_param1_get(swigCPtr);
      return ret;
    } 
  }

  public double param2 {
    set {
      yarpPINVOKE.CalibrationParameters_param2_set(swigCPtr, value);
    } 
    get {
      double ret = yarpPINVOKE.CalibrationParameters_param2_get(swigCPtr);
      return ret;
    } 
  }

  public double param3 {
    set {
      yarpPINVOKE.CalibrationParameters_param3_set(swigCPtr, value);
    } 
    get {
      double ret = yarpPINVOKE.CalibrationParameters_param3_get(swigCPtr);
      return ret;
    } 
  }

  public double param4 {
    set {
      yarpPINVOKE.CalibrationParameters_param4_set(swigCPtr, value);
    } 
    get {
      double ret = yarpPINVOKE.CalibrationParameters_param4_get(swigCPtr);
      return ret;
    } 
  }

  public double param5 {
    set {
      yarpPINVOKE.CalibrationParameters_param5_set(swigCPtr, value);
    } 
    get {
      double ret = yarpPINVOKE.CalibrationParameters_param5_get(swigCPtr);
      return ret;
    } 
  }

  public double paramZero {
    set {
      yarpPINVOKE.CalibrationParameters_paramZero_set(swigCPtr, value);
    } 
    get {
      double ret = yarpPINVOKE.CalibrationParameters_paramZero_get(swigCPtr);
      return ret;
    } 
  }

  public CalibrationParameters() : this(yarpPINVOKE.new_CalibrationParameters(), true) {
  }

}
