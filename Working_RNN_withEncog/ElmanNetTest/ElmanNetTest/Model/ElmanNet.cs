﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNN.Model
{
    public struct TrainingParams
    {
        public int lambda;
        public float learningRate;
        public float trainingErrorVal;
        public float testingErrorVal;
        public float validationErrorVal;

        public TrainingParams(int l, float learnRate, float trErr, float tstErr, float validErr)
        {
            lambda = l;
            learningRate = learnRate;
            trainingErrorVal = trErr;
            testingErrorVal = tstErr;
            validationErrorVal = validErr;
        }
    }

    public class ElmanNet
    {
        private List<Layer> layers = new List<Layer>();
        //private List<int> connections = new List<int>();
        //input -> hidden
        //hidden -> context
        //context -> hidden
        //hidden -> output
        private List<LayerConnection> connections = new List<LayerConnection>();
        private int layerNo;
        private Dictionary<WeightType, List<float>> weights = new Dictionary<WeightType, List<float>>();
        private List<List<float>> spoofInputData = new List<List<float>>();
        private List<List<float>> spoofTargets = new List<List<float>>();
        private List<float> terminationData = new List<float>();
        private string netName;
        private int hiddenNodesNo;
        private int maxIterations = 4000; //make it const later (?)
        private float trainingError = 0.0f;
        private TrainingParams trainingParameters;
        private float iterationError = 0.0f;


        public ElmanNet(int numberOfLayers, int hiddenUnits, string name)
        {
            netName = name;
            hiddenNodesNo = hiddenUnits;
            trainingParameters = new TrainingParams();
            trainingParameters.lambda = 1;
            trainingParameters.learningRate = 0.2f;
            CreateData();
            //to setup network call the constructor and then create layers, 
            //this will setup layers and connections expected for Elam NN 
            layerNo = numberOfLayers;
            //List<float> lst=new List<float>();
            //lst.Add(1.0f);
            //create network's layers
            //Layer l=new Layer(2,"Input",)
            CreateLayers();
            //for(int i=0; i<)
            //layers.First(x => x.LayerName == "Input").GetNodes.ElementAt(0).SetNodeVal = spoofInputData.ElementAt(0).ElementAt(0);
            //copyActivationToContext(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Context));

        }

        public ElmanNet(int numberOfLayers, int hiddenUnits, string name, List<List<float>> input, List<List<float>> output)
        {
            netName = name;
            hiddenNodesNo = hiddenUnits;
            trainingParameters = new TrainingParams();
            trainingParameters.lambda = 1;
            trainingParameters.learningRate = 0.2f;
            spoofInputData = input;
            spoofTargets = output;
            //CreateData();
            //to setup network call the constructor and then create layers, 
            //this will setup layers and connections expected for Elam NN 
            layerNo = numberOfLayers;
            //this will needs inputs and outputs to be passed into the method
            CreateLayers(input.ElementAt(0).Count - 1, output.ElementAt(0).Count);
            //layers.First(x => x.LayerName == "Input").GetNodes.ElementAt(0).SetNodeVal = spoofInputData.ElementAt(0).ElementAt(0);
            //copyActivationToContext(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Context));

        }

        public void copyActivationToContext(Layer hidden, Layer context)
        {
            context.SetInputValues = hidden.GetActivationValues;
            context.setNodeValues(hidden.GetActivationValues);
        }

        public void Train(ref List<float> errorTrain)
        {
            int iteration = 0;
            bool stopTraining = false;
            int iterationCount = 0;

            //maxIterations = spoofInputData.Count - 1;
            //randomize weights (?) or use the assigned ones - 0

            //continue training 
            while (!stopTraining)
            {
                
                for (int iter2=0; iter2 < 4; iter2++)
                {
                    List<float> errorList = new List<float>();
                    for(int i=0;i<hiddenNodesNo;i++)
                    {
                        layers.First(x => x.LayerType == LayerType.Context).GetNodes.ElementAt(i).NodeValue = 0.0f;
                    }
                    for (int iter = 0; iter < 2; iter++)
                    {
                        //set input nodes values 
                        //for (int i = 0; i < spoofInputData.ElementAt(iteration).Count; i++)
                        for (int i = 0; i < layers.First(x => x.LayerType == LayerType.Input).GetNodes.Count; i++)
                        {
                            //for (int j = 0; j < layers.First(x => x.LayerType == LayerType.Input).GetNodes.Count; j++)
                            //{

                            layers.First(x => x.LayerType == LayerType.Input).GetNodes.ElementAt(i).NodeValue = spoofInputData.ElementAt(iter2).ElementAt(iter);
                            float val = spoofInputData.ElementAt(iter2).ElementAt(i);
                            float valTmp = layers.First(x => x.LayerType == LayerType.Input).GetNodes.ElementAt(i).NodeValue;
                            //}
                        }

                        List<float> errOutputLayer = new List<float>();
                        Layer inL = layers.First(x => x.LayerType == LayerType.Input);
                        Layer conL = layers.First(x => x.LayerType == LayerType.Context);
                        Layer hidL = layers.First(x => x.LayerType == LayerType.Hidden);
                        Layer outL = layers.First(x => x.LayerType == LayerType.Output);
                        int idxI = layers.IndexOf(inL);
                        int idxH = layers.IndexOf(hidL);
                        int idxO = layers.IndexOf(outL);

                        //feedforward -  calculate activations from input to hidden and hidden to output
                        //below needs to be done twice for both sets of layers (up to err calc)
                        List<float> sumActiv = new List<float>();
                        List<float> sumActivHid = new List<float>();
                        //calculate activation values from input and context to hidden
                        //sumActivHid = CalculateSumActivations(layers.First(x => x.LayerType == LayerType.Input), layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Context));
                        //layers.First(x => x.LayerType == LayerType.Hidden).setNodeValues(sumActivHid);
                        //calculate activation values from hidden to output
                        //sumActiv = CalculateSumActivations(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Output));

                        sumActivHid = CalculateSumActivations(inL, ref hidL, conL);
                        sumActiv = CalculateSumActivations(hidL, ref outL);


                        //layers.First(x => x.LayerType == LayerType.Hidden).setNodeValues(sumActivHid);
                        //}
                        //layers.First(x => x.LayerType == LayerType.Output).setNodeValues(sumActiv);

                        layers[idxI] = inL;
                        layers[idxH] = hidL;
                        layers[idxO] = outL;


                        for (int i = 0; i < sumActiv.Count; i++)
                        {
                            //sumActiv[i] = layers.First(x => x.LayerType == LayerType.Output).calculateActivationDerivative(sumActiv.ElementAt(i));
                        }

                        //calc error
                        trainingError = 0.0f;
                        //for (int i = 0; i < layers.First(x => x.LayerType == LayerType.Output).GetNodes.Count; i++)
                        //{
                        for (int j = 0; j < sumActiv.Count; j++)
                        {
                            //wrong indices
                            //trainingError += (float)Math.Sqrt(spoofTargets.ElementAt(iteration).ElementAt(j) - sumActiv.ElementAt(j));
                            trainingError += Math.Abs(spoofTargets.ElementAt(iter2).ElementAt(j) - sumActiv.ElementAt(j));
                        }
                        //}
                        trainingError = 0.5f * trainingError;
                        errorList.Add(trainingError);
                        iterationError += trainingError;

                        //backprop
                        //below needs to be done twice for both sets of layers
                        //List<float> errOutputLayer = new List<float>();
                        //errOutputLayer=updateWeights(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Output), iteration);
                        //updateWeights(layers.First(x => x.LayerType == LayerType.Input), layers.First(x => x.LayerType == LayerType.Hidden), iteration, errOutputLayer, layers.First(x=>x.LayerType==LayerType.Output)/*,layers.First(x=>x.LayerType==LayerType.Context)*/);
                        inL = layers.First(x => x.LayerType == LayerType.Input);
                        hidL = layers.First(x => x.LayerType == LayerType.Hidden);
                        outL = layers.First(x => x.LayerType == LayerType.Output);
                        
                        errOutputLayer = updateWeights(ref hidL, ref outL, iter2);
                        updateWeights(ref inL, ref hidL, iter2, errOutputLayer, outL/*,layers.First(x=>x.LayerType==LayerType.Context)*/);

                        layers[idxI] = inL;
                        layers[idxH] = hidL;
                        layers[idxO] = outL;

                        copyActivationToContext(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Context));
                       // layers.First(x => x.LayerType == LayerType.Context).setNodeValues(sumActivHid);

                        //check max iterations reached
                        if (iterationCount >= maxIterations)
                        {
                            stopTraining = true;
                        }

                        //iteration = iteration + 1;

                        //if (iteration > 3)
                        //{
                        //    iteration = 0;
                        //    errorList.Add(iterationError);
                        //    iterationError = 0.0f;
                        //}

                    }
                    errorList.Add(iterationError/2);
                    iteration = iteration + 1;

                    if (iteration > 3)
                    {
                        iteration = 0;
                        //errorList.Add(iterationError);
                        //errorTrain.Add(iterationError);
                        errorTrain.Add(errorList.Sum()/4);
                        iterationError = 0.0f;
                    }
                }
                
                iterationCount = iterationCount + 1;
                //errorTrain.Add(iterationError);
                //errorTrain.Add(errorList.Sum());
            }

        }

        public List<float> Test(List<float> inputs)
        {

            List<float> sumActiv = new List<float>();
            List<float> sumActivHid = new List<float>();
            for (int i = 0; i < hiddenNodesNo; i++)
            {
                layers.First(x => x.LayerType == LayerType.Context).GetNodes.ElementAt(i).NodeValue = 0.0f;
            }
            for (int i = 0; i < inputs.Count; i++)
            {
                layers.First(x => x.LayerType == LayerType.Input).GetNodes.ElementAt(0).NodeValue = inputs.ElementAt(i);
                
                //calculate activation values from input and context to hidden
                //sumActivHid = CalculateSumActivations(layers.First(x => x.LayerType == LayerType.Input), layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Context));
                //layers.First(x => x.LayerType == LayerType.Hidden).setNodeValues(sumActivHid);
                //calculate activation values from hidden to output
                //sumActiv = CalculateSumActivations(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Output));

                //layers.First(x => x.LayerType == LayerType.Hidden).setNodeValues(sumActivHid);

                //layers.First(x => x.LayerType == LayerType.Output).setNodeValues(sumActiv);

                //layers.First(x => x.LayerType == LayerType.Context).setNodeValues(sumActivHid);
                //copyActivationToContext(layers.First(x => x.LayerType == LayerType.Hidden), layers.First(x => x.LayerType == LayerType.Context));

            }

            return sumActiv;
        }

        //private List<float> CalculateSumActivations(int outputNodes,List<float> input, List<Weight> weightsInput, List<float> context = null, List<Weight> weightsContext = null)
        private List<float> CalculateSumActivations(Layer input, ref Layer output, Layer context = null)
        {

            List<float> activationOutput = new List<float>();
            //float sumActivations = 0.0f;
            for (int i = 0; i < output.GetNodes.Count; i++)
            {
                float sumActivations = 0.0f;
                for (int j = 0; j < input.GetNodes.Count; j++)
                {
                    sumActivations += input.GetNodes.ElementAt(j).NodeValue * output.GetNodes.ElementAt(i).GetWeights.ElementAt(j).WeightVal;
                }

                if (context != null)
                {
                    for (int j = 0; j < context.GetNodes.Count; j++)
                    {
                        sumActivations += context.GetNodes.ElementAt(j).NodeValue * output.GetNodes.ElementAt(j).GetWeights.ElementAt(j + input.GetNodes.Count).WeightVal;
                    }
                }
                //sumActivations=output.calculateActivationDerivative(sumActivations);
                sumActivations = output.calculateSigmoidValue(sumActivations, trainingParameters.lambda);
                activationOutput.Add(sumActivations);

                //add bias

                //change node value
                output.GetNodes.ElementAt(i).NodeValue = sumActivations;
            }
            return activationOutput;
        }

        //private List<float> updateWeights(Layer input, Layer output,int iteration, List<float> errorOut = null, Layer outFinal=null/*, Layer context*/)
        private List<float> updateWeights(ref Layer input, ref Layer output, int iteration, List<float> errorOut = null, Layer outFinal = null/*, Layer context*/)
        {
            List<float> errOut = new List<float>();
            //calculate errors based on layer types
            if (output.LayerType == LayerType.Hidden)
            {
                if (errorOut != null)
                {
                    //
                    //for (int i = 0; i < input.GetNodes.Count; i++)
                    //{
                    //    float error = 0.0f;
                    //    for (int j = 0; j < output.GetNodes.Count; j++)
                    //    {
                    //        error += output.GetNodes.ElementAt(j).GetWeights.ElementAt(i).WeightVal;                
                    //    }
                    //    error *= output.calculateActivationDerivative(errorOut.ElementAt(i));
                    //    errOut.Add(error);
                    //}
                    //output - 10 nodes, input - 6 nodes
                    for (int i = 0; i < output.GetNodes.Count; i++)
                    {
                        float error = 0.0f;
                        for (int j = 0; j < outFinal.GetNodes.Count; j++)
                        {
                            error += errorOut.ElementAt(j) * outFinal.GetNodes.ElementAt(j).GetWeights.ElementAt(i).WeightVal;
                        }
                        error *= output.calculateActivationDerivative(output.GetNodes.ElementAt(i).NodeValue);
                        errOut.Add(error);
                    }
                }
                else
                {
                    //break and return error message
                }
            }
            else
            {
                for (int i = 0; i < output.GetNodes.Count; i++)
                {
                    float valDer = 0.0f;
                    valDer = output.calculateActivationDerivative(output.GetNodes.ElementAt(i).NodeValue);
                    errOut.Add((spoofTargets.ElementAt(iteration).ElementAt(i) - output.GetNodes.ElementAt(i).NodeValue) * valDer);
                }
            }

            //update weights
            for (int i = 0; i < output.GetNodes.Count; i++)
            {
                //commented out - addition to update context layer's weights as well
                for (int j = 0; j < input.GetNodes.Count/*+context.GetNodes.Count*/; j++)
                {
                    //if(i<input.GetNodes.Count)
                    //{
                    output.GetNodes.ElementAt(i).GetWeights.ElementAt(j).WeightVal += (trainingParameters.learningRate * errOut.ElementAt(i) * input.GetNodes.ElementAt(j).NodeValue);
                    float tmp = trainingParameters.learningRate * errOut.ElementAt(i) * input.GetNodes.ElementAt(j).NodeValue;
                    output.GetNodes.ElementAt(i).WeightsVals[j] += (trainingParameters.learningRate * errOut.ElementAt(i) * input.GetNodes.ElementAt(j).NodeValue);
                    //}
                    //else
                    //{
                    //output.GetNodes.ElementAt(i).GetWeights.ElementAt(j).WeightVal+=(trainingParameters.learningRate*errOut.ElementAt(i)*context.GetNodes.ElementAt(j).NodeValue);
                    //}
                }
                //update bias node - can be done within the prev loop
            }

            return errOut;
        }

        private void CreateLayers(int inputNodes = 6, int outputNodes = 6)
        {
            List<float> valsTmp = new List<float>();
            valsTmp.Add(0.5f);
            valsTmp.Add(0.3f);
            valsTmp.Add(0.8f);
            valsTmp.Add(0.5f);
            valsTmp.Add(0.3f);
            valsTmp.Add(0.8f);
            valsTmp.Add(0.5f);
            valsTmp.Add(0.3f);
            valsTmp.Add(0.8f);
            valsTmp.Add(0.8f);
            valsTmp.Add(0.8f);
            List<Layer> layersInput = new List<Layer>();
            List<Layer> layersOutput = new List<Layer>();

            //input and ouput layers both have 6 neurons because of spoof training data ->will need adjusting 
            //for different data or make it detect values from training data
            Layer input = new Layer(inputNodes, "Input");
            Layer context = new Layer(hiddenNodesNo, "Context");
            layersInput.Add(input);
            layersInput.Add(context);
            Layer hidden = new Layer(hiddenNodesNo, "Hidden", spoofInputData.ElementAt(0), layersInput);
            layersOutput.Add(hidden);
            hidden.getActivationForNodes();
            Layer output = new Layer(outputNodes, "Output", null, layersOutput);
            //context.addToInputLayer(hidden)
            ///hidden.getActivationForNodes();
            //copyActivationToContext(hidden, context);
            for (int i = 0; i < hiddenNodesNo; i++)
            {
                for (int j = hidden.GetNodes.IndexOf(hidden.GetNodes.ElementAt(input.GetNodes.Count)); j < hidden.GetNodes.Count + hidden.GetNodes.IndexOf(hidden.GetNodes.ElementAt(input.GetNodes.Count)); j++)
                {
                    hidden.GetNodes.ElementAt(i).GetWeights.ElementAt(j).WeightVal = 0.5f;
                    hidden.GetNodes.ElementAt(i).WeightsVals[j] = 0.5f;
                }
            }

            layers.Add(input);
            layers.Add(context);
            layers.Add(hidden);
            layers.Add(output);

            //if more hidden layers needed
            for (int i = 0; i < layerNo - 4; i++)
            {
                //Layer newLayer = new Layer(nodesNo, layerName, inputs);
                Layer hiddenTmp = new Layer(1, "Hidden");
                layers.Add(hiddenTmp);
            }

            for (int i = 0; i < 6; i++)
            {
                //layers.First(x => x.LayerName == "Input").GetNodes.ElementAt(i).NodeValue = spoofInputData.ElementAt(0).ElementAt(i);
            }


            createConnections();
        }

        private List<LayerConnection> createConnections()
        {
            List<LayerConnection> connectionsTmp = new List<LayerConnection>();
            return connectionsTmp;
        }

        private void CreateData()
        {
            for (int i = 0; i < 3; i++)
            {
                List<float> vals = new List<float>();
                for (int j = 0; j < 6; j++)
                {
                    //if ((i == 0 && j == 0) || (i == 1 && j == 3) || (i == 2 && j == 5) || (i == 3 && j == 2) || (i == 4 && j == 0))
                    if ((i == 0 && j == 3) || (i == 1 && j == 5) || (i == 2 && j == 2))
                    {
                        vals.Add(1.0f);
                    }
                    else
                    {
                        vals.Add(0.0f);
                    }
                }
                spoofInputData.Add(vals);
                spoofTargets.Add(vals);
                //vals.Clear();
            }


            terminationData.Add(1.0f);
            for (int i = 1; i < 6; i++)
            {
                terminationData.Add(0.0f);
            }
        }

        public Dictionary<LayerType, List<Node>> getDataForDrawing()
        {
            Dictionary<LayerType, List<Node>> graphData = new Dictionary<LayerType, List<Node>>();

            foreach (Layer layer in layers)
            {
                graphData.Add(layer.LayerType, layer.GetNodes);
            }

            return graphData;
        }

        public string NetName
        {
            //remove after tested!
            set { netName = value; }
            get { return netName; }
        }

        public TrainingParams TrainingParameters
        {
            set { trainingParameters = value; }
            get { return trainingParameters; }
        }


        public string ToString(bool def = true)
        {
            if (def)
            {
                return "Neural Network - " + netName;
            }
            else
            {
                string netDescription = "";

                netDescription += "Neural Network - " + netName + ":" + Environment.NewLine + Environment.NewLine;
                netDescription += "Layers: " + Environment.NewLine + Environment.NewLine;

                for (int i = 0; i < layers.Count; i++)
                {
                    netDescription += layers.ElementAt(i).LayerName + ":" + Environment.NewLine;
                    netDescription += "Number of nodes: " + layers.ElementAt(i).GetNodes.Count + Environment.NewLine;
                    netDescription += "Weights/Connections to this layer (per node): " + layers.ElementAt(i).GetNodes.ElementAt(0).GetWeights.Count + Environment.NewLine + Environment.NewLine;
                }
                return netDescription;
            }
        }
    }
}
