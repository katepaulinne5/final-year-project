﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RNN.Model;

namespace RNN.Observer
{
    public class Observer
    {
        private delegate void ElmanNetEventHandler(ElmanNet _net);
        private event ElmanNetEventHandler ElmanNetChangedEvent;
        private List<ElmanNet> listNet;

        public Observer(List<ElmanNet> _listNet)
        {
            this.ElmanNetChangedEvent += new ElmanNetEventHandler(Observer_ElmanNetChangedEvent);
            this.listNet = _listNet;
        }

        public void OnChange(ElmanNet _net)
        {
            if (ElmanNetChangedEvent != null)
            {
                ElmanNetChangedEvent(_net);
            }
        }

        public void Update(ElmanNet _net)
        {
            OnChange(_net);
        }

        private void Observer_ElmanNetChangedEvent(ElmanNet _net)
        {
            listNet[listNet.IndexOf(listNet.First(x => x.NetName == _net.NetName))] = _net;
        }
    }
}
