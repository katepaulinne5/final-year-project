﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNN
{
    class ErrorUpdatedEventArgs:EventArgs
    {
        public List<float> ErrorUpdate { get; set; }
    }
}
