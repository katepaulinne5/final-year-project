﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RNN.Model;
using OxyPlot;
using OxyPlot.Series;
using System.Threading;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using System.IO;

namespace RNN
{
    public partial class TrainTestNet : Form
    {
        private bool isChecked = false;
        //used to get outcome of the training & testing
        //true only if error is below threshold
        private bool testResult = false;
        private ElmanNet net;
        private IElmanNet network;
        private IMLDataSet data;
        private InputType testInputType;
        //private ObservableCollection<ElmanNet> observableNets;

        public event EventHandler<NetUpdatedEventArgs> netUpdated;
        public event EventHandler<EncogNetUpdatedEventArgs> encogNetUpdated;

        List<float> error = new List<float>();


        private BackgroundWorker trainingWorkerThread;
        public TrainTestNet()
        {
            InitializeComponent();
            rbtnAdjustParams.Location = new Point(gbAdjustParams.Location.X + 13, gbAdjustParams.Location.Y - 1);
            nudLambda.Enabled = false;
            nudLearningRate.Enabled = false;
            btnSave.Enabled = false;
            //rbtnAdjustParams.Checked = true;
            //rbtnAdjustParams.Checked = false;

            trainingWorkerThread = new BackgroundWorker();
            trainingWorkerThread.DoWork += new DoWorkEventHandler(backgroundTrain_Work);
            trainingWorkerThread.ProgressChanged += new ProgressChangedEventHandler(backgroundTrain_Progress);
            trainingWorkerThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(beckgroundTrain_Completed);
            trainingWorkerThread.WorkerReportsProgress = true;
            trainingWorkerThread.WorkerSupportsCancellation = true;
        }

        public TrainTestNet(ElmanNet _net)
        {
            net = _net;
            //observableNets = nets;
            InitializeComponent();
            rbtnAdjustParams.Location = new Point(gbAdjustParams.Location.X + 13, gbAdjustParams.Location.Y - 1);
            nudLambda.Enabled = false;
            nudLearningRate.Enabled = false;
            btnSave.Enabled = false;
            //rbtnAdjustParams.Checked = true;
            //rbtnAdjustParams.Checked = false;

            //display plot

            PlotModel model = new PlotModel { Title = "Training Error Plot" };
            model.Series.Add(new FunctionSeries());
            this.plotTrainError.Model = model;
        }

        public TrainTestNet(IElmanNet _net, IMLDataSet dataSet)
        {
            network = _net;
            //observableNets = nets;
            InitializeComponent();
            rbtnAdjustParams.Location = new Point(gbAdjustParams.Location.X + 13, gbAdjustParams.Location.Y - 1);
            nudLambda.Enabled = false;
            nudLearningRate.Enabled = false;
            btnSave.Enabled = false;
            data = dataSet;
            //rbtnAdjustParams.Checked = true;
            //rbtnAdjustParams.Checked = false;

            //display plot
            nudLambda.Value = network.TrainingParameters.lambda;
            nudLearningRate.Value = Convert.ToDecimal(network.TrainingParameters.learningRate);

            PlotModel model = new PlotModel { Title = "Training Error Plot" };
            model.Series.Add(new FunctionSeries());
            this.plotTrainError.Model = model;
        }

        private void rbtnAdjustParams_CheckedChanged(object sender, EventArgs e)
        {
            isChecked = rbtnAdjustParams.Checked;
        }

        private void rbtnAdjustParams_Click(object sender, EventArgs e)
        {
            if (rbtnAdjustParams.Checked && !isChecked)
            {
                rbtnAdjustParams.Checked = false;
                nudLambda.Enabled = false;
                nudLearningRate.Enabled = false;
                btnSave.Enabled = false;
            }
            else
            {
                rbtnAdjustParams.Checked = true;
                isChecked = false;
                nudLambda.Enabled = true;
                nudLearningRate.Enabled = true;
                btnSave.Enabled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //observer update (?) or pass network back and update
            //using events raise event when save pressed and save net's details
            //net.TrainingParameters.lambda = Convert.ToInt32(nudLambda.Value);
            TrainingParams tr;
            //tr = net.TrainingParameters;
            tr = network.TrainingParameters;
            tr.lambda = Convert.ToInt32(nudLambda.Value);
            tr.learningRate = (float)nudLearningRate.Value;
            //net.TrainingParameters = tr;
            //net.NetName = "changed";
            network.TrainingParameters = tr;
            //network.NetName = "changed";
            //NetUpdatedEventArgs netUpdatedEA = new NetUpdatedEventArgs();
            EncogNetUpdatedEventArgs netUpdatedEA = new EncogNetUpdatedEventArgs();
            //netUpdatedEA.Net = net;
            netUpdatedEA.Net = network;
            OnNetUpdated(netUpdatedEA);
        }

        public virtual void OnNetUpdated(NetUpdatedEventArgs e)
        {
            if (netUpdated != null)
            {
                netUpdated(this, e);
            }
        }

        public virtual void OnNetUpdated(EncogNetUpdatedEventArgs e)
        {
            if (encogNetUpdated != null)
            {
                encogNetUpdated(this, e);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSaveResults_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnTrainNet_Click(object sender, EventArgs e)
        {
            //btnTrainNet.Enabled = false;
            //btnTrainCancel.Enabled = true;

            //net.Train(ref error);

            PlotModel model = new PlotModel { Title = "Training Error Plot" };
            LineSeries lsData = new LineSeries();

            network.Train(ref error);
            //Thread trainNet = new Thread(delegate() { network.Train(ref error); });
            //trainNet.Start();


            for (int i = 0; i < error.Count; i++)
            {
                lsData.Points.Add(new DataPoint(i + 1, Convert.ToDouble(error.ElementAt(i))));
            }
            model.Series.Add(lsData);
            this.plotTrainError.Model = model;

            lblErrorFinal.Text += error.Last().ToString();
            lblErrorMin.Text += error.Min().ToString();
        }

        //private void _netUpdated(object sender, ErrorUpdatedEventArgs e)
        //{

        //    if (e != null && e.ErrorUpdate != null)
        //    {
        //        LockWindowUpdate(plotTrainError.Handle);

        //        error = e.ErrorUpdate;

        //        LockWindowUpdate(IntPtr.Zero);
        //    }
        //}

        private void btnTestSequence_Click(object sender, EventArgs e)
        {
            List<float> inputs = new List<float>();
            List<double> prediction = new List<double>();
            List<float> error = new List<float>();
            //List<float> prediction = new List<float>();
            //inputs.Add((float)nudInput1.Value);
            //inputs.Add((float)nudInput2.Value);
            ////inputs.Add((float)nudInput3.Value);
            ////inputs.Add((float)nudInput4.Value);
            ////inputs.Add((float)nudInput5.Value);
            ////inputs.Add((float)nudInput6.Value);

            ////prediction=net.Test(inputs);

            ////lblOutputSeq.Text = string.Join(", ", prediction);
            //lblOutputSeq.Text = "";
            //for(int i=0; i<prediction.Count;i++)
            //{
            //    lblOutputSeq.Text += prediction.ElementAt(i).ToString("0.00");
            //    lblOutputSeq.Text += ",";
            //}
            if (data != null)
            {
                //error = network.Test(data, ref prediction, ref inputs);
            }

            List<float> inputs2 = new List<float>();
            List<double> prediction2 = new List<double>();
            List<float> error2 = new List<float>();

            //test with count up to 5 all permutations data
            List<List<float>> inputTmp = new List<List<float>>();
            List<List<float>> outputTmp = new List<List<float>>();

            if (txtTestDataPath.Text != "" && (rbtnSequential.Checked || rbtnStatic.Checked))
            {
                //readCSV("D:\\Debug\\Count5x10AllPerm.csv", ref inputTmp, ref outputTmp);
                readCSV(txtTestDataPath.Text, ref inputTmp, ref outputTmp);
                //dataset needs to be split into columns and saved as encog dataset 
                IMLDataSet testDataSet;
                if (testInputType == InputType.Sequential)
                {
                    testDataSet = SplitDataIntoRowsList(inputTmp, outputTmp);
                }
                else
                {
                    testDataSet = SplitDataIntoRowsAndColumns(inputTmp, outputTmp);
                }
                error2 = network.Test(testDataSet, ref prediction2, ref inputs2);
                lblOutputSeq.Text = error2.ElementAt(0).ToString();
            }
            else
            {
                MessageBox.Show("Choose a file to use for network's testing and select input type.", "", MessageBoxButtons.OK);
            }
        }

        void backgroundTrain_Work(object sender, DoWorkEventArgs e)
        {

        }

        void backgroundTrain_Progress(object sender, ProgressChangedEventArgs e)
        {

        }

        void beckgroundTrain_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            btnTrainCancel.Enabled = false;
        }

        private void btnTrainCancel_Click(object sender, EventArgs e)
        {
            if (trainingWorkerThread.IsBusy)
            {
                trainingWorkerThread.CancelAsync();
            }
        }

        private static void readCSV(string path, ref List<List<float>> inputVals, ref List<List<float>> outputTargets)
        {
            //List<float> inputVals = new List<float>();
            //List<float> outputTargets = new List<float>();

            FileStream fs = File.OpenRead(path);
            using (StreamReader reader = new StreamReader(fs))
            {
                for (int l = 0; l < File.ReadAllLines(path).Count(); l++)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');

                    List<float> inputValsTmp = new List<float>();
                    List<float> outputValsTmp = new List<float>();

                    int counter = 1;
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (counter == 1)
                        {
                            if (!string.IsNullOrEmpty(values[i]))
                            {
                                float val = float.Parse(values.ElementAt(i));
                                if (val != null)
                                {
                                    inputValsTmp.Add(val);
                                }
                                else
                                {
                                    counter++;
                                }
                            }
                            else
                            {
                                counter++;
                            }
                        }
                        else
                        {
                            float val = float.Parse(values.ElementAt(i));
                            if (val != null)
                            {
                                outputValsTmp.Add(val);
                            }
                        }
                    }
                    inputVals.Add(inputValsTmp);
                    outputTargets.Add(outputValsTmp);
                }
            }
            //return inputVals;
        }

        private static BasicMLDataSet SplitDataIntoRowsList(List<List<float>> data, List<List<float>> idealData)
        {
            List<List<int>> dataInput = new List<List<int>>();
            //idealData = new double[data.Length * data[0].Length][];
            double[][] dataSet = new double[data.Count * data.ElementAt(0).Count][];
            double[][] dataSetIdeal = new double[idealData.Count * data.ElementAt(0).Count][];
            BasicMLData dataset;
            for (int i = 0; i < data.Count; i++)
            {
                int counter = 0;
                for (int j = 0; j < data.ElementAt(i).Count; j++)
                {
                    dataSet[data.ElementAt(i).Count * i + j] = new double[1];
                    dataSet[data.ElementAt(i).Count * i + j][0] = data.ElementAt(i).ElementAt(j);
                    if (dataSet[data.ElementAt(i).Count * i + j][0] == 1)
                    {
                        counter++;
                    }
                    dataSetIdeal[data.ElementAt(i).Count * i + j] = new double[1];
                    dataSetIdeal[data.ElementAt(i).Count * i + j][0] = counter;
                }
            }
            return new BasicMLDataSet(dataSet, dataSetIdeal);
            //return dataInput;
        }

        private static BasicMLDataSet SplitDataIntoRowsAndColumns(List<List<float>> data, List<List<float>> idealData)
        {
            List<List<int>> dataInput = new List<List<int>>();
            //idealData = new double[data.Length * data[0].Length][];
            double[][] dataSet = new double[data.Count][];
            double[][] dataSetIdeal = new double[idealData.Count][];
            BasicMLData dataset;
            for (int i = 0; i < data.Count; i++)
            {
                int counter = 0;
                dataSet[i] = new double[data.ElementAt(i).Count];
                dataSetIdeal[i] = new double[data.ElementAt(i).Count];
                for (int j = 0; j < data.ElementAt(i).Count; j++)
                {

                    dataSet[i][j] = data.ElementAt(i).ElementAt(j);
                    if (dataSet[i][j] == 1)
                    {
                        counter++;
                    }

                    dataSetIdeal[i][j] = counter;
                }
            }
            return new BasicMLDataSet(dataSet, dataSetIdeal);
            //return dataInput;
        }

        private static double[][] SplitDataIntoRows(double[][] data, out double[][] idealData)
        {
            double[][] dataInput = new double[data.Length * data[0].Length][];
            idealData = new double[data.Length * data[0].Length][];

            for (int i = 0; i < data.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < data[i].Length; j++)
                {
                    dataInput[data[i].Length * i + j] = new double[1];
                    dataInput[data[i].Length * i + j][0] = data[i][j];
                    if (data[i][j] == 1)
                    {
                        counter++;
                    }
                    idealData[data[i].Length * i + j] = new double[1];
                    idealData[data[i].Length * i + j][0] = counter;
                }
            }
            return dataInput;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadCSVDialog = new OpenFileDialog();
            loadCSVDialog.Filter = "CSV Files|*.csv";
            loadCSVDialog.Title = "Select CSV testing file";
            loadCSVDialog.RestoreDirectory = true;

            if (loadCSVDialog.ShowDialog() == DialogResult.OK)
            {
                string nameFile = loadCSVDialog.SafeFileName;
                loadCSVDialog.OpenFile();
                //string directoryPath = Path.GetDirectoryName(nameFile + ".csv");
                txtTestDataPath.Text = loadCSVDialog.FileName;
            }
        }

        private void rbtnSequential_CheckedChanged(object sender, EventArgs e)
        {
            testInputType = InputType.Sequential;
        }

        private void rbtnStatic_CheckedChanged(object sender, EventArgs e)
        {
            testInputType = InputType.Static;
        }

    }
}
