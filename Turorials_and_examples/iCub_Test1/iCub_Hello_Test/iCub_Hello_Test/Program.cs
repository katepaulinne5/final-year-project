﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace iCub_Hello_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Process iCubSim = new Process();
            iCubSim.StartInfo.FileName="C:\\Program Files\\robotology\\icub-1.4.0\\bin\\iCub_SIM.exe";
            iCubSim.Start();
        }
    }
}
