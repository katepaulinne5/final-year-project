﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Linq.Expressions;

namespace RNN
{
    public static class SimulatorPathManager
    {
        private enum procId
        {
            YARP,
            ICub,
            YarpRobotInterface,
            IKinCartesianSolver
        }

        private static int maxIndex = Enum.GetNames(typeof(procId)).Length;
        private const string AppName = "RNN";
        private static string proc_YARP = "yarpserver";
        private static string proc_ICub = "iCub_SIM";
        private static string proc_YarpRobotInterface = "yarprobotinterface";
        private static string proc_IKinCartesianSolver = "iKinCartesianSolver";
        private static string path_YARP;
        private static string path_ICub;
        private static string path_YarpRobotInterface;
        private static string path_IKinCartesianSolver;
        private static string lineArgs_YARP;
        private static string lineArgs_YarpRobotInterface;
        private static string lineArgs_IKinCartesianSolver;
        private static List<string> processes = new List<string>(4);
        private static List<string> paths = new List<string>(4);
        private static List<string> lineArgs = new List<string>(3);
        private static List<string> subKeyNames = new List<string>(11);
        private static bool initialised = false;
        //private static RegistryKey key= Registry.CurrentUser.;

        public static void SaveToRegistry()
        {
            if (!initialised)
            {
                for (int i = 0; i < subKeyNames.Count; i++)
                {
                    subKeyNames[i] = GetKeyNameByIndex(i);
                }

                initialised = true;
            }
            //check if all values not null 
            RegistryKey key;
            bool exists = false;
            foreach (string subKeyName in Registry.CurrentUser.OpenSubKey("SOFTWARE").GetSubKeyNames())
            {
                if (subKeyName.Contains(AppName))
                {
                    exists = true;
                }
            }

            if (!exists)
            {
                key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\" + AppName);
            }
            else
            {
                key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\" + AppName, true);
            }
            key.SetValue(VariableInfo.GetVariableName(() => path_YARP), path_YARP);
            key.SetValue(VariableInfo.GetVariableName(() => path_ICub), path_ICub);
            key.SetValue(VariableInfo.GetVariableName(() => path_YarpRobotInterface), path_YarpRobotInterface);
            key.SetValue(VariableInfo.GetVariableName(() => path_IKinCartesianSolver), path_IKinCartesianSolver);
            key.SetValue(VariableInfo.GetVariableName(() => lineArgs_YARP), lineArgs_YARP);
            key.SetValue(VariableInfo.GetVariableName(() => lineArgs_YarpRobotInterface), lineArgs_YarpRobotInterface);
            key.SetValue(VariableInfo.GetVariableName(() => lineArgs_IKinCartesianSolver), lineArgs_IKinCartesianSolver);
            key.Close();
        }

        public static void RetrieveFromRegistry()
        {
            if (!initialised)
            {
                for (int i = 0; i < subKeyNames.Capacity; i++)
                {
                    subKeyNames.Add(GetKeyNameByIndex(i));
                }

                initialised = true;
            }

            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\" + AppName);
            if (key != null)
            {
                
                path_YARP = key.GetValue(VariableInfo.GetVariableName(() => path_YARP)).ToString();
                path_ICub = key.GetValue(VariableInfo.GetVariableName(() => path_ICub)).ToString();
                if (key.GetValueNames().Contains(VariableInfo.GetVariableName(() => path_YarpRobotInterface))) 
                {
                    path_YarpRobotInterface = key.GetValue(VariableInfo.GetVariableName(() => path_YarpRobotInterface)).ToString();
                }
                if (key.GetValueNames().Contains(VariableInfo.GetVariableName(() => path_IKinCartesianSolver)))
                {
                    path_IKinCartesianSolver = key.GetValue(VariableInfo.GetVariableName(() => path_IKinCartesianSolver)).ToString();
                }
                lineArgs_YARP = key.GetValue(VariableInfo.GetVariableName(() => lineArgs_YARP)).ToString();
                if (key.GetValueNames().Contains(VariableInfo.GetVariableName(() => lineArgs_YarpRobotInterface))) 
                {
                    lineArgs_YarpRobotInterface = key.GetValue(VariableInfo.GetVariableName(() => lineArgs_YarpRobotInterface)).ToString();
                }
                if (key.GetValueNames().Contains(VariableInfo.GetVariableName(() => lineArgs_IKinCartesianSolver)))
                {
                    lineArgs_IKinCartesianSolver = key.GetValue(VariableInfo.GetVariableName(() => lineArgs_IKinCartesianSolver)).ToString();
                }
                

                for (int i = 0; i < maxIndex; i++)
                {
                    if (key.GetSubKeyNames().Contains(subKeyNames.ElementAt(i)))
                    {

                    }
                    else
                    {

                    }
                }
                key.Close();
            }
        }

        private static string GetKeyNameByIndex(int idx)
        {
            string ret = "";
            switch (idx)
            {
                case 0:
                    ret = VariableInfo.GetVariableName(() => path_YARP).ToString();
                    break;
                case 1:
                    ret = VariableInfo.GetVariableName(() => path_ICub).ToString();
                    break;
                case 2:
                    ret = VariableInfo.GetVariableName(() => path_YarpRobotInterface).ToString();
                    break;
                case 3:
                    ret = VariableInfo.GetVariableName(() => path_IKinCartesianSolver).ToString();
                    break;
                case 4:
                    ret = VariableInfo.GetVariableName(() => proc_YARP).ToString();
                    break;
                case 5:
                    ret = VariableInfo.GetVariableName(() => proc_ICub).ToString();
                    break;
                case 6:
                    ret = VariableInfo.GetVariableName(() => proc_YarpRobotInterface).ToString();
                    break;
                case 7:
                    ret = VariableInfo.GetVariableName(() => proc_IKinCartesianSolver).ToString();
                    break;
                case 8:
                    ret = VariableInfo.GetVariableName(() => lineArgs_YARP).ToString();
                    break;
                case 9:
                    ret = VariableInfo.GetVariableName(() => lineArgs_YarpRobotInterface).ToString();
                    break;
                case 10:
                    ret = VariableInfo.GetVariableName(() => lineArgs_IKinCartesianSolver).ToString();
                    break;
            }
            
            return ret;
        }


        public static string GetProc_YARP
        {
            get { return proc_YARP; }
        }

        public static string GetProc_ICub
        {
            get { return proc_ICub; }
        }

        public static string GetSetPath_YARP
        {
            set { path_YARP = value; }
            get { return path_YARP; }
        }

        public static string GetSetPath_YarpRobotInterface
        {
            set { path_YarpRobotInterface = value; }
            get { return path_YarpRobotInterface; }
        }

        public static string GetSetPath_IKinCartesianSolver
        {
            set { path_IKinCartesianSolver = value; }
            get { return path_IKinCartesianSolver; }
        }

        public static string GetSetPath_ICub
        {
            set { path_ICub = value; }
            get { return path_ICub; }
        }

        public static string GetSetLineArgs_YARP
        {
            set { lineArgs_YARP = value; }
            get { return lineArgs_YARP; }
        }

        public static string GetSetLineArgs_YarpRobotInterface
        {
            set { lineArgs_YarpRobotInterface = value; }
            get { return lineArgs_YarpRobotInterface; }
        }
        public static string GetSetLineArgs_IKinCartesianSolver
        {
            set { lineArgs_IKinCartesianSolver = value; }
            get { return lineArgs_IKinCartesianSolver; }
        }

    }

    public static class VariableInfo
    {
        public static string GetVariableName<T>(Expression<Func<T>> expression)
        {
            return (expression.Body as MemberExpression).Member.Name;
        }
    }
}
