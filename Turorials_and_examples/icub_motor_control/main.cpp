#include <stdio.h>
#include <yarp/os/Network.h>
#include <yarp/dev/ControlBoardInterfaces.h>
#include <yarp/dev/PolyDriver.h>
#include <yarp/os/Time.h>
#include <yarp/sig/Vector.h>
#include <string>

using namespace std;
using namespace yarp::dev;
using namespace yarp::sig;
using namespace yarp::os;

int main(/*int arc, char *argv[]*/)
{
	Network yarp;

	Property options;
	options.put("device", "remote_controlboard");
	options.put("local", "/icub_motor_control/client");
	options.put("remote", "/icubSim/right_arm");

	PolyDriver robotDevice(options);
	if (!robotDevice.isValid())
	{
		printf("Device not available.  Here are the known devices:\n");
		printf("%s", Drivers::factory().toString().c_str());
		//error display
		//return 1;
	}

	IPositionControl *pos;
	IVelocityControl *vel;
	IEncoders *enc;

	robotDevice.view(pos);
	robotDevice.view(vel); 
	robotDevice.view(enc);

	if (robotDevice.view(pos) && robotDevice.view(enc))//pos == 0)
	{
		//error
		//return 1;
	}

	int jnts = 0;
	pos->getAxes(&jnts);

	Vector tmp;
	Vector encoders;
	Vector command_position;
	Vector command_velocity;

	tmp.resize(jnts);
	encoders.resize(jnts);

	for (int i = 0; i < jnts; i++)
	{
		//50 degrees per sec^2
		tmp[i] = 50.0;
	}

	pos->setRefAccelerations(tmp.data());


	

	for (int i = 0; i < jnts; i++)
	{
		//speed 40 degrees per second
		tmp[i] = 40.0;
	}
	pos->setRefSpeeds(tmp.data());

	while(!enc->getEncoders(encoders.data()))
	{
		Time::delay(0.1);
		printf(".");
	}
	printf("\n");

	command_position = encoders;
	command_position[0] = -50;
	command_position[1] = 20;
	command_position[2] = -10;
	command_position[3] = 50;

	//bool ok = 
	pos->positionMove(command_position.data());

	bool done = false;

	while (!done)
	{
		pos->checkMotionDone(&done);
		Time::delay(0.1);
	}

	//bool ok = vel->velocityMove(command_velocity.data());

	robotDevice.close();

	return 0;
}