﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RNN.Model;
using System.IO;

namespace RNN
{
    public partial class CreateNewNet : Form
    {
        //File trainingFile;
        List<List<float>> inputs = new List<List<float>>();
        List<List<float>> outputs = new List<List<float>>();
        private bool isChecked = false;

        public CreateNewNet()
        {
            InitializeComponent();
            //for testing
            txtNetName.Text = "a";
            txtHiddenNodesNo.Text = "10";
            txtTrainingFilePath.Text="C:\\Users\\Kate\\Desktop\\Kate\\University\\Final Year\\Final Year Project\\final-year-project\\XOR_training_data.csv";
            txtOuputNodes.Text = "0";
            txtOuputNodes.Enabled = false;
        }

        private void btnCreateNet_Click(object sender, EventArgs e)
        {
            NetName=txtNetName.Text;
            FilePath = txtTrainingFilePath.Text;
            int res;
            int res2;
            if (int.TryParse(txtOuputNodes.Text, out res2))
            {
                if (int.TryParse(txtNodesNo.Text, out res2) || rbtnSeq.Checked)
                {
                    if (int.TryParse(txtHiddenNodesNo.Text, out res))
                    {
                        if ((rbtnElmanNet.Checked || rbtnEncogNet.Checked) && (rbtnSeq.Checked || rbtnStatic.Checked))
                        {
                            if (Convert.ToInt32(txtHiddenNodesNo.Text) <= 50)
                            {

                                HiddenNodesNo = Convert.ToInt32(txtHiddenNodesNo.Text);
                                if (txtTrainingFilePath.Text != null)
                                {
                                    //get file data and parse it into two lists of inputs and target outputs
                                    readCSV(txtTrainingFilePath.Text, ref inputs, ref outputs);
                                    Inputs = inputs;
                                    Outputs = outputs;
                                }
                                if (rbtnElmanNet.Checked)
                                {
                                    NetworkType = NetType.ElmanNet;
                                }
                                else
                                {
                                    NetworkType = NetType.ElmanEncog;
                                }

                                if (rbtnStatic.Checked)
                                {
                                    InputType = InputType.Static;
                                    InputNodesNo = Convert.ToInt32(txtNodesNo.Text);
                                }
                                else
                                {
                                    InputType = InputType.Sequential;
                                    InputNodesNo = 1;
                                }

                                if (cbIncGestures.Checked)
                                {
                                    OutputNodesNo = Convert.ToInt32(txtOuputNodes.Text);
                                }
                                else
                                {
                                    OutputNodesNo = InputNodesNo;
                                }

                                ContSeq = cbContinuousSeq.Checked;

                                this.DialogResult = DialogResult.OK;
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Hidden nodes number must be less or equal to 50", "", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Network type and/or type of input must be selected", "", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Hidden nodes number must be an integer", "", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("Input nodes number must be an integer", "", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Output nodes number must be an integer", "", MessageBoxButtons.OK);
            }
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult=DialogResult.Cancel;
            this.Close();
        }

        private void readCSV(string path, ref List<List<float>> inputVals, ref List<List<float>> outputTargets)
        {
            //List<float> inputVals = new List<float>();
            //List<float> outputTargets = new List<float>();

            FileStream fs = File.OpenRead(path);
            using(StreamReader reader=new StreamReader(fs))
            {
                for (int l = 0; l < File.ReadAllLines(path).Count(); l++)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');

                    List<float> inputValsTmp = new List<float>();
                    List<float> outputValsTmp = new List<float>();

                    int counter = 1;
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (counter == 1)
                        {
                            if (!string.IsNullOrEmpty(values[i]))
                            {
                                float val = float.Parse(values.ElementAt(i));
                                if (val != null)
                                {
                                    inputValsTmp.Add(val);
                                }
                                else
                                {
                                    counter++;
                                }
                            }
                            else
                            {
                                counter++;
                            }
                        }
                        else
                        {
                            float val = float.Parse(values.ElementAt(i));
                            if (val != null)
                            {
                                outputValsTmp.Add(val);
                            }
                        }
                    }
                    inputVals.Add(inputValsTmp);
                    outputTargets.Add(outputValsTmp);
                }
            }
            //return inputVals;
        }

        public string NetName
        {
            get;
            set;
        }

        public int HiddenNodesNo
        {
            get;
            set;
        }

        public NetType NetworkType
        {
            get;
            set;
        }

        public InputType InputType
        {
            get;
            set;
        }

        public List<List<float>> Inputs
        {
            get;
            set;
        }

        public List<List<float>> Outputs
        {
            get;
            set;
        }

        public string FilePath
        {
            get;
            set;
        }

        public int InputNodesNo
        {
            get;
            set;
        }

        public int OutputNodesNo
        {
            get;
            set;
        }

        public bool ContSeq
        {
            get;
            set;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadCSVDialog = new OpenFileDialog();
            loadCSVDialog.Filter = "CSV Files|*.csv";
            loadCSVDialog.Title = "Select CSV training file";
            loadCSVDialog.RestoreDirectory = true;

            if (loadCSVDialog.ShowDialog() == DialogResult.OK)
            {
                string nameFile = loadCSVDialog.SafeFileName;
                loadCSVDialog.OpenFile();
                //string directoryPath = Path.GetDirectoryName(nameFile + ".csv");
                txtTrainingFilePath.Text = loadCSVDialog.FileName;
            }
        }

        private void seq_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void seq_Clicked(object sender, EventArgs e)
        {


            if (!rbtnStatic.Checked && !isChecked)
            {
                rbtnStatic.Checked = false;
                txtNodesNo.Enabled = false;
                lblSeq.Enabled = false;
            }
            else
            {
                isChecked = false;
                rbtnStatic.Checked = true;
                txtNodesNo.Enabled = true;
                lblSeq.Enabled = true;
            }
        }

        private void static_CheckedChanged(object sender, EventArgs e)
        {
            isChecked = rbtnStatic.Checked;
        }

        private void static_Click(object sender, EventArgs e)
        {
            if (rbtnStatic.Checked && !isChecked)
            {
                //rbtnSeq.Checked = false;
                //txtNodesNo.Enabled = false;
                //lblSeq.Enabled = false;
            }
            else
            {
                isChecked = false;
                rbtnStatic.Checked = true;
                txtNodesNo.Enabled = true;
                rbtnStatic.Enabled = true;
                lblSeq.Enabled = true;
            }
        }

        private void incGestures_CheckedChanged(object sender, EventArgs e)
        {
            if(cbIncGestures.Checked)
            {
                txtOuputNodes.Enabled = true;
                lblOutputNodes.Enabled = true;
            }
            else
            {
                txtOuputNodes.Enabled = false;
                lblOutputNodes.Enabled = false;
                txtOuputNodes.Text = "0";
            }
        }
    }
}
