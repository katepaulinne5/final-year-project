﻿namespace RNN
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.clbNetworks = new System.Windows.Forms.CheckedListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.networkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCreateNewNet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSaveNet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLoadNet = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRunWithYARP = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManageAppPaths = new System.Windows.Forms.ToolStripMenuItem();
            this.btnManageNet = new System.Windows.Forms.Button();
            this.btnTrainTestNet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rtxtNetDetails = new System.Windows.Forms.RichTextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnManageYARPRobotInterface = new System.Windows.Forms.Button();
            this.btnManageCartesianSolver = new System.Windows.Forms.Button();
            this.lblYARPRobotInterface = new System.Windows.Forms.Label();
            this.lblIKinCartesianSolver = new System.Windows.Forms.Label();
            this.btnManageICub = new System.Windows.Forms.Button();
            this.btnManageYarp = new System.Windows.Forms.Button();
            this.btnCheckState = new System.Windows.Forms.Button();
            this.lblICubSim = new System.Windows.Forms.Label();
            this.lblYarpServer = new System.Windows.Forms.Label();
            this.ttInfo = new System.Windows.Forms.ToolTip(this.components);
            this.btnViewNet = new System.Windows.Forms.Button();
            this.pbNet = new System.Windows.Forms.PictureBox();
            this.gbNetTools = new System.Windows.Forms.GroupBox();
            this.panelTools = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbColourToggle = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbNet)).BeginInit();
            this.gbNetTools.SuspendLayout();
            this.panelTools.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // clbNetworks
            // 
            this.clbNetworks.BackColor = System.Drawing.SystemColors.Control;
            this.clbNetworks.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbNetworks.ForeColor = System.Drawing.Color.Black;
            this.clbNetworks.FormattingEnabled = true;
            this.clbNetworks.Location = new System.Drawing.Point(9, 49);
            this.clbNetworks.Name = "clbNetworks";
            this.clbNetworks.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.clbNetworks.Size = new System.Drawing.Size(298, 104);
            this.clbNetworks.TabIndex = 0;
            this.clbNetworks.Click += new System.EventHandler(this.clbNetworks_Click);
            this.clbNetworks.MouseClick += new System.Windows.Forms.MouseEventHandler(this.clbNetworks_MouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.networkToolStripMenuItem,
            this.runToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1095, 26);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // networkToolStripMenuItem
            // 
            this.networkToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCreateNewNet,
            this.tsmSaveNet,
            this.tsmLoadNet});
            this.networkToolStripMenuItem.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.networkToolStripMenuItem.Name = "networkToolStripMenuItem";
            this.networkToolStripMenuItem.Size = new System.Drawing.Size(79, 22);
            this.networkToolStripMenuItem.Text = "Network";
            // 
            // tsmCreateNewNet
            // 
            this.tsmCreateNewNet.Name = "tsmCreateNewNet";
            this.tsmCreateNewNet.Size = new System.Drawing.Size(194, 26);
            this.tsmCreateNewNet.Text = "Create New Net";
            this.tsmCreateNewNet.Click += new System.EventHandler(this.tsmCreateNewNet_Click);
            // 
            // tsmSaveNet
            // 
            this.tsmSaveNet.Name = "tsmSaveNet";
            this.tsmSaveNet.Size = new System.Drawing.Size(194, 26);
            this.tsmSaveNet.Text = "Save Net";
            this.tsmSaveNet.Click += new System.EventHandler(this.tsmSaveNet_Click);
            // 
            // tsmLoadNet
            // 
            this.tsmLoadNet.Name = "tsmLoadNet";
            this.tsmLoadNet.Size = new System.Drawing.Size(194, 26);
            this.tsmLoadNet.Text = "Load Net";
            this.tsmLoadNet.Click += new System.EventHandler(this.tsmLoadNet_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRunWithYARP,
            this.tsmManageAppPaths});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.runToolStripMenuItem.Text = "Simulator ";
            // 
            // tsmRunWithYARP
            // 
            this.tsmRunWithYARP.Name = "tsmRunWithYARP";
            this.tsmRunWithYARP.Size = new System.Drawing.Size(212, 26);
            this.tsmRunWithYARP.Text = "Run with YARP";
            this.tsmRunWithYARP.Click += new System.EventHandler(this.tsmRunWithYARP_Click);
            // 
            // tsmManageAppPaths
            // 
            this.tsmManageAppPaths.Name = "tsmManageAppPaths";
            this.tsmManageAppPaths.Size = new System.Drawing.Size(212, 26);
            this.tsmManageAppPaths.Text = "Manage App Paths";
            this.tsmManageAppPaths.Click += new System.EventHandler(this.tsmManageAppPaths_Click);
            // 
            // btnManageNet
            // 
            this.btnManageNet.BackColor = System.Drawing.Color.Silver;
            this.btnManageNet.FlatAppearance.BorderSize = 0;
            this.btnManageNet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageNet.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageNet.Location = new System.Drawing.Point(9, 173);
            this.btnManageNet.Name = "btnManageNet";
            this.btnManageNet.Size = new System.Drawing.Size(144, 29);
            this.btnManageNet.TabIndex = 2;
            this.btnManageNet.Text = "Manage Network";
            this.btnManageNet.UseVisualStyleBackColor = false;
            this.btnManageNet.Click += new System.EventHandler(this.btnManageNet_Click);
            // 
            // btnTrainTestNet
            // 
            this.btnTrainTestNet.BackColor = System.Drawing.Color.Silver;
            this.btnTrainTestNet.FlatAppearance.BorderSize = 0;
            this.btnTrainTestNet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrainTestNet.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrainTestNet.Location = new System.Drawing.Point(159, 173);
            this.btnTrainTestNet.Name = "btnTrainTestNet";
            this.btnTrainTestNet.Size = new System.Drawing.Size(148, 29);
            this.btnTrainTestNet.TabIndex = 3;
            this.btnTrainTestNet.Text = "Train/Test Network";
            this.btnTrainTestNet.UseVisualStyleBackColor = false;
            this.btnTrainTestNet.Click += new System.EventHandler(this.btnTrainTestNet_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(15, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Selected network structure:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Neural networks list:";
            // 
            // rtxtNetDetails
            // 
            this.rtxtNetDetails.BackColor = System.Drawing.SystemColors.Control;
            this.rtxtNetDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtNetDetails.ForeColor = System.Drawing.Color.Black;
            this.rtxtNetDetails.Location = new System.Drawing.Point(13, 44);
            this.rtxtNetDetails.Name = "rtxtNetDetails";
            this.rtxtNetDetails.ReadOnly = true;
            this.rtxtNetDetails.Size = new System.Drawing.Size(401, 452);
            this.rtxtNetDetails.TabIndex = 7;
            this.rtxtNetDetails.Text = "";
            this.rtxtNetDetails.SelectionChanged += new System.EventHandler(this.rtxtNetDetails_SelectionChanged);
            this.rtxtNetDetails.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rtxtNetDetails_MouseClick);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(159, 208);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(148, 29);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Delete Selected Net";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnManageYARPRobotInterface);
            this.groupBox1.Controls.Add(this.btnManageCartesianSolver);
            this.groupBox1.Controls.Add(this.lblYARPRobotInterface);
            this.groupBox1.Controls.Add(this.lblIKinCartesianSolver);
            this.groupBox1.Controls.Add(this.btnManageICub);
            this.groupBox1.Controls.Add(this.btnManageYarp);
            this.groupBox1.Controls.Add(this.btnCheckState);
            this.groupBox1.Controls.Add(this.lblICubSim);
            this.groupBox1.Controls.Add(this.lblYarpServer);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(3, 271);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 188);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Processes State";
            // 
            // btnManageYARPRobotInterface
            // 
            this.btnManageYARPRobotInterface.BackColor = System.Drawing.Color.Silver;
            this.btnManageYARPRobotInterface.FlatAppearance.BorderSize = 0;
            this.btnManageYARPRobotInterface.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageYARPRobotInterface.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageYARPRobotInterface.ForeColor = System.Drawing.Color.Black;
            this.btnManageYARPRobotInterface.Location = new System.Drawing.Point(242, 83);
            this.btnManageYARPRobotInterface.Name = "btnManageYARPRobotInterface";
            this.btnManageYARPRobotInterface.Size = new System.Drawing.Size(68, 24);
            this.btnManageYARPRobotInterface.TabIndex = 8;
            this.btnManageYARPRobotInterface.Text = "Start";
            this.btnManageYARPRobotInterface.UseVisualStyleBackColor = false;
            this.btnManageYARPRobotInterface.Click += new System.EventHandler(this.btnManageYARPRobotInterface_Click);
            // 
            // btnManageCartesianSolver
            // 
            this.btnManageCartesianSolver.BackColor = System.Drawing.Color.Silver;
            this.btnManageCartesianSolver.Enabled = false;
            this.btnManageCartesianSolver.FlatAppearance.BorderSize = 0;
            this.btnManageCartesianSolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageCartesianSolver.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageCartesianSolver.ForeColor = System.Drawing.Color.Black;
            this.btnManageCartesianSolver.Location = new System.Drawing.Point(242, 113);
            this.btnManageCartesianSolver.Name = "btnManageCartesianSolver";
            this.btnManageCartesianSolver.Size = new System.Drawing.Size(68, 24);
            this.btnManageCartesianSolver.TabIndex = 7;
            this.btnManageCartesianSolver.Text = "Start";
            this.btnManageCartesianSolver.UseVisualStyleBackColor = false;
            this.btnManageCartesianSolver.Click += new System.EventHandler(this.btnManageCartesianSolver_Click);
            // 
            // lblYARPRobotInterface
            // 
            this.lblYARPRobotInterface.BackColor = System.Drawing.SystemColors.Control;
            this.lblYARPRobotInterface.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYARPRobotInterface.ForeColor = System.Drawing.Color.Black;
            this.lblYARPRobotInterface.Location = new System.Drawing.Point(6, 90);
            this.lblYARPRobotInterface.Name = "lblYARPRobotInterface";
            this.lblYARPRobotInterface.Size = new System.Drawing.Size(230, 17);
            this.lblYARPRobotInterface.TabIndex = 6;
            this.lblYARPRobotInterface.Text = "Robot Interface - Not Running";
            // 
            // lblIKinCartesianSolver
            // 
            this.lblIKinCartesianSolver.BackColor = System.Drawing.SystemColors.Control;
            this.lblIKinCartesianSolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIKinCartesianSolver.ForeColor = System.Drawing.Color.Black;
            this.lblIKinCartesianSolver.Location = new System.Drawing.Point(6, 120);
            this.lblIKinCartesianSolver.Name = "lblIKinCartesianSolver";
            this.lblIKinCartesianSolver.Size = new System.Drawing.Size(230, 17);
            this.lblIKinCartesianSolver.TabIndex = 5;
            this.lblIKinCartesianSolver.Text = "Cartesian Solver - Not Running";
            // 
            // btnManageICub
            // 
            this.btnManageICub.BackColor = System.Drawing.Color.Silver;
            this.btnManageICub.FlatAppearance.BorderSize = 0;
            this.btnManageICub.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageICub.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageICub.ForeColor = System.Drawing.Color.Black;
            this.btnManageICub.Location = new System.Drawing.Point(242, 53);
            this.btnManageICub.Name = "btnManageICub";
            this.btnManageICub.Size = new System.Drawing.Size(68, 24);
            this.btnManageICub.TabIndex = 4;
            this.btnManageICub.Text = "Start";
            this.btnManageICub.UseVisualStyleBackColor = false;
            this.btnManageICub.Click += new System.EventHandler(this.btnManageICub_Click);
            // 
            // btnManageYarp
            // 
            this.btnManageYarp.BackColor = System.Drawing.Color.Silver;
            this.btnManageYarp.FlatAppearance.BorderSize = 0;
            this.btnManageYarp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageYarp.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageYarp.ForeColor = System.Drawing.Color.Black;
            this.btnManageYarp.Location = new System.Drawing.Point(242, 21);
            this.btnManageYarp.Name = "btnManageYarp";
            this.btnManageYarp.Size = new System.Drawing.Size(68, 26);
            this.btnManageYarp.TabIndex = 3;
            this.btnManageYarp.Text = "Start";
            this.btnManageYarp.UseVisualStyleBackColor = false;
            this.btnManageYarp.Click += new System.EventHandler(this.btnManageYarp_Click);
            // 
            // btnCheckState
            // 
            this.btnCheckState.BackColor = System.Drawing.Color.Silver;
            this.btnCheckState.FlatAppearance.BorderSize = 0;
            this.btnCheckState.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckState.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckState.ForeColor = System.Drawing.Color.Black;
            this.btnCheckState.Location = new System.Drawing.Point(124, 143);
            this.btnCheckState.Name = "btnCheckState";
            this.btnCheckState.Size = new System.Drawing.Size(186, 31);
            this.btnCheckState.TabIndex = 2;
            this.btnCheckState.Text = "Check Processes State";
            this.btnCheckState.UseVisualStyleBackColor = false;
            this.btnCheckState.Click += new System.EventHandler(this.btnCheckState_Click);
            // 
            // lblICubSim
            // 
            this.lblICubSim.BackColor = System.Drawing.SystemColors.Control;
            this.lblICubSim.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblICubSim.ForeColor = System.Drawing.Color.Black;
            this.lblICubSim.Location = new System.Drawing.Point(6, 60);
            this.lblICubSim.Name = "lblICubSim";
            this.lblICubSim.Size = new System.Drawing.Size(230, 17);
            this.lblICubSim.TabIndex = 1;
            this.lblICubSim.Text = "ICub Simulator - Not Running";
            // 
            // lblYarpServer
            // 
            this.lblYarpServer.BackColor = System.Drawing.SystemColors.Control;
            this.lblYarpServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYarpServer.ForeColor = System.Drawing.Color.Black;
            this.lblYarpServer.Location = new System.Drawing.Point(6, 30);
            this.lblYarpServer.Name = "lblYarpServer";
            this.lblYarpServer.Size = new System.Drawing.Size(230, 17);
            this.lblYarpServer.TabIndex = 0;
            this.lblYarpServer.Text = "YARP Server - Not Running";
            // 
            // btnViewNet
            // 
            this.btnViewNet.BackColor = System.Drawing.Color.Silver;
            this.btnViewNet.FlatAppearance.BorderSize = 0;
            this.btnViewNet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewNet.Font = new System.Drawing.Font("Arial", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewNet.Location = new System.Drawing.Point(9, 208);
            this.btnViewNet.Name = "btnViewNet";
            this.btnViewNet.Size = new System.Drawing.Size(144, 29);
            this.btnViewNet.TabIndex = 10;
            this.btnViewNet.Text = "View Network";
            this.btnViewNet.UseVisualStyleBackColor = false;
            this.btnViewNet.Click += new System.EventHandler(this.btnViewNet_Click);
            // 
            // pbNet
            // 
            this.pbNet.Location = new System.Drawing.Point(18, 31);
            this.pbNet.Name = "pbNet";
            this.pbNet.Size = new System.Drawing.Size(650, 683);
            this.pbNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbNet.TabIndex = 11;
            this.pbNet.TabStop = false;
            this.pbNet.Paint += new System.Windows.Forms.PaintEventHandler(this.pbNet_Paint);
            this.pbNet.Resize += new System.EventHandler(this.pbNet_Resize);
            // 
            // gbNetTools
            // 
            this.gbNetTools.Controls.Add(this.label2);
            this.gbNetTools.Controls.Add(this.clbNetworks);
            this.gbNetTools.Controls.Add(this.btnViewNet);
            this.gbNetTools.Controls.Add(this.btnDelete);
            this.gbNetTools.Controls.Add(this.btnManageNet);
            this.gbNetTools.Controls.Add(this.btnTrainTestNet);
            this.gbNetTools.Location = new System.Drawing.Point(3, 3);
            this.gbNetTools.Name = "gbNetTools";
            this.gbNetTools.Size = new System.Drawing.Size(318, 262);
            this.gbNetTools.TabIndex = 12;
            this.gbNetTools.TabStop = false;
            this.gbNetTools.Text = "Network Toolbox";
            // 
            // panelTools
            // 
            this.panelTools.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelTools.Controls.Add(this.groupBox2);
            this.panelTools.Controls.Add(this.gbNetTools);
            this.panelTools.Controls.Add(this.groupBox1);
            this.panelTools.Location = new System.Drawing.Point(18, 48);
            this.panelTools.Name = "panelTools";
            this.panelTools.Size = new System.Drawing.Size(324, 534);
            this.panelTools.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbColourToggle);
            this.groupBox2.Location = new System.Drawing.Point(3, 465);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(317, 63);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // rbColourToggle
            // 
            this.rbColourToggle.AutoSize = true;
            this.rbColourToggle.Checked = true;
            this.rbColourToggle.Location = new System.Drawing.Point(9, 36);
            this.rbColourToggle.Name = "rbColourToggle";
            this.rbColourToggle.Size = new System.Drawing.Size(151, 21);
            this.rbColourToggle.TabIndex = 0;
            this.rbColourToggle.TabStop = true;
            this.rbColourToggle.Text = "Enable colour code";
            this.rbColourToggle.UseVisualStyleBackColor = true;
            this.rbColourToggle.CheckedChanged += new System.EventHandler(this.rbColourToggle_CheckedChanged);
            this.rbColourToggle.Click += new System.EventHandler(this.rbColourToggle_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pbNet);
            this.panel1.Location = new System.Drawing.Point(15, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(721, 720);
            this.panel1.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Selected network details:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(348, 48);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(747, 755);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.rtxtNetDetails);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(739, 726);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Details";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(739, 726);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Architecture";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1095, 815);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelTools);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menu";
            this.Text = "RNN Menu";
            this.Load += new System.EventHandler(this.menu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbNet)).EndInit();
            this.gbNetTools.ResumeLayout(false);
            this.gbNetTools.PerformLayout();
            this.panelTools.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clbNetworks;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem networkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmCreateNewNet;
        private System.Windows.Forms.ToolStripMenuItem tsmSaveNet;
        private System.Windows.Forms.Button btnManageNet;
        private System.Windows.Forms.Button btnTrainTestNet;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmRunWithYARP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtxtNetDetails;
        private System.Windows.Forms.ToolStripMenuItem tsmLoadNet;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblICubSim;
        private System.Windows.Forms.Label lblYarpServer;
        private System.Windows.Forms.Button btnCheckState;
        private System.Windows.Forms.Button btnManageICub;
        private System.Windows.Forms.Button btnManageYarp;
        private System.Windows.Forms.ToolStripMenuItem tsmManageAppPaths;
        private System.Windows.Forms.ToolTip ttInfo;
        private System.Windows.Forms.Button btnViewNet;
        private System.Windows.Forms.PictureBox pbNet;
        private System.Windows.Forms.GroupBox gbNetTools;
        private System.Windows.Forms.Panel panelTools;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbColourToggle;
        private System.Windows.Forms.Button btnManageYARPRobotInterface;
        private System.Windows.Forms.Button btnManageCartesianSolver;
        private System.Windows.Forms.Label lblYARPRobotInterface;
        private System.Windows.Forms.Label lblIKinCartesianSolver;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}

