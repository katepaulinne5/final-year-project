﻿namespace DataGenApp
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenData = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtObjects = new System.Windows.Forms.TextBox();
            this.txtLocations = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGenWithGestures = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTrainingPath = new System.Windows.Forms.TextBox();
            this.txtGesturesPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBrowse2 = new System.Windows.Forms.Button();
            this.btnBrowse1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(163, 31);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(199, 22);
            this.txtFileName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Name";
            // 
            // btnGenData
            // 
            this.btnGenData.Location = new System.Drawing.Point(234, 133);
            this.btnGenData.Name = "btnGenData";
            this.btnGenData.Size = new System.Drawing.Size(128, 23);
            this.btnGenData.TabIndex = 2;
            this.btnGenData.Text = "Generate Data";
            this.btnGenData.UseVisualStyleBackColor = true;
            this.btnGenData.Click += new System.EventHandler(this.btnGenData_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(163, 133);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtObjects
            // 
            this.txtObjects.Location = new System.Drawing.Point(229, 82);
            this.txtObjects.Name = "txtObjects";
            this.txtObjects.Size = new System.Drawing.Size(45, 22);
            this.txtObjects.TabIndex = 4;
            // 
            // txtLocations
            // 
            this.txtLocations.Location = new System.Drawing.Point(314, 82);
            this.txtLocations.Name = "txtLocations";
            this.txtLocations.Size = new System.Drawing.Size(48, 22);
            this.txtLocations.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(293, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Locations";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(218, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Objects";
            // 
            // btnGenWithGestures
            // 
            this.btnGenWithGestures.Location = new System.Drawing.Point(163, 279);
            this.btnGenWithGestures.Name = "btnGenWithGestures";
            this.btnGenWithGestures.Size = new System.Drawing.Size(199, 31);
            this.btnGenWithGestures.TabIndex = 8;
            this.btnGenWithGestures.Text = "Generate with gestures";
            this.btnGenWithGestures.UseVisualStyleBackColor = true;
            this.btnGenWithGestures.Click += new System.EventHandler(this.btnGenWithGestures_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "(to save)";
            // 
            // txtTrainingPath
            // 
            this.txtTrainingPath.Location = new System.Drawing.Point(163, 187);
            this.txtTrainingPath.Name = "txtTrainingPath";
            this.txtTrainingPath.Size = new System.Drawing.Size(138, 22);
            this.txtTrainingPath.TabIndex = 10;
            // 
            // txtGesturesPath
            // 
            this.txtGesturesPath.Location = new System.Drawing.Point(163, 236);
            this.txtGesturesPath.Name = "txtGesturesPath";
            this.txtGesturesPath.Size = new System.Drawing.Size(138, 22);
            this.txtGesturesPath.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Training data path:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 241);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Gestures data path:";
            // 
            // btnBrowse2
            // 
            this.btnBrowse2.Location = new System.Drawing.Point(297, 236);
            this.btnBrowse2.Name = "btnBrowse2";
            this.btnBrowse2.Size = new System.Drawing.Size(65, 23);
            this.btnBrowse2.TabIndex = 15;
            this.btnBrowse2.Text = "Browse";
            this.btnBrowse2.UseVisualStyleBackColor = true;
            this.btnBrowse2.Click += new System.EventHandler(this.btnBrowse2_Click);
            // 
            // btnBrowse1
            // 
            this.btnBrowse1.Location = new System.Drawing.Point(297, 187);
            this.btnBrowse1.Name = "btnBrowse1";
            this.btnBrowse1.Size = new System.Drawing.Size(65, 23);
            this.btnBrowse1.TabIndex = 16;
            this.btnBrowse1.Text = "Browse";
            this.btnBrowse1.UseVisualStyleBackColor = true;
            this.btnBrowse1.Click += new System.EventHandler(this.btnBrowse1_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 333);
            this.Controls.Add(this.btnBrowse1);
            this.Controls.Add(this.btnBrowse2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtGesturesPath);
            this.Controls.Add(this.txtTrainingPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnGenWithGestures);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLocations);
            this.Controls.Add(this.txtObjects);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnGenData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFileName);
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenData;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtObjects;
        private System.Windows.Forms.TextBox txtLocations;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGenWithGestures;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTrainingPath;
        private System.Windows.Forms.TextBox txtGesturesPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnBrowse2;
        private System.Windows.Forms.Button btnBrowse1;
    }
}

