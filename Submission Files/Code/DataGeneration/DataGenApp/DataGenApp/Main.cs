﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
//using Encog.ML;
//using Encog.ML.Data;
//using Encog.ML.Data.Basic;

namespace DataGenApp
{
    public partial class Main : Form
    {

        private List<List<int>> dataTrain;
        private List<int> ideal;
        private const string mainPath = "D:\\Debug\\";
        static int elementCount = 0;
        private List<List<float>> dataTrainGestures;
        private List<List<float>> idealTrainGestures;

        public Main()
        {
            InitializeComponent();
            dataTrain = new List<List<int>>();
            ideal = new List<int>();
            dataTrainGestures = new List<List<float>>();
            idealTrainGestures = new List<List<float>>();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveCSV();
        }

        private void btnGenData_Click(object sender, EventArgs e)
        {
            if (txtObjects.Text != "" && txtLocations.Text != "")
            {
                int objects = Convert.ToInt32(txtObjects.Text);
                int locations = Convert.ToInt32(txtLocations.Text);

                dataTrain = Generate(100, objects, locations);
                dataTrain = limitPermutatations(dataTrain, objects, ref ideal);
            }
        }

        private void SaveCSV()
        {
            string fileName = "default";
            if (txtFileName.Text != "")
            {
                fileName = txtFileName.Text;
            }

            System.IO.StreamWriter file = new System.IO.StreamWriter(mainPath + fileName + ".txt");

            for (int i = 0; i < dataTrain.Count; i++)
            {
                for (int j = 0; j < dataTrain.ElementAt(i).Count; j++)
                {
                    if (j == Convert.ToInt32(txtLocations.Text) - 1)
                    {
                        file.WriteLine(dataTrain.ElementAt(i).ElementAt(j) + ", " + ideal.ElementAt(i));
                    }
                    else
                    {
                        file.Write(dataTrain.ElementAt(i).ElementAt(j) + ", ");
                    }
                }
            }

            file.Close();

        }

        private static List<List<int>> Generate(int count, int objects, int locations)
        {
            List<List<int>> dataGenerated = new List<List<int>>();
            dataGenerated = GenerateTrainingData(locations, 2);
            return dataGenerated;

        }

        private List<List<int>> limitPermutatations(List<List<int>> data, int limit, ref List<int> ideal)
        {
            List<List<int>> dataUpdated = new List<List<int>>();
            for (int i = 0; i < data.Count; i++)
            {
                int counter = 0;
                bool added = false;
                for (int j = 0; j < data.ElementAt(i).Count; j++)
                {
                    if (counter < limit)
                    {
                        if (data.ElementAt(i).ElementAt(j) == 1)
                        {
                            counter++;
                        }
                        if (j == data.ElementAt(i).Count - 1)
                        {
                            ideal.Add(counter);
                            added = true;
                        }

                    }
                    else
                    {

                    }
                }
                if (added)
                {
                    dataUpdated.Add(data.ElementAt(i));
                }
            }

            return dataUpdated;
        }
        private static List<List<int>> GenerateTrainingData(int digits, int chars)
        {
            //double[][] dataset = new double[size][];

            //double[] sequence = new double[10];
            int size = 32;
            double[][] dataset = new double[size][];
            //sequence = GeneratePermutation(dataset);
            // GeneratePermutation(null, out dataset, 10, 2);

            List<List<int>> sequence = new List<List<int>>();
            List<int> seq = new List<int>();
            GeneratePermutation(ref seq, ref sequence, "", digits, chars);

            //for (int i = 0; i < size; i++)
            //{
            //    dataset[i] = new double[digits];

            //    for (int j = 0; j < digits; j++)
            //    {
            //        //dataset[i][j] = 0;
            //        //dataset[i][j] = 1;
            //        dataset[i][j] = sequence.ElementAt(i).ElementAt(j);
            //    }
            //}

            return sequence;
        }

        private static void GeneratePermutation(ref List<int> seq, ref List<List<int>> sequence, string txt, int digits, int chars/*, out double[][] sequence*/)
        {

            //double[] seq = new double[10];
            if (digits > 0)
            {
                for (int j = 0; j < chars; j++)
                {
                    //seq.Add((int)j);
                    GeneratePermutation(ref seq, ref sequence, txt + j.ToString(), digits - 1, chars);
                }
            }
            else
            {
                //seq.Add()
                //float[] arr=new float[10];
                //seq.CopyTo(arr);
                //if (!sequence.Contains(arr.ToList()))
                //{
                //    sequence.Add(arr.ToList());
                //}
                List<char> lst = new List<char>(txt.ToList());
                List<int> tmp = lst.Select(x => int.Parse(x.ToString())).ToList();
                if (tmp.Sum() <= 12)
                {
                    using (StreamWriter writer = File.AppendText("D:\\Debug\\logTest3.txt"))
                    {
                        writer.WriteLine(String.Join(", ", lst.ToArray()));
                    }
                    //sequence.Add(tmp);
                    //seq.Clear();
                    elementCount++;
                    Console.WriteLine(txt);
                }

            }

        }

        private static double[][] SplitDataIntoRows(double[][] data, out double[][] idealData)
        {
            double[][] dataInput = new double[data.Length * data[0].Length][];
            idealData = new double[data.Length * data[0].Length][];

            for (int i = 0; i < data.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < data[i].Length; j++)
                {
                    dataInput[data[i].Length * i + j] = new double[1];
                    dataInput[data[i].Length * i + j][0] = data[i][j];
                    if (data[i][j] == 1)
                    {
                        counter++;
                    }
                    idealData[data[i].Length * i + j] = new double[1];
                    idealData[data[i].Length * i + j][0] = counter;
                }
            }
            return dataInput;
        }

        private static void readCSV(string path, ref List<List<float>> inputVals, ref List<List<float>> outputTargets)
        {
            //List<float> inputVals = new List<float>();
            //List<float> outputTargets = new List<float>();

            FileStream fs = File.OpenRead(path);
            //using (BufferedStream bs = new BufferedStream(fs))
            using (StreamReader reader = new StreamReader(fs))
            {
                for (int l = 0; l < File.ReadAllLines(path).Count(); l++)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');

                    List<float> inputValsTmp = new List<float>();
                    List<float> outputValsTmp = new List<float>();

                    int counter = 1;
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (counter == 1)
                        {
                            if (!string.IsNullOrEmpty(values[i]))
                            {
                                float val = float.Parse(values.ElementAt(i));
                                if (val != null)
                                {
                                    inputValsTmp.Add(val);
                                }
                                else
                                {
                                    counter++;
                                }
                            }
                            else
                            {
                                counter++;
                            }
                        }
                        else
                        {
                            float val = float.Parse(values.ElementAt(i));
                            if (val != null)
                            {
                                outputValsTmp.Add(val);
                            }
                        }
                    }
                    inputVals.Add(inputValsTmp);
                    outputTargets.Add(outputValsTmp);
                }
            }
            //return inputVals;
        }

        private void btnGenWithGestures_Click(object sender, EventArgs e)
        {
            List<List<float>> inputs = new List<List<float>>();
            List<List<float>> outputs = new List<List<float>>();

            //List<List<float>> inputs2 = new List<List<float>>();
            //List<List<float>> outputs2 = new List<List<float>>();
            //if (txtGesturesPath.Text != "" && txtTrainingPath.Text != "")
            if (txtTrainingPath.Text != "" && txtLocations.Text!="")
            {
                readCSV(txtTrainingPath.Text, ref inputs, ref outputs);
                //readCSV(txtGesturesPath.Text, ref inputs2, ref outputs2);
                int locations = Convert.ToInt32(txtLocations.Text);
                dataTrainGestures.Clear();
                idealTrainGestures.Clear();
                if(locations<=20)
                {
                    GenerateDataWithGestures(inputs, ref dataTrainGestures, ref idealTrainGestures,0.03f);
                }
                else
                {
                    GenerateDataWithGestures(inputs, ref dataTrainGestures, ref idealTrainGestures, 0.015f);
                }
                SaveCSVGestures();
            }
        }

        private void btnBrowse1_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadCSVDialog = new OpenFileDialog();
            loadCSVDialog.Filter = "CSV Files|*.csv";
            loadCSVDialog.Title = "Select CSV training file";
            loadCSVDialog.RestoreDirectory = true;

            if (loadCSVDialog.ShowDialog() == DialogResult.OK)
            {
                string nameFile = loadCSVDialog.SafeFileName;
                loadCSVDialog.OpenFile();
                //string directoryPath = Path.GetDirectoryName(nameFile + ".csv");
                txtTrainingPath.Text = loadCSVDialog.FileName;
            }
        }

        private void btnBrowse2_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadCSVDialog = new OpenFileDialog();
            loadCSVDialog.Filter = "CSV Files|*.csv";
            loadCSVDialog.Title = "Select CSV training file";
            loadCSVDialog.RestoreDirectory = true;

            if (loadCSVDialog.ShowDialog() == DialogResult.OK)
            {
                string nameFile = loadCSVDialog.SafeFileName;
                loadCSVDialog.OpenFile();
                //string directoryPath = Path.GetDirectoryName(nameFile + ".csv");
                txtGesturesPath.Text = loadCSVDialog.FileName;
            }
        }

        private void GenerateDataWithGestures(List<List<float>> data, ref List<List<float>> trainingData, ref List<List<float>> idealData, float adjustValue)
        {
            for (int i = 0; i < data.Count; i++)
            {
                float counter = 0.0f;
                float value = 0.0f;
                List<float> allData = new List<float>();
                List<float> ideal = new List<float>();
                for (int j = 0; j < data.ElementAt(i).Count; j++)
                {
                    if (j != data.ElementAt(i).Count - 1)
                    {
                        allData.Add(data.ElementAt(i).ElementAt(j));
                        if (data.ElementAt(i).ElementAt(j) == 1)
                        {
                            counter++;
                        }
                        ideal.Add(counter);
                    }
                    else
                    {
                        allData.Add(data.ElementAt(i).ElementAt(j));
                        if (data.ElementAt(i).ElementAt(j) == 1)
                        {
                            counter++;
                        }
                        ideal.Add(counter);
                        counter = 0;
                        for (int k = 0; k < data.ElementAt(i).Count; k++)
                        {

                            if (data.ElementAt(i).ElementAt(k) == 1)
                            {
                                //value = -0.3f + 0.03f * counter;
                                //value = -0.3f + 0.015f * counter;
                                value = -0.3f + adjustValue * counter;
                                counter++;
                            }
                            allData.Add(value);
                        }

                    }
                }
                trainingData.Add(allData);
                idealData.Add(ideal);
            }
        }



        private void SaveCSVGestures()
        {
            string fileName = "default";
            if (txtFileName.Text != "")
            {
                fileName = txtFileName.Text;
            }

            System.IO.StreamWriter file = new System.IO.StreamWriter(mainPath + fileName + ".txt");

            for (int i = 0; i < dataTrainGestures.Count; i++)
            {
                for (int j = 0; j < dataTrainGestures.ElementAt(i).Count; j++)
                {
                    if (j == Convert.ToInt32(txtLocations.Text) - 1)
                    {
                        file.WriteLine(dataTrainGestures.ElementAt(i).ElementAt(j) + ", " + idealTrainGestures.ElementAt(i).ElementAt(9));
                    }
                    else
                    {
                        file.Write(dataTrainGestures.ElementAt(i).ElementAt(j) + ", ");
                    }
                }
            }

            file.Close();

        }


    }
}
