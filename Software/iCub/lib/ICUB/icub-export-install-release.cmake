#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "iCubDev" for configuration "Release"
set_property(TARGET iCubDev APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(iCubDev PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/iCubDev.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS iCubDev )
list(APPEND _IMPORT_CHECK_FILES_FOR_iCubDev "${_IMPORT_PREFIX}/lib/iCubDev.lib" )

# Import target "ctrlLib" for configuration "Release"
set_property(TARGET ctrlLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(ctrlLib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "C:/Program Files/robotology/gsl-1.14/lib/gsl.lib;C:/Program Files/robotology/gsl-1.14/lib/gslcblas.lib;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/ctrlLib.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS ctrlLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_ctrlLib "${_IMPORT_PREFIX}/lib/ctrlLib.lib" )

# Import target "skinDynLib" for configuration "Release"
set_property(TARGET skinDynLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(skinDynLib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init;ctrlLib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/skinDynLib.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS skinDynLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_skinDynLib "${_IMPORT_PREFIX}/lib/skinDynLib.lib" )

# Import target "iKin" for configuration "Release"
set_property(TARGET iKin APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(iKin PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "ctrlLib;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init;C:/Program Files/robotology/ipopt-3.11.7/lib/libipopt.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/ifconsol.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libifcoremd.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libifportmd.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libmmd.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libirc.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/svml_dispmd.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/iKin.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS iKin )
list(APPEND _IMPORT_CHECK_FILES_FOR_iKin "${_IMPORT_PREFIX}/lib/iKin.lib" )

# Import target "iDyn" for configuration "Release"
set_property(TARGET iDyn APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(iDyn PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "iKin;skinDynLib;C:/Program Files/robotology/gsl-1.14/lib/gsl.lib;C:/Program Files/robotology/gsl-1.14/lib/gslcblas.lib;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/iDyn.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS iDyn )
list(APPEND _IMPORT_CHECK_FILES_FOR_iDyn "${_IMPORT_PREFIX}/lib/iDyn.lib" )

# Import target "learningMachine" for configuration "Release"
set_property(TARGET learningMachine APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(learningMachine PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "C:/Program Files/robotology/gsl-1.14/lib/gsl.lib;C:/Program Files/robotology/gsl-1.14/lib/gslcblas.lib;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init;YARP::YARP_gsl"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/learningMachine.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS learningMachine )
list(APPEND _IMPORT_CHECK_FILES_FOR_learningMachine "${_IMPORT_PREFIX}/lib/learningMachine.lib" )

# Import target "perceptiveModels" for configuration "Release"
set_property(TARGET perceptiveModels APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(perceptiveModels PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "ctrlLib;learningMachine;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/perceptiveModels.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS perceptiveModels )
list(APPEND _IMPORT_CHECK_FILES_FOR_perceptiveModels "${_IMPORT_PREFIX}/lib/perceptiveModels.lib" )

# Import target "actionPrimitives" for configuration "Release"
set_property(TARGET actionPrimitives APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(actionPrimitives PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "perceptiveModels;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/actionPrimitives.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS actionPrimitives )
list(APPEND _IMPORT_CHECK_FILES_FOR_actionPrimitives "${_IMPORT_PREFIX}/lib/actionPrimitives.lib" )

# Import target "optimization" for configuration "Release"
set_property(TARGET optimization APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(optimization PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "ctrlLib;C:/Program Files/robotology/ipopt-3.11.7/lib/libipopt.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/ifconsol.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libifcoremd.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libifportmd.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libmmd.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/libirc.lib;C:/Program Files/robotology/ipopt-3.11.7/lib/svml_dispmd.lib;YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/optimization.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS optimization )
list(APPEND _IMPORT_CHECK_FILES_FOR_optimization "${_IMPORT_PREFIX}/lib/optimization.lib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
