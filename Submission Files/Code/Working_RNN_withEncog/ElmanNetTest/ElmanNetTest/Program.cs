﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Encog.Engine.Network.Activation;
//using Encog.Examples.Util;
using Encog.ML;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.ML.Train;
using Encog.ML.Train.Strategy;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training;
using Encog.Neural.Networks.Training.Anneal;
using Encog.Neural.Networks.Training.Lma;
using Encog.Neural.Networks.Training.Propagation.Back;
//org.encog.neural.networks.training.propagation.Propagation
using Encog.Neural.Networks.Training.Propagation.SCG;
using Encog.Neural.Pattern;
using SuperUtils = Encog.Util.NetworkUtil.NetworkUtility;
using SuperUtilsTrainer = Encog.Util.NetworkUtil.TrainerHelper;



namespace ElmanNetTest
{
    class Program
    {

        public static double[,] XOR_INPUT = new double[,]{ { 0.0, 0.0 }, { 1.0, 0.0 },
            { 0.0, 1.0 }, { 1.0, 1.0 } };
        public static double[,] XOR_IDEAL = new double[,] { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };

        static void Main(string[] args)
        {

            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("D:/Debug/Net.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Net.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            List<List<float>> input = new List<List<float>>();
            List<List<float>> output = new List<List<float>>();
            //string path = "C:\\Users\\Kate\\Desktop\\Kate\\University\\Final Year\\Final Year Project\\final-year-project\\XOR_training_data.csv";
            string path = "D:\\Debug\\NetTrainDataXORTest.csv";

            readCSV(path, ref input, ref output);
            //ElmanNet net = new ElmanNet();
            //ElmanNet net = new ElmanNet(4, 10, "test");
            ElmanNet net = new ElmanNet(4, 3, "test", input, output);
            List<float> errorTrain = new List<float>();
            net.Train(ref errorTrain);

            Console.WriteLine("Net created: " + net.NetName);

            List<float> inp = new List<float>();
            inp.Add(1.0f);
            inp.Add(1.0f);
            float val = net.Test(inp);
            Console.WriteLine(val);

            inp.Clear();
            inp.Add(0.0f);
            inp.Add(1.0f);
            val = net.Test(inp);
            Console.WriteLine(val);

            inp.Clear();
            inp.Add(1.0f);
            inp.Add(0.0f);
            val = net.Test(inp);
            Console.WriteLine(val);

            inp.Clear();
            inp.Add(0.0f);
            inp.Add(0.0f);
            val = net.Test(inp);
            Console.WriteLine(val);

            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();


            BuildNetWithEncog();

            Console.ReadKey();
        }

        private static void BuildNetWithEncog()
        {
            //var temp = new TemporalXOR();
            IMLDataSet trainingSet = Generate(100);


            //ElmanPattern elmanPattern = new ElmanPattern { ActivationFunction = new ActivationSigmoid(), InputNeurons = 1 };
            ElmanPattern elmanPattern = new ElmanPattern { ActivationFunction = new ActivationSigmoid(), InputNeurons = 40 };

            elmanPattern.AddHiddenLayer(15);
            //elmanPattern.OutputNeurons = 1;
            //elmanPattern.OutputNeurons = 20;
            elmanPattern.OutputNeurons = 40;
            IMLMethod elmanNet = elmanPattern.Generate();
            BasicNetwork net5 = (BasicNetwork)elmanNet;
            double error = Train((BasicNetwork)elmanNet, trainingSet);
            BasicNetwork net = (BasicNetwork)elmanNet;
            Console.WriteLine(@"Neural Network Results:");
            foreach (IMLDataPair pair in trainingSet)
            {
                IMLData output = net.Compute(pair.Input);
                for (int i = 0; i < 20; i++)
                {
                    Console.WriteLine(pair.Input[i] + @", actual=" + output[i] + @",ideal=" + pair.Ideal[i]);
                }
            }

            Console.WriteLine(error);

            BasicLayer input, hidden;
            BasicNetwork net2 = new BasicNetwork();
            net2.AddLayer(input = new BasicLayer(1));
            net2.AddLayer(hidden = new BasicLayer(2));
            net2.AddLayer(new BasicLayer(1));
            input.ContextFedBy = hidden;
            net2.Structure.FinalizeStructure();
            net2.Reset();
        }

        private static BasicMLDataSet Generate(int count)
        {
            double[] seq = { 1.0, 0.0, 1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 1.0, 1.0, 1.0, 0.0 };
            //double[] seq = { 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            //                 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0};
            //double[] seq2 = { 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 4.0, 5.0,
            //                  0.0, 0.0, 0.0, 1.0, 1.0, 2.0, 2.0, 2.0, 3.0, 3.0};
            double[][] input;
            double[][] ideal;
            input = new double[count][];
            ideal = new double[count][];

            System.IO.StreamWriter file = new System.IO.StreamWriter("D:/Debug/NetTrainData.txt");
            System.IO.StreamWriter file2 = new System.IO.StreamWriter("D:/Debug/NetTrainData2.txt");


            for (int i = 0; i < input.Length; i++)
            {
                input[i] = new double[1];
                ideal[i] = new double[1];
                input[i][0] = seq[i
                        % seq.Length];
                ideal[i][0] = seq[(i + 1)
                        % seq.Length];

                file2.WriteLine(input[i][0] + ", " + ideal[i][0]);
            }

            file2.Close();

            //double[][] data7a = new double[1024][];
            //double[][] data7 = new double[1024][];
            //double[][] idealData7 = new double[1024][];
            //double[][] idealData7a = new double[1024 * 10][];
            //data7a = GenerateTrainingData(10, 2);
            //data7 = SplitDataIntoRows(data7a, out idealData7a);


            //for (int i = 0; i < data7a.Length; i++)
            //{
            //    int counter = 0;
            //    for (int j = 0; j < data7a[i].Length; j++)
            //    {
            //        if (counter < 5)
            //        {
            //            if (data7a[i][j] == 1)
            //            {
            //                counter++;
            //            }
            //            if (j == 5)
            //            {
            //                idealData7[i] = new double[1];
            //                idealData7[i][0] = counter;
            //                //file.WriteLine(data7a[i][j] + ", " + idealData[i][0]);
            //            }
            //            else
            //            {
            //                //file.Write(data[i][j] + ", ");
            //            }
            //        }
            //        else
            //        {

            //        }
            //    }
            //}

            double[][] data = new double[160][];
            double[][] data2 = new double[160][];
            double[][] idealData = new double[1024][];
            double[][] idealData2 = new double[160 * 10][];
            data = GenerateTrainingData(5, 2);
            data2 = SplitDataIntoRows(data, out idealData2);


            for (int i = 0; i < data.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < data[i].Length; j++)
                {

                    if (data[i][j] == 1)
                    {
                        counter++;
                    }
                    if (j == 5)
                    {
                        idealData[i] = new double[1];
                        idealData[i][0] = counter;
                        file.WriteLine(data[i][j] + ", " + idealData[i][0]);
                    }
                    else
                    {
                        file.Write(data[i][j] + ", ");
                    }
                }
            }

            double[][] data6 = new double[1600][];
            double[][] idealData6 = new double[1600][];

            for (int it = 0; it < 10; it++)
            {
                for (int i = 0; i < data2.Length; i++)
                {
                    data6[i + 160 * it] = new double[1];
                    idealData6[i + 160 * it] = new double[1];
                    for (int j = 0; j < data2[i].Length; j++)
                    {
                        data6[i + 160 * it][j] = data2[i][j];
                        if (j == data2[i].Length - 1)
                        {
                            idealData6[i + 160 * it][0] = idealData2[i][0];
                        }
                    }
                }
            }

            file.Close();

            double[][] data3 = new double[92][];
            double[][] idealData3 = new double[92][];

            List<List<float>> inputTmp = new List<List<float>>();
            List<List<float>> outputTmp = new List<List<float>>();

            readCSV("D:\\Debug\\NetTrainData2.csv", ref inputTmp, ref outputTmp);

            for (int i = 0; i < inputTmp.Count; i++)
            {
                data3[i] = new double[inputTmp.ElementAt(i).Count];
                idealData3[i] = new double[outputTmp.ElementAt(i).Count];
                for (int j = 0; j < inputTmp.ElementAt(i).Count; j++)
                {
                    data3[i][j] = inputTmp.ElementAt(i).ElementAt(j);
                    if (j == inputTmp.ElementAt(i).Count - 1)
                    {
                        idealData3[i][0] = outputTmp.ElementAt(i).ElementAt(0);
                    }
                }
            }


            data3 = SplitDataIntoRows(data3, out idealData3);

            double[][] data4 = new double[47][];
            double[][] idealData4 = new double[47][];

            List<List<float>> inputTmp1 = new List<List<float>>();
            List<List<float>> outputTmp1 = new List<List<float>>();

            readCSV("D:\\Debug\\NetTrainData3.csv", ref inputTmp1, ref outputTmp1);

            for (int i = 0; i < inputTmp1.Count; i++)
            {
                data4[i] = new double[inputTmp1.ElementAt(i).Count];
                idealData4[i] = new double[outputTmp1.ElementAt(i).Count];
                for (int j = 0; j < inputTmp1.ElementAt(i).Count; j++)
                {
                    data4[i][j] = inputTmp1.ElementAt(i).ElementAt(j);
                    if (j == inputTmp1.ElementAt(i).Count - 1)
                    {
                        idealData4[i][0] = outputTmp1.ElementAt(i).ElementAt(0);
                    }
                }
            }


            data4 = SplitDataIntoRows(data4, out idealData4);
            double[][] data5 = new double[4700][];
            double[][] idealData5 = new double[4700][];

            for (int it = 0; it < 10; it++)
            {
                for (int i = 0; i < data4.Length; i++)
                {
                    data5[i+470*it] = new double[1];
                    idealData5[i + 470 * it] = new double[1];
                    for (int j = 0; j < data4[i].Length; j++)
                    {
                        data5[i + 470 * it][j] = data4[i][j];
                        if (j == data4[i].Length - 1)
                        {
                            idealData5[i + 470 * it][0] = idealData4[i][0];
                        }
                    }
                }
            }


            

            double[][] data8 = new double[51][];
            double[][] idealData8 = new double[51][];

            List<List<float>> inputTmp2 = new List<List<float>>();
            List<List<float>> outputTmp2 = new List<List<float>>();

            readCSV("D:\\Debug\\NetTrainData5x10x10.csv", ref inputTmp2, ref outputTmp2);

            for (int i = 0; i < inputTmp2.Count; i++)
            {
                data8[i] = new double[inputTmp2.ElementAt(i).Count];
                idealData8[i] = new double[outputTmp2.ElementAt(i).Count];
                for (int j = 0; j < inputTmp2.ElementAt(i).Count; j++)
                {
                    data8[i][j] = inputTmp2.ElementAt(i).ElementAt(j);
                    if (j == inputTmp2.ElementAt(i).Count - 1)
                    {
                        idealData8[i][0] = outputTmp2.ElementAt(i).ElementAt(0);
                    }
                }
            }


            data8 = SplitDataIntoRows(data8, out idealData8);
            double[][] data9 = new double[5100][];
            double[][] idealData9 = new double[5100][];

            for (int it = 0; it < 10; it++)
            {
                for (int i = 0; i < data8.Length; i++)
                {
                    data9[i + 510 * it] = new double[1];
                    idealData9[i + 510 * it] = new double[1];
                    for (int j = 0; j < data8[i].Length; j++)
                    {
                        data9[i + 510 * it][j] = data8[i][j];
                        if (j == data8[i].Length - 1)
                        {
                            idealData9[i + 510 * it][0] = idealData8[i][0];
                        }
                    }
                }
            }

            List<List<float>> inputTmp2a = new List<List<float>>();
            List<List<float>> outputTmp2a = new List<List<float>>();

            //readCSV("D:\\Debug\\NetTrainData5x10x10.csv", ref inputTmp2, ref outputTmp2);
            //readCSV("D:\\Debug\\10x20TrainingData2.csv", ref inputTmp2a, ref outputTmp2a);
            //readCSV("D:\\Debug\\5x10DataTrainWithGestures.csv", ref inputTmp2a, ref outputTmp2a);
            //readCSV("D:\\Debug\\10x20DataWithGestures.csv", ref inputTmp2a, ref outputTmp2a);
            readCSV("D:\\Debug\\10x20DataGesturesCorrect.csv", ref inputTmp2a, ref outputTmp2a);
            

            double[][] data8a = new double[inputTmp2a.Count][];
            double[][] idealData8a = new double[outputTmp2a.Count][];
            for (int i = 0; i < inputTmp2a.Count; i++)
            {
                data8a[i] = new double[inputTmp2a.ElementAt(i).Count];
                idealData8a[i] = new double[outputTmp2a.ElementAt(i).Count];
                for (int j = 0; j < inputTmp2a.ElementAt(i).Count; j++)
                {
                    data8a[i][j] = inputTmp2a.ElementAt(i).ElementAt(j);
                    if (j == inputTmp2a.ElementAt(i).Count - 1)
                    {
                        idealData8a[i][0] = outputTmp2a.ElementAt(i).ElementAt(0);
                    }
                }
            }


            


            data8a = SplitDataIntoRows2(data8a, out idealData8a);
            int size = data8a.Length;
            double[][] data9a = new double[size * 10][];
            double[][] idealData9a = new double[size * 10][];

            for (int it = 0; it < 10; it++)
            {
                for (int i = 0; i < data8a.Length; i++)
                {
                    data9a[i + size * it] = new double[1];
                    idealData9a[i + size * it] = new double[1];
                    for (int j = 0; j < data8a[i].Length; j++)
                    {
                        data9a[i + size * it][j] = data8a[i][j];
                        if (j == data8a[i].Length - 1)
                        {
                            idealData9a[i + size * it][0] = idealData8a[i][0];
                        }
                    }
                }
            }

            int counterIdeal = 0;
            double[][] data8b = new double[inputTmp2a.Count][];
            double[][] idealData8b = new double[inputTmp2a.Count][];
            for (int i = 0; i < inputTmp2a.Count; i++)
            {
                data8b[i] = new double[inputTmp2a.ElementAt(i).Count];
                idealData8b[i] = new double[inputTmp2a.ElementAt(i).Count];
                
                for (int j = 0; j < inputTmp2a.ElementAt(i).Count; j++)
                {
                    data8b[i][j] = inputTmp2a.ElementAt(i).ElementAt(j);
                    //if (j == inputTmp2a.ElementAt(i).Count - 1)
                    //{
                    if (inputTmp2a.ElementAt(i).ElementAt(j)==1)
                    {
                        counterIdeal++;
                    }
                    //idealData8b[i][j] = outputTmp2a.ElementAt(i).ElementAt(j);
                    idealData8b[i][j] = counterIdeal;
                    //}
                    if(j>=inputTmp2a.ElementAt(i).Count-1)
                    {
                        counterIdeal = 0;
                    }
                }
            }


            //int size2 = data8b.Length;
            //double[][] data9b = new double[size2 * 10][];
            //double[][] idealData9b = new double[size2 * 10][];
            //for (int it = 0; it < 100; it++)
            //{
            //    for (int i = 0; i < data8b.Length; i++)
            //    {
            //        data9b[i + size2 * it] = new double[10];
            //        idealData9b[i + size2 * it] = new double[10];
            //        for (int j = 0; j < data8b[i].Length; j++)
            //        {
            //            data9b[i + size2 * it][j] = data8b[i][j];
            //            if (j == data8b[i].Length - 1)
            //            {
            //                idealData9b[i + size2 * it][j] = idealData8b[i][j];
            //            }
            //        }
            //    }
            //}
            //return new BasicMLDataSet(input, ideal);
            //return new BasicMLDataSet(data2, idealData2);
            //return new BasicMLDataSet(data3, idealData3);
            //return new BasicMLDataSet(data5, idealData5);
            //return new BasicMLDataSet(data6, idealData6);
            //return new BasicMLDataSet(data9, idealData9);
            //return new BasicMLDataSet(data9a, idealData9a);
            return new BasicMLDataSet(data8b, idealData8b);
        }

        private static double[][] SplitDataIntoRows2(double[][] data, out double[][] idealData)
        {
            double[][] dataInput = new double[data.Length * data[0].Length][];
            idealData = new double[data.Length * data[0].Length][];

            for (int i = 0; i < data.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < data[i].Length; j++)
                {
                    dataInput[data[i].Length * i + j] = new double[1];
                    dataInput[data[i].Length * i + j][0] = data[i][j];
                    if (data[i][j] == 1)
                    {
                        counter++;
                    }
                    idealData[data[i].Length * i + j] = new double[1];
                    idealData[data[i].Length * i + j][0] = counter;
                }
            }
            return dataInput;
        }

        private static double Train(BasicNetwork network, IMLDataSet trainingSet)
        {
            ICalculateScore score = new TrainingSetScore(trainingSet);
            //IMLTrain trainAlt = new NeuralSimulatedAnnealing(network, score, 266402, 2, 10);
            IMLTrain trainAlt = new NeuralSimulatedAnnealing(network, score, 1334, 2, 20);
            IMLTrain trainMain;
            IMLTrain trainSCG;
            //if (Method.Equals("Leven"))
            //{
            //    Console.WriteLine("Using LevenbergMarquardtTraining");
            //    trainMain = new LevenbergMarquardtTraining(network, trainingSet);
            //}
            //else
            BasicNetwork networkSCG = network;
            //IMLTrain trainAlt2 = new NeuralSimulatedAnnealing(networkSCG, score, 512, 2, 20);
            //trainSCG = new ScaledConjugateGradient(networkSCG, trainingSet);
            //trainMain = new Backpropagation(network, trainingSet, 0.0000001, 0.0);//,0.0002,0.1);
            trainMain = new Backpropagation(network, trainingSet, 0.000001, 0.0);//,0.0002,0.1);

            var stop = new StopTrainingStrategy();
            trainMain.AddStrategy(new Greedy());
            trainMain.AddStrategy(new HybridStrategy(trainAlt));
            trainMain.AddStrategy(stop);

            int epoch = 0;
            while (!stop.ShouldStop())
            //while (trainMain.Error>0.5 || epoch==0)
            {
                trainMain.Iteration();
                //trainSCG.Iteration(10);
                Console.WriteLine("Training  net, Epoch #" + epoch + " Error:" + trainMain.Error);
                epoch++;
            }

            //var stop2 = new StopTrainingStrategy();
            //trainSCG.AddStrategy(new Greedy());
            //trainSCG.AddStrategy(new HybridStrategy(trainAlt2));
            //trainSCG.AddStrategy(stop2);
            //int epoch2 = 0;
            //while (!stop2.ShouldStop())
            ////while (trainMain.Error>0.5 || epoch==0)
            //{
            //    //trainMain.Iteration(10);
            //    trainSCG.Iteration(10);
            //    Console.WriteLine("Training  net, SCG Epoch #" + epoch2 + " Error:" + trainSCG.Error);
            //    epoch2++;
            //}
            trainMain.FinishTraining();
            //trainSCG.FinishTraining();
            return trainMain.Error;
        }

        private static double[][] GenerateTrainingData(int digits, int chars)
        {
            //double[][] dataset = new double[size][];

            //double[] sequence = new double[10];
            int size = 32;
            double[][] dataset = new double[size][];
            //sequence = GeneratePermutation(dataset);
            // GeneratePermutation(null, out dataset, 10, 2);

            List<List<float>> sequence = new List<List<float>>();
            List<float> seq = new List<float>();
            GeneratePermutation(ref seq, ref sequence, "", digits, chars);

            for (int i = 0; i < size; i++)
            {
                dataset[i] = new double[digits];

                for (int j = 0; j < digits; j++)
                {
                    //dataset[i][j] = 0;
                    //dataset[i][j] = 1;
                    dataset[i][j] = sequence.ElementAt(i).ElementAt(j);
                }
            }

            return dataset;
        }

        private static void GeneratePermutation(ref List<float> seq, ref List<List<float>> sequence, string txt, int digits, int chars/*, out double[][] sequence*/)
        {

            //double[] seq = new double[10];
            if (digits > 0)
            {
                for (int j = 0; j < chars; j++)
                {
                    seq.Add((float)j);
                    GeneratePermutation(ref seq, ref sequence, txt + j.ToString(), digits - 1, chars);
                }
            }
            else
            {
                //seq.Add()
                //float[] arr=new float[10];
                //seq.CopyTo(arr);
                //if (!sequence.Contains(arr.ToList()))
                //{
                //    sequence.Add(arr.ToList());
                //}
                List<char> lst = new List<char>(txt.ToList());
                List<float> tmp = lst.Select(x => float.Parse(x.ToString())).ToList();
                sequence.Add(tmp);
                //seq.Clear();
                Console.WriteLine(txt);

            }

        }

        private static double[][] SplitDataIntoRows(double[][] data, out double[][] idealData)
        {
            double[][] dataInput = new double[data.Length * data[0].Length][];
            idealData = new double[data.Length * data[0].Length][];

            for (int i = 0; i < data.Length; i++)
            {
                int counter = 0;
                for (int j = 0; j < data[i].Length; j++)
                {
                    dataInput[data[i].Length * i + j] = new double[1];
                    dataInput[data[i].Length * i + j][0] = data[i][j];
                    if (data[i][j] == 1)
                    {
                        counter++;
                    }
                    idealData[data[i].Length * i + j] = new double[1];
                    idealData[data[i].Length * i + j][0] = counter;
                }
            }
            return dataInput;
        }

        private static void readCSV(string path, ref List<List<float>> inputVals, ref List<List<float>> outputTargets)
        {
            //List<float> inputVals = new List<float>();
            //List<float> outputTargets = new List<float>();

            FileStream fs = File.OpenRead(path);
            using (StreamReader reader = new StreamReader(fs))
            {
                for (int l = 0; l < File.ReadAllLines(path).Count(); l++)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');

                    List<float> inputValsTmp = new List<float>();
                    List<float> outputValsTmp = new List<float>();

                    int counter = 1;
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (counter == 1)
                        {
                            if (!string.IsNullOrEmpty(values[i]))
                            {
                                float val = float.Parse(values.ElementAt(i));
                                if (val != null)
                                {
                                    inputValsTmp.Add(val);
                                }
                                else
                                {
                                    counter++;
                                }
                            }
                            else
                            {
                                counter++;
                            }
                        }
                        else
                        {
                            float val = float.Parse(values.ElementAt(i));
                            if (val != null)
                            {
                                outputValsTmp.Add(val);
                            }
                        }
                    }
                    inputVals.Add(inputValsTmp);
                    outputTargets.Add(outputValsTmp);
                }
            }
            //return inputVals;
        }
    }
}
