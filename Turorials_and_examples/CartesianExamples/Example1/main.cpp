#include <cstdio>
#include <cmath>
#include <yarp/os/Network.h>
#include <yarp/os/RFModule.h>
#include <yarp/os/RateThread.h>
#include <yarp/os/Time.h>
#include <yarp/sig/Vector.h>
#include <yarp/math/Math.h>
#include <yarp/dev/Drivers.h>
#include <yarp/dev/CartesianControl.h>
#include <yarp/dev/PolyDriver.h>

#define CTRL_THREAD_PER     0.02    // [s]
#define PRINT_STATUS_PER    1.0     // [s]
#define MAX_TORSO_PITCH     30.0    // [deg]

using namespace std;
using namespace yarp::os;
using namespace yarp::dev;
using namespace yarp::sig;
using namespace yarp::math;

class CtrlThread : public RateThread,
	public CartesianEvent
{
protected:
	PolyDriver client;
	ICartesianControl *icart;

	Vector xd;
	Vector od;

	int startup_context_id;

	double t;
	double t0;
	double t1;

	virtual void cartesianEventCallback()
	{
		fprintf(stdout, "20%% of trajectory attained\n");
	}

public:
	CtrlThread(const double period) :RateThread(int(period*1000.0))
	{
		cartesianEventParameters.type = "motion-ongoing";
		cartesianEventParameters.motionOngoingCheckPoint = 0.2;
	}

	virtual bool threadInit()
	{
		Property option;
		option.put("device", "cartesiancontrollerclient");
		option.put("remote", "/icub/cartesianController/right_arm");

	}
};

int main()
{

}