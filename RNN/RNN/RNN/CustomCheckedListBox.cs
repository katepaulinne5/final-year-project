﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace RNN
{
    public class CustomCheckedListBox : CheckedListBox
    {
        Color checkedItemColor = Color.Green;

        public CustomCheckedListBox()
        {
            DoubleBuffered = true;
        }
        public List<int> Completionlist = new List<int>();
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            //    Size checkSize = CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.MixedNormal);
            //    int dx = (e.Bounds.Height - checkSize.Width) / 2;

            //    e.DrawBackground();
            //    bool isChecked = GetItemChecked(e.Index);
            //    CheckBoxRenderer.DrawCheckBox(e.Graphics, new Point(dx, e.Bounds.Top + dx), isChecked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            //    Font myFont = e.Font;
            //    Brush myBrush;
            //    int i = e.Index;
            //    if (Completionlist.Contains(i))
            //    {
            //        myBrush = Brushes.Green;
            //    }
            //    else
            //    {
            //        myBrush = Brushes.Black;
            //    }
            //    e.Graphics.DrawString(this.Items[i].ToString(), myFont, myBrush, new Rectangle(e.Bounds.Height, e.Bounds.Top, e.Bounds.Width - e.Bounds.Height, e.Bounds.Height), StringFormat.GenericDefault);
            Size checkSize = CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.MixedNormal);
            int dx = (e.Bounds.Height - checkSize.Width) / 2;
            e.DrawBackground();
            bool isChecked = GetItemChecked(e.Index);//For some reason e.State doesn't work so we have to do this instead.
            CheckBoxRenderer.DrawCheckBox(e.Graphics, new Point(dx, e.Bounds.Top + dx), isChecked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            using (StringFormat sf = new StringFormat { LineAlignment = StringAlignment.Center })
            {
                using (Brush brush = new SolidBrush(isChecked ? CheckedItemColor : ForeColor))
                {
                    e.Graphics.DrawString(Items[e.Index].ToString(), Font, brush, new Rectangle(e.Bounds.Height, e.Bounds.Top, e.Bounds.Width - e.Bounds.Height, e.Bounds.Height), sf);
                }
            }
        }

        public Color CheckedItemColor
        {
            get { return checkedItemColor; }
            set
            {
                checkedItemColor = value;
                Invalidate();
            }
        }
    }

}
