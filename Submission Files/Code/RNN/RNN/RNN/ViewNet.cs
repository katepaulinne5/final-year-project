﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuickGraph;
using RNN.Model;

namespace RNN
{
    public partial class ViewNet : Form
    {
        private ElmanNet net;
        public ViewNet()
        {
            InitializeComponent();
        }

        public ViewNet(ElmanNet _net)
        {
            InitializeComponent();
            net = _net;
            //DrawNet();
        }

        private void DrawNet(PaintEventArgs e)
        {
            var graph = new AdjacencyGraph<int, TaggedEdge<int, string>>();
            Dictionary<LayerType, List<Node>> graphData = new Dictionary<LayerType, List<Node>>();

            graphData=net.getDataForDrawing();
            int counter = 0;
            List<Point> trackNodes = new List<Point>();
            List<Point> prevNodes = new List<Point>();
            Point p1 = new Point();
            Point p2 = new Point();
            Point p3 = new Point();
            Point p4 = new Point();
            int offset = 0;
            foreach (KeyValuePair <LayerType,List<Node>> keyPair in graphData)
            {
                
                int layerRectSizeX = 22;
                int layerRectSizeY = 21;
                Point startL=new Point();
                int y = 20;
                int x = 40+200*counter;
                if(keyPair.Key==LayerType.Context)
                {
                    y = 20 + 30*6+30;
                    x = 40;
                    counter--;
                }
                else if(trackNodes.Count>0)
                {
                    prevNodes = trackNodes.ToList();
                    trackNodes.Clear();
                }
                if(keyPair.Key!=LayerType.Input &&keyPair.Key!=LayerType.Context)
                {
                    offset += 60;
                    y += offset;
                }
                else
                {
                    offset = 0;
                }

                for(int i=0;i<keyPair.Value.Count;i++)
                {
                    //graph.AddVertex(i);
                    DrawCircle(x,y,e);
                    

                    if (keyPair.Key != LayerType.Context && keyPair.Key != LayerType.Input)
                    {
                        for(int j=0;j<prevNodes.Count;j++)
                        {
                            DrawLine(prevNodes.ElementAt(j),new Point(x,y+5),e);
                        }
                        if(keyPair.Key == LayerType.Hidden&&i==keyPair.Value.Count-1)
                        {
                            p1.X = x+5;
                            p1.Y = y+15;//y + 15+9*30;
                            p2.X = p1.X;
                            p2.Y = p4.Y + 20;
                            p3.X = p4.X;
                            p3.Y = p2.Y;
                            DrawArrow(p1, p2, p3, p4, e);
                        }
                        
                    }
                    if (keyPair.Key == LayerType.Context)
                    {
                        p4.X = x+6;
                        p4.Y = y+15;
                    }
                    trackNodes.Add(new Point(x+10, y+5));

                    if (i == 0)
                    {
                        startL.X = x - 6;
                        startL.Y = y-6;
                        layerRectSizeY += 30;
                    }
                    else if(i==keyPair.Value.Count-1)
                    {
                        DrawLayerRect(startL.X, startL.Y, layerRectSizeX, layerRectSizeY, e);
                    }
                    else
                    {
                        layerRectSizeY += 30;
                    }

                    y += 30;

                    
                }
                counter++;
                
            }
            
        }

        private void DrawCircle(int x, int y, PaintEventArgs e)
        {
            int size = 10;
            //Graphics gr = CreateGraphics();
            Rectangle rect = new Rectangle(x, y, size, size);
            e.Graphics.DrawEllipse(new Pen(Color.Blue), rect);
        }

        private void DrawLine(Point point1,Point point2,PaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(Color.Purple), point1, point2);
        }

        private void DrawLayerRect(int x,int y, int sizeX, int sizeY, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.Black), x, y, sizeX, sizeY);
        }

        private void DrawArrow(Point p1, Point p2,Point p3, Point p4, PaintEventArgs e)
        {
            Pen pen=new Pen(Color.Black,4);
            e.Graphics.DrawLine(pen, p1, p2);
            e.Graphics.DrawLine(pen, p2, p3);
            pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            e.Graphics.DrawLine(pen, p3, p4);

        }

        private void pbNet_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            DrawNet(e);
        }

        private void pbNet_Resize(object sender, EventArgs e)
        {
            pbNet.Invalidate();
        }
    }
}
