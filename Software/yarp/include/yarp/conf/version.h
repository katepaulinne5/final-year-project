/*
 * Copyright: (C) 2009 RobotCub Consortium
 * Author: Paul Fitzpatrick
 * CopyPolicy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT
 */


#ifndef YARP_CONFIG_VERSION_H
#define YARP_CONFIG_VERSION_H

#define YARP_VERSION_MAJOR 2
#define YARP_VERSION_MINOR 3
#define YARP_VERSION_PATCH 68
#define YARP_VERSION_TWEAK 
#define YARP_VERSION "2.3.68~dev"
#define YARP_VERSION_SHORT "2.3.68"

#endif
