﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ElmanNetTest
{
    public class Layer
    {
        private List<Node> nodes = new List<Node>();
        private List<float> nodesValues=new List<float>();
        private List<Layer> inputLayer = new List<Layer>();
        private int nodesNo;
        private string layerName;
        private LayerType layerType;
        private string errorMsg;
        private bool activeLayer=false;
        private List<Node> bias = new List<Node>();

        public Layer(int numberOfNodes, string name, List<Layer> inputLayers=null)
        {
            nodesNo = numberOfNodes;
            layerName = name;
 
            if(!Enum.TryParse(name, out layerType))
            {
                //add a check for this? up one class
                errorMsg = "incorrect layer name";
            }
            
            //create nodes
            for(int i=0;i<nodesNo;i++)
            {
                if (inputLayers != null)
                {
                    Node newNode = new Node(inputLayers);
                    nodes.Add(newNode);
                    nodesValues.Add(newNode.NodeValue);
                    inputLayer = inputLayers;
                    //inputValues = inputLayers.ElementAt(1).inputValues;
                }
                else
                {
                    Node newNode = new Node();
                    nodes.Add(newNode);
                    nodesValues.Add(newNode.NodeValue);
                }

                
            }
            if (layerType == LayerType.Hidden)
            {
                activeLayer = true;

            }
            else if (layerType == LayerType.Context)
            {
                ResetContextInputs();
            }


            if (layerType == LayerType.Hidden||layerType==LayerType.Output)
            {
                for(int i=0;i<nodesNo;i++)
                {
                    nodes.ElementAt(i).BiasNode = 0.1f;
                }

            }
        }

        public void ResetContextInputs()
        {
            if (layerType == LayerType.Context)
            {
                for (int i = 0; i < nodesNo; i++)
                {
                    nodes.ElementAt(i).NodeValue = 0.0f;
                }
            }
        }

        //public List<float> GetNodesValues()
        //{
        //    List<float> vals = new List<float>();
        //    for(int i=0;i<nodes.Count;i++)
        //    {
        //        vals.Add(nodes.ElementAt(i).NodeValue);
        //    }

        //    return vals;
        //}

        //public void getActivationForNodes()
        //{
        //    if (activeLayer)
        //    {
        //        activationValues.Clear();
        //        for (int i = 0; i < nodes.Count; i++)
        //        {
        //            //nodes.ElementAt(i).activateNode(inputValues,inputLayer);
        //            //activationValues.Add(nodes.ElementAt(i).GetActivationVal);
        //        }
        //    }
        //}

        public float calculateSigmoidValue(float x)
        {
            float activationValTmp;
            //activation equation
            double val = Convert.ToDouble(x);
            return activationValTmp = (float)(1 / (1 + Math.Exp(-val)));
        }

        public float calculateActivationDerivative(float x)
        {
            return x * (1 - x);
        }

        public void addToInputLayer(Layer l)
        {
            inputLayer.Add(l);
        }

        public void setNodeValues(List<float>values)
        {
            for(int i=0; i<nodes.Count;i++)
            {
                nodes.ElementAt(i).NodeValue = values.ElementAt(i);
            }
        }

        public List<Node> GetNodes
        {
            get { return nodes; }
        }

        public List<float> GetNodesValues
        {
            get { return nodesValues; }
        }

        public LayerType LayerType
        {
            get { return layerType; }
        }

        public string LayerName
        {
            get { return layerName; }
        }

    }
}
