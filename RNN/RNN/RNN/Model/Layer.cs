﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RNN.Model
{
    public class Layer
    {
        private List<Node> nodes = new List<Node>();
        private List<float> activationValues=new List<float>();
        private List<float> inputValues = new List<float>();
        private List<Layer> inputLayer = new List<Layer>();
        private int nodesNo;
        private string layerName;
        private LayerType layerType;
        private string errorMsg;
        private bool activeLayer=false;

        public Layer(int numberOfNodes, string name, List<float> inputVals=null, List<Layer> inputLayers=null)
        {
            nodesNo = numberOfNodes;
            layerName = name;
            //list assigned to a list type (?)
            if (inputVals != null)
            {
                inputValues = inputVals;

            }
            if(!Enum.TryParse(name, out layerType))
            {
                //add a check for this? up one class
                errorMsg = "incorrect layer name";
            }
            if(layerType==LayerType.Hidden)
            {
                activeLayer=true;
            }
            else if(layerType==LayerType.Context)
            {
                InitialiseContextInputs();
            }
            //create nodes
            for(int i=0;i<nodesNo;i++)
            {
                if (inputLayers != null)
                {
                    Node newNode = new Node(activeLayer,inputLayers);
                    nodes.Add(newNode);
                    inputLayer = inputLayers;
                    //inputValues = inputLayers.ElementAt(1).inputValues;
                }
                else
                {
                    Node newNode = new Node(activeLayer);
                    nodes.Add(newNode);
                }
            }


        }

        public void InitialiseContextInputs()
        {
            for(int i=0; i<nodesNo;i++)
            {
                inputValues.Add(0.5f);
            }
        }

        public List<float> GetNodesValues()
        {
            List<float> vals = new List<float>();
            for(int i=0;i<nodes.Count;i++)
            {
                vals.Add(nodes.ElementAt(i).NodeValue);
            }

            return vals;
        }

        public void getActivationForNodes()
        {
            if (activeLayer)
            {
                activationValues.Clear();
                for (int i = 0; i < nodes.Count; i++)
                {
                    nodes.ElementAt(i).activateNode(inputValues,inputLayer);
                    activationValues.Add(nodes.ElementAt(i).GetActivationVal);
                }
            }
        }

        public float calculateSigmoidValue(float x, int lambda)
        {
            float activationValTmp;
            //activation equation
            double val = Convert.ToDouble(x);
            return activationValTmp = (float)(1 / (1 + Math.Exp(-lambda*val)));
        }

        public float calculateActivationDerivative(float x)
        {
            return x * (1 - x);
        }

        public void addToInputLayer(Layer l)
        {
            inputLayer.Add(l);
        }

        public void setNodeValues(List<float>values)
        {
            for(int i=0; i<nodes.Count;i++)
            {
                nodes.ElementAt(i).NodeValue = values.ElementAt(i);
            }
        }

        public List<float> GetActivationValues
        {
            get { return activationValues; }
        }

        public List<float> SetInputValues
        {
            //may need to loop through the list
            set { inputValues=value; }
        }

        public List<Node> GetNodes
        {
            get { return nodes; }
        }

        public LayerType LayerType
        {
            get { return layerType; }
        }

        public string LayerName
        {
            get { return layerName; }
        }

    }
}
