﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RNN.Model;

namespace RNN
{
    public partial class ManageNet : Form
    {
        ElmanNet net;
        IElmanNet network;
        public event EventHandler<EncogNetUpdatedEventArgs> netUpdated;

        public ManageNet()
        {
            InitializeComponent();
        }

        public ManageNet(ElmanNet _net)
        {
            net = _net;
            InitializeComponent();
            txtNetName.Text = net.NetName;
        }

        public ManageNet(IElmanNet _net)
        {
            network = _net;
            InitializeComponent();
            txtNetName.Text = network.NetName;
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {

            //if(!txtNetName.Text.Equals(net.NetName))
            //{
            //    net.NetName = txtNetName.Text;
            //}

            //NetUpdatedEventArgs netUpdatedEA = new NetUpdatedEventArgs();
            //netUpdatedEA.Net = net;
            //OnNetUpdated(netUpdatedEA);

            if (!txtNetName.Text.Equals(network.NetName))
            {
                network.NetName = txtNetName.Text;
            }

            EncogNetUpdatedEventArgs netUpdatedEA = new EncogNetUpdatedEventArgs();
            netUpdatedEA.Net = network;
            OnNetUpdated(netUpdatedEA);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public virtual void OnNetUpdated(EncogNetUpdatedEventArgs e)
        {
            if (netUpdated != null)
            {
                netUpdated(this, e);
            }
        }
       
    }
}
