﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using RNN.NamedPipes;
using RNN.Model;

namespace RNN
{
    public partial class PipeCommWithYARP : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        bool listen = false;
        NamedPipeServer PServer1 = new NamedPipeServer(@"\\.\pipe\myNamedPipe1", 0);
        NamedPipeServer PServer2 = new NamedPipeServer(@"\\.\pipe\myNamedPipe2", 1);

        bool send = true;

        IElmanNet net;
        string[] allInputs;
        string[] netInputs;
        string[] outputs;
        string input="";

        bool serverRunning=false;
        ExperimentType experimentType;
        bool startNewExperiment = true;
        int counterExp = 0;
        int maxIndex;

        public PipeCommWithYARP(IElmanNet _net)
        {
            InitializeComponent();
            net = _net;
            btnStop.Enabled = false;

            string[] str=Enum.GetNames(ExperimentType.Count_10.GetType());
            for (int i=0; i < str.Length; i++)
            {
                cbExperiments.Items.Add(str[i].Replace("_"," "));
            }
        }

        private void btnStartPipeServer_Click(object sender, EventArgs e)
        {
            //AllocConsole();
            
            PServer1.Start();
            PServer2.Start();
            lblState.Text = lblState.Text.Replace("Not Running", "Running");
            btnStartPipeServer.Enabled = false;
            btnStop.Enabled = true;
            serverRunning = true;
            gbCommToolbox.Enabled = true;
            
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string Ms = "Start";
            string prev = "Start";
            //do
            //{
                //if (!listen)
                //{
                    //Console.WriteLine("Enter the message");
                    //Ms = Console.ReadLine();
                    //if (txtMsg.Text != null && txtMsg.Text != prev)
                    //{
                    //    prev = txtMsg.Text;
                    //    PServer2.SendMessage(Ms, PServer2.clientse);
                    //    PServer1.StopServer();
                    //    listen = true;
                    //}
                    if (txtMsg.Text != null)
                    {
                        PServer2.SendMessage(txtMsg.Text, PServer2.clientse);
                        //PServer1.StopServer();
                        listen = true;
                    }
                //}
                //else
                //{
                    //PServer1.Start();
                   // listen = false;
                //}
            //} while (Ms != "quit");

            
        }

        private void btnReceive_Click(object sender, EventArgs e)
        {
            lblMsg.Text = PServer1.val;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            PServer1.StopServer();
            PServer2.StopServer();
            lblState.Text = lblState.Text.Replace("Running", "Not Running");
            btnStartPipeServer.Enabled = true;
            btnStop.Enabled = false;
            serverRunning = false;
            gbCommToolbox.Enabled = false;
            txtMsg.Text = "";
            lblMsg.Text = "";
        }

        private void btnTogglePipeComm_Click(object sender, EventArgs e)
        {
            
            if ((rbtnSeq.Checked || rbtnStatic.Checked)&&cbExperiments.SelectedIndex!=null)
            {
                //if (counterExp >= maxIndex - 1)
                //{
                   // startNewExperiment = true;
                    //counterExp = 0;

                //}
                //else
                //{
                    if (send)
                    {
                        //if (startNewExperiment)
                        //{
                           // startNewExperiment = false;
                            //PServer2.SendMessage(cbExperiments.SelectedIndex.ToString(), PServer2.clientse);
                            //counterExp = 0;

                        //}
                        //else
                        //{
                            btnTogglePipeComm.Text = "Receive Command";
                            send = false;
                            if (txtMsg.Text != null)
                            {
                                PServer2.SendMessage(txtMsg.Text, PServer2.clientse);
                                //PServer1.StopServer();
                                //listen = true;
                            }
                        //}
                    }
                    else
                    {
                        btnTogglePipeComm.Text = "Send Command";
                        send = true;
                        while (true)
                        {
                            lblMsg.Text = PServer1.val;
                            if (PServer1.val != "")
                            {
                                break;
                            }
                        }
                        if (input == "")
                        {
                            input = lblMsg.Text;
                        }
                        allInputs = lblMsg.Text.Split(',');
                        if (netInputs[0] == null || netInputs.Length == 0)
                        {
                            for (int i = 0; i < netInputs.Length; i++)
                            {
                                netInputs[i] = allInputs[i];
                            }

                            net.CalculateOutVal(netInputs, ref outputs, (rbtnStatic.Checked ? true : false));
                        }
                        lblMsg.Text = PServer1.val;
                        PServer1.val = "";

                        //string outVal = net.CalculateSingleOutVal(Convert.ToDouble(netInputs[Convert.ToInt32(allInputs[10])])).ToString();
                        //outVal = outVal.Replace("[", "");
                        //outVal = outVal.Replace("]", "");

                        //double test=Convert.ToDouble(outVal);
                        //decimal test1 = Convert.ToDecimal(outVal);
                        //test1=decimal.Round(test1);
                        //txtMsg.Text= Math.Ceiling(Convert.ToDouble(outVal)).ToString();

                        //txtMsg.Text = decimal.Round(Convert.ToDecimal(outVal), 0, MidpointRounding.AwayFromZero).ToString();

                        //use outputs list and change the function to get input passed through pipe communication
                        for (int i = 0; i < outputs.Length; i++)
                        {
                            outputs[i] = outputs[i].Replace("[", "");
                            outputs[i] = outputs[i].Replace("]", "");

                        }
                        txtMsg.Text = decimal.Round(Convert.ToDecimal(outputs[Convert.ToInt32(allInputs[maxIndex-1])]), 0, MidpointRounding.AwayFromZero).ToString();
                        counterExp = Convert.ToInt32(outputs[Convert.ToInt32(allInputs[maxIndex-1])]);
                    }
                //}
            }
            else
            {
                MessageBox.Show("Select input type the network uses.", "", MessageBoxButtons.OK);
            }
        }

        private void btnGetNetOutput_Click(object sender, EventArgs e)
        {
            //net.CalculateOutVal(ref outputs);
            //net.CalculateSingleOutVal();
            startNewExperiment = true;
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (serverRunning)
            {
                PServer1.StopServer();
                PServer2.StopServer();
            }
        }

        private void experiments_DropDownClosed(object sender, EventArgs e)
        {
            this.BeginInvoke(new Action(() => { cbExperiments.Select(0, 0); }));
        }

        private void cbExperiment_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbExperiments.SelectedIndex)
            {
        //            Count_5,
        //Count_10,
        //Count_5_With_Gestures,
        //Count_10_With_Gestures
                case 0:
                    experimentType = ExperimentType.Count_5;
                    allInputs = new string[11];
                    netInputs = new string[10];
                    outputs = new string[10];
                    maxIndex = 10;
                    break;
                case 1:
                    experimentType = ExperimentType.Count_10;
                    allInputs = new string[21];
                    netInputs = new string[20];
                    outputs = new string[20];
                    maxIndex = 20;
                    break;
                case 2:
                    experimentType = ExperimentType.Count_5_With_Gestures;
                    allInputs = new string[21];
                    netInputs = new string[20];
                    outputs = new string[20];
                    maxIndex = 20;
                    break;
                case 3:
                    experimentType = ExperimentType.Count_10_With_Gestures;
                    allInputs = new string[41];
                    netInputs = new string[40];
                    outputs = new string[40];
                    maxIndex = 40;
                    break;
                default:
                    break;
            }
        }

    }
}
