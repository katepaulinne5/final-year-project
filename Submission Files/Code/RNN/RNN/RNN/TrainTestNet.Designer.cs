﻿namespace RNN
{
    partial class TrainTestNet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.nudLambda = new System.Windows.Forms.NumericUpDown();
            this.nudLearningRate = new System.Windows.Forms.NumericUpDown();
            this.gbAdjustParams = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtnAdjustParams = new System.Windows.Forms.RadioButton();
            this.btnSaveResults = new System.Windows.Forms.Button();
            this.gbTrainNet = new System.Windows.Forms.GroupBox();
            this.btnTrainCancel = new System.Windows.Forms.Button();
            this.lblErrorFinal = new System.Windows.Forms.Label();
            this.lblErrorMin = new System.Windows.Forms.Label();
            this.btnTrainNet = new System.Windows.Forms.Button();
            this.plotTrainError = new OxyPlot.WindowsForms.PlotView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnStatic = new System.Windows.Forms.RadioButton();
            this.rbtnSequential = new System.Windows.Forms.RadioButton();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTestDataPath = new System.Windows.Forms.TextBox();
            this.nudInput2 = new System.Windows.Forms.NumericUpDown();
            this.nudInput3 = new System.Windows.Forms.NumericUpDown();
            this.nudInput4 = new System.Windows.Forms.NumericUpDown();
            this.nudInput5 = new System.Windows.Forms.NumericUpDown();
            this.nudInput6 = new System.Windows.Forms.NumericUpDown();
            this.nudInput1 = new System.Windows.Forms.NumericUpDown();
            this.lblOutputSeq = new System.Windows.Forms.Label();
            this.btnTestSequence = new System.Windows.Forms.Button();
            this.cbContinuous = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudLambda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLearningRate)).BeginInit();
            this.gbAdjustParams.SuspendLayout();
            this.gbTrainNet.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(158, 158);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(146, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Confirm Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // nudLambda
            // 
            this.nudLambda.Location = new System.Drawing.Point(158, 41);
            this.nudLambda.Name = "nudLambda";
            this.nudLambda.Size = new System.Drawing.Size(69, 22);
            this.nudLambda.TabIndex = 2;
            // 
            // nudLearningRate
            // 
            this.nudLearningRate.DecimalPlaces = 1;
            this.nudLearningRate.Location = new System.Drawing.Point(158, 82);
            this.nudLearningRate.Name = "nudLearningRate";
            this.nudLearningRate.Size = new System.Drawing.Size(69, 22);
            this.nudLearningRate.TabIndex = 3;
            // 
            // gbAdjustParams
            // 
            this.gbAdjustParams.Controls.Add(this.label2);
            this.gbAdjustParams.Controls.Add(this.label1);
            this.gbAdjustParams.Controls.Add(this.nudLambda);
            this.gbAdjustParams.Controls.Add(this.btnSave);
            this.gbAdjustParams.Controls.Add(this.rbtnAdjustParams);
            this.gbAdjustParams.Controls.Add(this.nudLearningRate);
            this.gbAdjustParams.Location = new System.Drawing.Point(12, 12);
            this.gbAdjustParams.Name = "gbAdjustParams";
            this.gbAdjustParams.Size = new System.Drawing.Size(324, 206);
            this.gbAdjustParams.TabIndex = 4;
            this.gbAdjustParams.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Learning Rate:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lambda:";
            // 
            // rbtnAdjustParams
            // 
            this.rbtnAdjustParams.AutoSize = true;
            this.rbtnAdjustParams.Checked = true;
            this.rbtnAdjustParams.Location = new System.Drawing.Point(13, 0);
            this.rbtnAdjustParams.Name = "rbtnAdjustParams";
            this.rbtnAdjustParams.Size = new System.Drawing.Size(291, 21);
            this.rbtnAdjustParams.TabIndex = 5;
            this.rbtnAdjustParams.TabStop = true;
            this.rbtnAdjustParams.Text = "Adjust training parameters enable/disable";
            this.rbtnAdjustParams.UseVisualStyleBackColor = true;
            this.rbtnAdjustParams.CheckedChanged += new System.EventHandler(this.rbtnAdjustParams_CheckedChanged);
            this.rbtnAdjustParams.Click += new System.EventHandler(this.rbtnAdjustParams_Click);
            // 
            // btnSaveResults
            // 
            this.btnSaveResults.Location = new System.Drawing.Point(609, 478);
            this.btnSaveResults.Name = "btnSaveResults";
            this.btnSaveResults.Size = new System.Drawing.Size(101, 23);
            this.btnSaveResults.TabIndex = 7;
            this.btnSaveResults.Text = "Close";
            this.btnSaveResults.UseVisualStyleBackColor = true;
            this.btnSaveResults.Click += new System.EventHandler(this.btnSaveResults_Click);
            // 
            // gbTrainNet
            // 
            this.gbTrainNet.Controls.Add(this.btnTrainCancel);
            this.gbTrainNet.Controls.Add(this.lblErrorFinal);
            this.gbTrainNet.Controls.Add(this.lblErrorMin);
            this.gbTrainNet.Controls.Add(this.btnTrainNet);
            this.gbTrainNet.Controls.Add(this.plotTrainError);
            this.gbTrainNet.Location = new System.Drawing.Point(12, 224);
            this.gbTrainNet.Name = "gbTrainNet";
            this.gbTrainNet.Size = new System.Drawing.Size(698, 248);
            this.gbTrainNet.TabIndex = 8;
            this.gbTrainNet.TabStop = false;
            this.gbTrainNet.Text = "Train Network";
            // 
            // btnTrainCancel
            // 
            this.btnTrainCancel.Enabled = false;
            this.btnTrainCancel.Location = new System.Drawing.Point(394, 173);
            this.btnTrainCancel.Name = "btnTrainCancel";
            this.btnTrainCancel.Size = new System.Drawing.Size(142, 30);
            this.btnTrainCancel.TabIndex = 12;
            this.btnTrainCancel.Text = "Cancel Training";
            this.btnTrainCancel.UseVisualStyleBackColor = true;
            this.btnTrainCancel.Visible = false;
            this.btnTrainCancel.Click += new System.EventHandler(this.btnTrainCancel_Click);
            // 
            // lblErrorFinal
            // 
            this.lblErrorFinal.AutoSize = true;
            this.lblErrorFinal.Location = new System.Drawing.Point(194, 225);
            this.lblErrorFinal.Name = "lblErrorFinal";
            this.lblErrorFinal.Size = new System.Drawing.Size(73, 17);
            this.lblErrorFinal.TabIndex = 11;
            this.lblErrorFinal.Text = "final error:";
            // 
            // lblErrorMin
            // 
            this.lblErrorMin.AutoSize = true;
            this.lblErrorMin.Location = new System.Drawing.Point(10, 222);
            this.lblErrorMin.Name = "lblErrorMin";
            this.lblErrorMin.Size = new System.Drawing.Size(73, 17);
            this.lblErrorMin.TabIndex = 10;
            this.lblErrorMin.Text = "min error: ";
            // 
            // btnTrainNet
            // 
            this.btnTrainNet.Location = new System.Drawing.Point(567, 209);
            this.btnTrainNet.Name = "btnTrainNet";
            this.btnTrainNet.Size = new System.Drawing.Size(125, 30);
            this.btnTrainNet.TabIndex = 0;
            this.btnTrainNet.Text = "Train Network";
            this.btnTrainNet.UseVisualStyleBackColor = true;
            this.btnTrainNet.Click += new System.EventHandler(this.btnTrainNet_Click);
            // 
            // plotTrainError
            // 
            this.plotTrainError.Location = new System.Drawing.Point(5, 15);
            this.plotTrainError.Name = "plotTrainError";
            this.plotTrainError.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plotTrainError.Size = new System.Drawing.Size(687, 200);
            this.plotTrainError.TabIndex = 9;
            this.plotTrainError.Text = "Training Error Plot";
            this.plotTrainError.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plotTrainError.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plotTrainError.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbContinuous);
            this.groupBox1.Controls.Add(this.rbtnStatic);
            this.groupBox1.Controls.Add(this.rbtnSequential);
            this.groupBox1.Controls.Add(this.btnBrowse);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtTestDataPath);
            this.groupBox1.Controls.Add(this.nudInput2);
            this.groupBox1.Controls.Add(this.nudInput3);
            this.groupBox1.Controls.Add(this.nudInput4);
            this.groupBox1.Controls.Add(this.nudInput5);
            this.groupBox1.Controls.Add(this.nudInput6);
            this.groupBox1.Controls.Add(this.nudInput1);
            this.groupBox1.Controls.Add(this.lblOutputSeq);
            this.groupBox1.Controls.Add(this.btnTestSequence);
            this.groupBox1.Location = new System.Drawing.Point(342, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 206);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test Network";
            // 
            // rbtnStatic
            // 
            this.rbtnStatic.AutoSize = true;
            this.rbtnStatic.Location = new System.Drawing.Point(227, 105);
            this.rbtnStatic.Name = "rbtnStatic";
            this.rbtnStatic.Size = new System.Drawing.Size(99, 21);
            this.rbtnStatic.TabIndex = 12;
            this.rbtnStatic.TabStop = true;
            this.rbtnStatic.Text = "Static Input";
            this.rbtnStatic.UseVisualStyleBackColor = true;
            this.rbtnStatic.CheckedChanged += new System.EventHandler(this.rbtnStatic_CheckedChanged);
            // 
            // rbtnSequential
            // 
            this.rbtnSequential.AutoSize = true;
            this.rbtnSequential.Location = new System.Drawing.Point(28, 105);
            this.rbtnSequential.Name = "rbtnSequential";
            this.rbtnSequential.Size = new System.Drawing.Size(131, 21);
            this.rbtnSequential.TabIndex = 11;
            this.rbtnSequential.TabStop = true;
            this.rbtnSequential.Text = "Sequential Input";
            this.rbtnSequential.UseVisualStyleBackColor = true;
            this.rbtnSequential.CheckedChanged += new System.EventHandler(this.rbtnSequential_CheckedChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(287, 76);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 10;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Testing data path";
            // 
            // txtTestDataPath
            // 
            this.txtTestDataPath.Location = new System.Drawing.Point(149, 48);
            this.txtTestDataPath.Name = "txtTestDataPath";
            this.txtTestDataPath.Size = new System.Drawing.Size(213, 22);
            this.txtTestDataPath.TabIndex = 8;
            // 
            // nudInput2
            // 
            this.nudInput2.Location = new System.Drawing.Point(68, 21);
            this.nudInput2.Name = "nudInput2";
            this.nudInput2.Size = new System.Drawing.Size(41, 22);
            this.nudInput2.TabIndex = 7;
            this.nudInput2.Visible = false;
            // 
            // nudInput3
            // 
            this.nudInput3.Location = new System.Drawing.Point(115, 21);
            this.nudInput3.Name = "nudInput3";
            this.nudInput3.Size = new System.Drawing.Size(41, 22);
            this.nudInput3.TabIndex = 6;
            this.nudInput3.Visible = false;
            // 
            // nudInput4
            // 
            this.nudInput4.Location = new System.Drawing.Point(162, 21);
            this.nudInput4.Name = "nudInput4";
            this.nudInput4.Size = new System.Drawing.Size(41, 22);
            this.nudInput4.TabIndex = 5;
            this.nudInput4.Visible = false;
            // 
            // nudInput5
            // 
            this.nudInput5.Location = new System.Drawing.Point(209, 21);
            this.nudInput5.Name = "nudInput5";
            this.nudInput5.Size = new System.Drawing.Size(41, 22);
            this.nudInput5.TabIndex = 4;
            this.nudInput5.Visible = false;
            // 
            // nudInput6
            // 
            this.nudInput6.Location = new System.Drawing.Point(257, 21);
            this.nudInput6.Name = "nudInput6";
            this.nudInput6.Size = new System.Drawing.Size(41, 22);
            this.nudInput6.TabIndex = 3;
            this.nudInput6.Visible = false;
            // 
            // nudInput1
            // 
            this.nudInput1.Location = new System.Drawing.Point(21, 21);
            this.nudInput1.Name = "nudInput1";
            this.nudInput1.Size = new System.Drawing.Size(41, 22);
            this.nudInput1.TabIndex = 2;
            this.nudInput1.Visible = false;
            // 
            // lblOutputSeq
            // 
            this.lblOutputSeq.AutoSize = true;
            this.lblOutputSeq.Location = new System.Drawing.Point(25, 171);
            this.lblOutputSeq.Name = "lblOutputSeq";
            this.lblOutputSeq.Size = new System.Drawing.Size(61, 17);
            this.lblOutputSeq.TabIndex = 1;
            this.lblOutputSeq.Text = "(Output)";
            // 
            // btnTestSequence
            // 
            this.btnTestSequence.Location = new System.Drawing.Point(227, 158);
            this.btnTestSequence.Name = "btnTestSequence";
            this.btnTestSequence.Size = new System.Drawing.Size(135, 30);
            this.btnTestSequence.TabIndex = 0;
            this.btnTestSequence.Text = "Test ";
            this.btnTestSequence.UseVisualStyleBackColor = true;
            this.btnTestSequence.Click += new System.EventHandler(this.btnTestSequence_Click);
            // 
            // cbContinuous
            // 
            this.cbContinuous.AutoSize = true;
            this.cbContinuous.Enabled = false;
            this.cbContinuous.Location = new System.Drawing.Point(28, 132);
            this.cbContinuous.Name = "cbContinuous";
            this.cbContinuous.Size = new System.Drawing.Size(101, 21);
            this.cbContinuous.TabIndex = 13;
            this.cbContinuous.Text = "Continuous";
            this.cbContinuous.UseVisualStyleBackColor = true;
            this.cbContinuous.Visible = false;
            // 
            // TrainTestNet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 513);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbTrainNet);
            this.Controls.Add(this.btnSaveResults);
            this.Controls.Add(this.gbAdjustParams);
            this.Name = "TrainTestNet";
            this.Text = "TrainTestNet";
            ((System.ComponentModel.ISupportInitialize)(this.nudLambda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLearningRate)).EndInit();
            this.gbAdjustParams.ResumeLayout(false);
            this.gbAdjustParams.PerformLayout();
            this.gbTrainNet.ResumeLayout(false);
            this.gbTrainNet.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInput1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.NumericUpDown nudLambda;
        private System.Windows.Forms.NumericUpDown nudLearningRate;
        private System.Windows.Forms.GroupBox gbAdjustParams;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnAdjustParams;
        private System.Windows.Forms.Button btnSaveResults;
        private System.Windows.Forms.GroupBox gbTrainNet;
        private System.Windows.Forms.Button btnTrainNet;
        private OxyPlot.WindowsForms.PlotView plotTrainError;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudInput2;
        private System.Windows.Forms.NumericUpDown nudInput3;
        private System.Windows.Forms.NumericUpDown nudInput4;
        private System.Windows.Forms.NumericUpDown nudInput5;
        private System.Windows.Forms.NumericUpDown nudInput6;
        private System.Windows.Forms.NumericUpDown nudInput1;
        private System.Windows.Forms.Label lblOutputSeq;
        private System.Windows.Forms.Button btnTestSequence;
        private System.Windows.Forms.Label lblErrorFinal;
        private System.Windows.Forms.Label lblErrorMin;
        private System.Windows.Forms.Button btnTrainCancel;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTestDataPath;
        private System.Windows.Forms.RadioButton rbtnStatic;
        private System.Windows.Forms.RadioButton rbtnSequential;
        private System.Windows.Forms.CheckBox cbContinuous;
    }
}