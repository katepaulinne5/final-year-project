﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RNN
{
    public partial class SimPathManager : Form
    {
        public SimPathManager()
        {
            InitializeComponent();
            SimulatorPathManager.RetrieveFromRegistry();
            setSavedTxt();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(txtYARPPath.Text))
            {
                SimulatorPathManager.GetSetPath_YARP = txtYARPPath.Text;
            }
            if (!string.IsNullOrEmpty(txtICubPath.Text))
            {
                SimulatorPathManager.GetSetPath_ICub = txtICubPath.Text;
            }
            if (!string.IsNullOrEmpty(txtYarpRobotInterfacePath.Text))
            {
                SimulatorPathManager.GetSetLineArgs_YARP = txtYARPLineArgs.Text;
            }
            if (!string.IsNullOrEmpty(txtIKinCartesianSolverPath.Text))
            {
                SimulatorPathManager.GetSetLineArgs_YARP = txtYARPLineArgs.Text;
            }
            if (!string.IsNullOrEmpty(txtYARPLineArgs.Text))
            {
                SimulatorPathManager.GetSetLineArgs_YARP = txtYARPLineArgs.Text;
            }
            if (!string.IsNullOrEmpty(txtYARPLineArgs.Text))
            {
                SimulatorPathManager.GetSetLineArgs_YARP = txtYARPLineArgs.Text;
            }
            if (!string.IsNullOrEmpty(txtYARPLineArgs.Text))
            {
                SimulatorPathManager.GetSetLineArgs_YARP = txtYARPLineArgs.Text;
            }

            SimulatorPathManager.SaveToRegistry();
        }

        private void setSavedTxt()
        {
            txtYARPPath.Text = SimulatorPathManager.GetSetPath_YARP;
            txtICubPath.Text = SimulatorPathManager.GetSetPath_ICub;
            txtYarpRobotInterfacePath.Text = SimulatorPathManager.GetSetPath_YarpRobotInterface;
            txtIKinCartesianSolverPath.Text = SimulatorPathManager.GetSetPath_IKinCartesianSolver;
            txtYARPLineArgs.Text = SimulatorPathManager.GetSetLineArgs_YARP;
            txtYarpRobotInterfaceLineArgs.Text = SimulatorPathManager.GetSetLineArgs_YarpRobotInterface;
            txtIKinCartesianSolverLineArgs.Text = SimulatorPathManager.GetSetLineArgs_IKinCartesianSolver;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
