﻿namespace RNN
{
    partial class CreateNewNet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreateNet = new System.Windows.Forms.Button();
            this.txtNetName = new System.Windows.Forms.TextBox();
            this.lblNetName = new System.Windows.Forms.Label();
            this.lblHiddenNodesNo = new System.Windows.Forms.Label();
            this.txtHiddenNodesNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrainingFilePath = new System.Windows.Forms.TextBox();
            this.rbtnElmanNet = new System.Windows.Forms.RadioButton();
            this.rbtnEncogNet = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbContinuousSeq = new System.Windows.Forms.CheckBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbIncGestures = new System.Windows.Forms.CheckBox();
            this.lblOutputNodes = new System.Windows.Forms.Label();
            this.txtOuputNodes = new System.Windows.Forms.TextBox();
            this.lblSeq = new System.Windows.Forms.Label();
            this.txtNodesNo = new System.Windows.Forms.TextBox();
            this.rbtnSeq = new System.Windows.Forms.RadioButton();
            this.rbtnStatic = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(295, 464);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(104, 26);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCreateNet
            // 
            this.btnCreateNet.Location = new System.Drawing.Point(174, 464);
            this.btnCreateNet.Name = "btnCreateNet";
            this.btnCreateNet.Size = new System.Drawing.Size(115, 26);
            this.btnCreateNet.TabIndex = 1;
            this.btnCreateNet.Text = "Create Network";
            this.btnCreateNet.UseVisualStyleBackColor = true;
            this.btnCreateNet.Click += new System.EventHandler(this.btnCreateNet_Click);
            // 
            // txtNetName
            // 
            this.txtNetName.Location = new System.Drawing.Point(203, 36);
            this.txtNetName.Name = "txtNetName";
            this.txtNetName.Size = new System.Drawing.Size(172, 22);
            this.txtNetName.TabIndex = 2;
            // 
            // lblNetName
            // 
            this.lblNetName.AutoSize = true;
            this.lblNetName.Location = new System.Drawing.Point(93, 39);
            this.lblNetName.Name = "lblNetName";
            this.lblNetName.Size = new System.Drawing.Size(104, 17);
            this.lblNetName.TabIndex = 3;
            this.lblNetName.Text = "Network Name:";
            // 
            // lblHiddenNodesNo
            // 
            this.lblHiddenNodesNo.AutoSize = true;
            this.lblHiddenNodesNo.Location = new System.Drawing.Point(29, 67);
            this.lblHiddenNodesNo.Name = "lblHiddenNodesNo";
            this.lblHiddenNodesNo.Size = new System.Drawing.Size(168, 17);
            this.lblHiddenNodesNo.TabIndex = 4;
            this.lblHiddenNodesNo.Text = "Number of hidden nodes:";
            // 
            // txtHiddenNodesNo
            // 
            this.txtHiddenNodesNo.Location = new System.Drawing.Point(203, 67);
            this.txtHiddenNodesNo.Name = "txtHiddenNodesNo";
            this.txtHiddenNodesNo.Size = new System.Drawing.Size(172, 22);
            this.txtHiddenNodesNo.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Training file path:";
            // 
            // txtTrainingFilePath
            // 
            this.txtTrainingFilePath.Location = new System.Drawing.Point(203, 102);
            this.txtTrainingFilePath.Name = "txtTrainingFilePath";
            this.txtTrainingFilePath.Size = new System.Drawing.Size(172, 22);
            this.txtTrainingFilePath.TabIndex = 7;
            // 
            // rbtnElmanNet
            // 
            this.rbtnElmanNet.AutoSize = true;
            this.rbtnElmanNet.Location = new System.Drawing.Point(194, 14);
            this.rbtnElmanNet.Name = "rbtnElmanNet";
            this.rbtnElmanNet.Size = new System.Drawing.Size(94, 21);
            this.rbtnElmanNet.TabIndex = 8;
            this.rbtnElmanNet.TabStop = true;
            this.rbtnElmanNet.Text = "Elman Net";
            this.rbtnElmanNet.UseVisualStyleBackColor = true;
            // 
            // rbtnEncogNet
            // 
            this.rbtnEncogNet.AutoSize = true;
            this.rbtnEncogNet.Location = new System.Drawing.Point(194, 43);
            this.rbtnEncogNet.Name = "rbtnEncogNet";
            this.rbtnEncogNet.Size = new System.Drawing.Size(138, 21);
            this.rbtnEncogNet.TabIndex = 9;
            this.rbtnEncogNet.TabStop = true;
            this.rbtnEncogNet.Text = "Encog Elman Net";
            this.rbtnEncogNet.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Network type:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbtnEncogNet);
            this.groupBox1.Controls.Add(this.rbtnElmanNet);
            this.groupBox1.Location = new System.Drawing.Point(9, 193);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(372, 77);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(93, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "(max 50 nodes)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "New Network Details:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbContinuousSeq);
            this.groupBox2.Controls.Add(this.btnBrowse);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtTrainingFilePath);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.lblNetName);
            this.groupBox2.Controls.Add(this.txtNetName);
            this.groupBox2.Controls.Add(this.txtHiddenNodesNo);
            this.groupBox2.Controls.Add(this.lblHiddenNodesNo);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(387, 446);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            // 
            // cbContinuousSeq
            // 
            this.cbContinuousSeq.AutoSize = true;
            this.cbContinuousSeq.Location = new System.Drawing.Point(203, 166);
            this.cbContinuousSeq.Name = "cbContinuousSeq";
            this.cbContinuousSeq.Size = new System.Drawing.Size(105, 21);
            this.cbContinuousSeq.TabIndex = 16;
            this.cbContinuousSeq.Text = "Continuous ";
            this.cbContinuousSeq.UseVisualStyleBackColor = true;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(283, 130);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(92, 26);
            this.btnBrowse.TabIndex = 15;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbIncGestures);
            this.groupBox3.Controls.Add(this.lblOutputNodes);
            this.groupBox3.Controls.Add(this.txtOuputNodes);
            this.groupBox3.Controls.Add(this.lblSeq);
            this.groupBox3.Controls.Add(this.txtNodesNo);
            this.groupBox3.Controls.Add(this.rbtnSeq);
            this.groupBox3.Controls.Add(this.rbtnStatic);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(9, 276);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(372, 164);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // cbIncGestures
            // 
            this.cbIncGestures.AutoSize = true;
            this.cbIncGestures.Location = new System.Drawing.Point(196, 109);
            this.cbIncGestures.Name = "cbIncGestures";
            this.cbIncGestures.Size = new System.Drawing.Size(137, 21);
            this.cbIncGestures.TabIndex = 18;
            this.cbIncGestures.Text = "Include Gestures";
            this.cbIncGestures.UseVisualStyleBackColor = true;
            this.cbIncGestures.CheckedChanged += new System.EventHandler(this.incGestures_CheckedChanged);
            // 
            // lblOutputNodes
            // 
            this.lblOutputNodes.AutoSize = true;
            this.lblOutputNodes.Enabled = false;
            this.lblOutputNodes.Location = new System.Drawing.Point(20, 137);
            this.lblOutputNodes.Name = "lblOutputNodes";
            this.lblOutputNodes.Size = new System.Drawing.Size(170, 17);
            this.lblOutputNodes.TabIndex = 17;
            this.lblOutputNodes.Text = "Number of Output Nodes:";
            // 
            // txtOuputNodes
            // 
            this.txtOuputNodes.Location = new System.Drawing.Point(196, 134);
            this.txtOuputNodes.Name = "txtOuputNodes";
            this.txtOuputNodes.Size = new System.Drawing.Size(172, 22);
            this.txtOuputNodes.TabIndex = 16;
            // 
            // lblSeq
            // 
            this.lblSeq.AutoSize = true;
            this.lblSeq.Enabled = false;
            this.lblSeq.Location = new System.Drawing.Point(32, 84);
            this.lblSeq.Name = "lblSeq";
            this.lblSeq.Size = new System.Drawing.Size(158, 17);
            this.lblSeq.TabIndex = 15;
            this.lblSeq.Text = "Number of Input Nodes:";
            // 
            // txtNodesNo
            // 
            this.txtNodesNo.Enabled = false;
            this.txtNodesNo.Location = new System.Drawing.Point(196, 81);
            this.txtNodesNo.Name = "txtNodesNo";
            this.txtNodesNo.Size = new System.Drawing.Size(172, 22);
            this.txtNodesNo.TabIndex = 15;
            // 
            // rbtnSeq
            // 
            this.rbtnSeq.AutoSize = true;
            this.rbtnSeq.Location = new System.Drawing.Point(196, 46);
            this.rbtnSeq.Name = "rbtnSeq";
            this.rbtnSeq.Size = new System.Drawing.Size(96, 21);
            this.rbtnSeq.TabIndex = 13;
            this.rbtnSeq.TabStop = true;
            this.rbtnSeq.Text = "Sequential";
            this.rbtnSeq.UseVisualStyleBackColor = true;
            this.rbtnSeq.CheckedChanged += new System.EventHandler(this.seq_CheckedChanged);
            this.rbtnSeq.Click += new System.EventHandler(this.seq_Clicked);
            // 
            // rbtnStatic
            // 
            this.rbtnStatic.AutoSize = true;
            this.rbtnStatic.Location = new System.Drawing.Point(196, 19);
            this.rbtnStatic.Name = "rbtnStatic";
            this.rbtnStatic.Size = new System.Drawing.Size(64, 21);
            this.rbtnStatic.TabIndex = 12;
            this.rbtnStatic.TabStop = true;
            this.rbtnStatic.Text = "Static";
            this.rbtnStatic.UseVisualStyleBackColor = true;
            this.rbtnStatic.CheckedChanged += new System.EventHandler(this.static_CheckedChanged);
            this.rbtnStatic.Click += new System.EventHandler(this.static_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Input Type:";
            // 
            // CreateNewNet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 502);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCreateNet);
            this.Controls.Add(this.btnCancel);
            this.Name = "CreateNewNet";
            this.Text = "Create New Network";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreateNet;
        private System.Windows.Forms.TextBox txtNetName;
        private System.Windows.Forms.Label lblNetName;
        private System.Windows.Forms.Label lblHiddenNodesNo;
        private System.Windows.Forms.TextBox txtHiddenNodesNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTrainingFilePath;
        private System.Windows.Forms.RadioButton rbtnElmanNet;
        private System.Windows.Forms.RadioButton rbtnEncogNet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbtnSeq;
        private System.Windows.Forms.RadioButton rbtnStatic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblSeq;
        private System.Windows.Forms.TextBox txtNodesNo;
        private System.Windows.Forms.CheckBox cbIncGestures;
        private System.Windows.Forms.Label lblOutputNodes;
        private System.Windows.Forms.TextBox txtOuputNodes;
        private System.Windows.Forms.CheckBox cbContinuousSeq;
    }
}