#include <cstdio>
#include <yarp/os/Time.h>
using namespace std;
using namespace yarp::os;

int main()
{
	printf("starting the app\n");
	int times=10;

	while(times--)
	{
		printf("iCub\n");
		Time::delay(0.5);
	}
	printf("program ending");
}