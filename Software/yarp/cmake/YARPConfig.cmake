# Copyright: (C) 2009, 2010 RobotCub Consortium
# Authors: Paul Fitzpatrick, Giorgio Metta
# CopyPolicy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT


# Version
set(YARP_VERSION_MAJOR "2")
set(YARP_VERSION_MINOR "3")
set(YARP_VERSION_PATCH "68")
set(YARP_VERSION_TWEAK "")
set(YARP_VERSION "2.3.68~dev")
set(YARP_VERSION_SHORT "2.3.68")
set(YARP_VERSION_ABI "1")



####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was YARPConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

####################################################################################

# Give the details of YARP libraries, include path etc.
set(YARP_LIBRARIES "YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math;YARP::YARP_dev;YARP::YARP_name;YARP::YARP_init")
set(YARP_INCLUDE_DIRS "${PACKAGE_PREFIX_DIR}/include")
set(YARP_DEFINES "-D_REENTRANT") # only needed if you use ACE directly
set_and_check(YARP_BINDINGS "${PACKAGE_PREFIX_DIR}/share/yarp/bindings")

# Check if deprecated methods are built
set(YARP_NO_DEPRECATED OFF)
if(YARP_NO_DEPRECATED)
  add_definitions("-DYARP_NO_DEPRECATED")
endif(YARP_NO_DEPRECATED)

# Disable deprecated warnings, but add an option to enable it
include(CMakeDependentOption)
# FIXME 2.4 Workaround to reset this value to its default, remove it
#           at some point.
if(NOT YARP_NO_DEPRECATED_WARNINGS_DEFAULT_UPDATED)
  unset(YARP_NO_DEPRECATED_WARNINGS CACHE)
  set(YARP_NO_DEPRECATED_WARNINGS_DEFAULT_UPDATED TRUE CACHE BOOL "Workaround to reset this value to its default" INTERNAL)
endif()
cmake_dependent_option(YARP_NO_DEPRECATED_WARNINGS
                       "Do not warn when using YARP deprecated declarations" FALSE
                       "NOT YARP_NO_DEPRECATED" FALSE)
mark_as_advanced(YARP_NO_DEPRECATED_WARNINGS)
if(YARP_NO_DEPRECATED_WARNINGS)
  add_definitions("-DYARP_NO_DEPRECATED_WARNINGS")
endif()

# CMake modules directories
set_and_check(YARP_MODULE_DIR "${PACKAGE_PREFIX_DIR}/share/yarp/cmake")
set(YARP_MODULE_PATH "${YARP_MODULE_DIR}")
if(NOT YARP_NO_DEPRECATED)
  list(APPEND YARP_MODULE_PATH "${YARP_MODULE_DIR}/deprecated")

  # find_package(YCM) will overwrite the PACKAGE_PREFIX_DIR variable
  # set by PACKAGE_INIT therefore we save and restore it later
  set(_PACKAGE_PREFIX_DIR ${PACKAGE_PREFIX_DIR})
  find_package(YCM 0.2 QUIET)
  set(PACKAGE_PREFIX_DIR ${_PACKAGE_PREFIX_DIR})
  unset(_PACKAGE_PREFIX_DIR)

  # Workaround to check YCM version until a better versioning system is in use
  if(YCM_FOUND  AND
     YCM_VERSION_API VERSION_EQUAL 0.2  AND
     YCM_VERSION_SOURCE VERSION_LESS 20161114.8)
    set(YCM_FOUND FALSE)
    set(YARP_MODULE_PATH "${YARP_MODULE_DIR}")
  endif()

  if(NOT YCM_FOUND)
    list(APPEND YARP_MODULE_PATH "${YARP_MODULE_DIR}/ycm-0.2/find-modules"
                                 "${YARP_MODULE_DIR}/ycm-0.2/modules"
                                 "${YARP_MODULE_DIR}/ycm-0.2/3rdparty"
                                 "${YARP_MODULE_DIR}/ycm-0.2/cmake-proposed")
    if(${CMAKE_VERSION} VERSION_LESS 3.1)
      list(APPEND YARP_MODULE_PATH "${YARP_MODULE_DIR}/ycm-0.2/cmake-3.1/Modules")
    endif()
    if(${CMAKE_VERSION} VERSION_LESS 3.8)
      list(APPEND YARP_MODULE_PATH "${YARP_MODULE_DIR}/ycm-0.2/cmake-next/Modules")
    endif()
  endif()
endif()

# Install prefix
set(YARP_INSTALL_PREFIX "${PACKAGE_PREFIX_DIR}")

# Install directories (relative path)
set(YARP_DATA_INSTALL_DIR "share/yarp")
set(YARP_CONFIG_INSTALL_DIR "share/yarp/config")
set(YARP_PLUGIN_MANIFESTS_INSTALL_DIR "share/yarp/plugins")
set(YARP_MODULES_INSTALL_DIR "share/yarp/modules")
set(YARP_APPLICATIONS_INSTALL_DIR "share/yarp/applications")
set(YARP_TEMPLATES_INSTALL_DIR "share/yarp/templates")
set(YARP_APPLICATIONS_TEMPLATES_INSTALL_DIR "share/yarp/templates/applications")
set(YARP_MODULES_TEMPLATES_INSTALL_DIR "share/yarp/templates/modules")
set(YARP_CONTEXTS_INSTALL_DIR "share/yarp/contexts")
set(YARP_ROBOTS_INSTALL_DIR "share/yarp/robots")
set(YARP_STATIC_PLUGINS_INSTALL_DIR "lib")
set(YARP_DYNAMIC_PLUGINS_INSTALL_DIR "lib/yarp")
set(YARP_QML2_IMPORT_DIR "lib/qt5/qml")

# Install directories (absolute path)
set_and_check(YARP_DATA_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp")
set(YARP_CONFIG_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/config")
set(YARP_PLUGIN_MANIFESTS_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/plugins")
set(YARP_MODULES_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/modules")
set(YARP_APPLICATIONS_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/applications")
set(YARP_TEMPLATES_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/templates")
set(YARP_APPLICATIONS_TEMPLATES_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/templates/applications")
set(YARP_MODULES_TEMPLATES_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/templates/modules")
set(YARP_CONTEXTS_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/contexts")
set(YARP_ROBOTS_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/share/yarp/robots")
set(YARP_STATIC_PLUGINS_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/lib")
set(YARP_DYNAMIC_PLUGINS_INSTALL_DIR_FULL "${PACKAGE_PREFIX_DIR}/lib/yarp")
set(YARP_QML2_IMPORT_DIR_FULL "${PACKAGE_PREFIX_DIR}/lib/qt5/qml")

# Pass along some compilation options that may be useful for clients
# to check.
set(YARP_HAS_MATH_LIB TRUE)
set(YARP_HAS_NAME_LIB TRUE)

set(YARP_IS_SHARED_LIBRARY )

# Check if IDL is available
set(YARP_HAS_IDL TRUE)
set(YARP_IDL_BINARY_HINT "${PACKAGE_PREFIX_DIR}/bin")

if(NOT TARGET YARP::YARP_OS)
  include(${CMAKE_CURRENT_LIST_DIR}/YARPTargets.cmake)
endif()

# Export variables for available targets
set(YARP_OS_LIBRARY YARP::YARP_OS)
set(YARP_SIG_LIBRARY YARP::YARP_sig)
if(TARGET YARP::YARP_math)
  set(YARP_MATH_LIBRARY YARP::YARP_math)
endif()
set(YARP_DEV_LIBRARY YARP::YARP_dev)
if(TARGET YARP::YARP_name)
  set(YARP_NAME_LIBRARY YARP::YARP_name)
endif()
set(YARP_INIT_LIBRARY YARP::YARP_init)

# load IDL functions
include(${YARP_MODULE_DIR}/YarpIDL.cmake)
