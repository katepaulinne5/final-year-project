#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "YARP::YARP_priv_tinyxml" for configuration "Release"
set_property(TARGET YARP::YARP_priv_tinyxml APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_priv_tinyxml PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_priv_tinyxml.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_priv_tinyxml )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_priv_tinyxml "${_IMPORT_PREFIX}/lib/YARP_priv_tinyxml.lib" )

# Import target "YARP::YARP_priv_sqlite" for configuration "Release"
set_property(TARGET YARP::YARP_priv_sqlite APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_priv_sqlite PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_priv_sqlite.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_priv_sqlite )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_priv_sqlite "${_IMPORT_PREFIX}/lib/YARP_priv_sqlite.lib" )

# Import target "YARP::YARP_priv_qcustomplot" for configuration "Release"
set_property(TARGET YARP::YARP_priv_qcustomplot APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_priv_qcustomplot PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "Qt5::Widgets"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_priv_qcustomplot.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_priv_qcustomplot )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_priv_qcustomplot "${_IMPORT_PREFIX}/lib/YARP_priv_qcustomplot.lib" )

# Import target "YARP::YARP_priv_xmlrpcpp" for configuration "Release"
set_property(TARGET YARP::YARP_priv_xmlrpcpp APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_priv_xmlrpcpp PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_priv_xmlrpcpp.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_priv_xmlrpcpp )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_priv_xmlrpcpp "${_IMPORT_PREFIX}/lib/YARP_priv_xmlrpcpp.lib" )

# Import target "YARP::YARP_OS" for configuration "Release"
set_property(TARGET YARP::YARP_OS APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_OS PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_OS.lib"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_OS.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_OS )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_OS "${_IMPORT_PREFIX}/lib/YARP_OS.lib" "${_IMPORT_PREFIX}/bin/YARP_OS.dll" )

# Import target "YARP::YARP_sig" for configuration "Release"
set_property(TARGET YARP::YARP_sig APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_sig PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_sig.lib"
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "YARP::YARP_OS"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_sig.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_sig )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_sig "${_IMPORT_PREFIX}/lib/YARP_sig.lib" "${_IMPORT_PREFIX}/bin/YARP_sig.dll" )

# Import target "YARP::YARP_gsl" for configuration "Release"
set_property(TARGET YARP::YARP_gsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_gsl PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_gsl.lib"
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "YARP::YARP_sig"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_gsl.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_gsl )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_gsl "${_IMPORT_PREFIX}/lib/YARP_gsl.lib" "${_IMPORT_PREFIX}/bin/YARP_gsl.dll" )

# Import target "YARP::YARP_math" for configuration "Release"
set_property(TARGET YARP::YARP_math APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_math PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_math.lib"
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "YARP::YARP_OS;YARP::YARP_sig"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_math.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_math )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_math "${_IMPORT_PREFIX}/lib/YARP_math.lib" "${_IMPORT_PREFIX}/bin/YARP_math.dll" )

# Import target "YARP::YARP_dev" for configuration "Release"
set_property(TARGET YARP::YARP_dev APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_dev PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_dev.lib"
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "YARP::YARP_OS;YARP::YARP_sig;YARP::YARP_math"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_dev.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_dev )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_dev "${_IMPORT_PREFIX}/lib/YARP_dev.lib" "${_IMPORT_PREFIX}/bin/YARP_dev.dll" )

# Import target "YARP::YARP_name" for configuration "Release"
set_property(TARGET YARP::YARP_name APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_name PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_name.lib"
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "YARP::YARP_OS"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_name.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_name )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_name "${_IMPORT_PREFIX}/lib/YARP_name.lib" "${_IMPORT_PREFIX}/bin/YARP_name.dll" )

# Import target "YARP::YARP_serversql" for configuration "Release"
set_property(TARGET YARP::YARP_serversql APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_serversql PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "YARP::YARP_OS;YARP::YARP_init;YARP::YARP_name;YARP::YARP_priv_sqlite"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_serversql.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_serversql )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_serversql "${_IMPORT_PREFIX}/lib/YARP_serversql.lib" )

# Import target "YARP::YARP_manager" for configuration "Release"
set_property(TARGET YARP::YARP_manager APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_manager PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "YARP::YARP_sig;YARP::YARP_OS;YARP::YARP_priv_tinyxml;YARP::YARP_math"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_manager.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_manager )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_manager "${_IMPORT_PREFIX}/lib/YARP_manager.lib" )

# Import target "YARP::YARP_logger" for configuration "Release"
set_property(TARGET YARP::YARP_logger APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_logger PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "YARP::YARP_sig;YARP::YARP_OS;YARP::YARP_math"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/YARP_logger.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_logger )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_logger "${_IMPORT_PREFIX}/lib/YARP_logger.lib" )

# Import target "YARP::yarpmod" for configuration "Release"
set_property(TARGET YARP::yarpmod APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::yarpmod PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/yarpmod.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/yarpmod.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::yarpmod )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::yarpmod "${_IMPORT_PREFIX}/lib/yarpmod.lib" "${_IMPORT_PREFIX}/bin/yarpmod.dll" )

# Import target "YARP::yarpcar" for configuration "Release"
set_property(TARGET YARP::yarpcar APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::yarpcar PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/yarpcar.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/yarpcar.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::yarpcar )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::yarpcar "${_IMPORT_PREFIX}/lib/yarpcar.lib" "${_IMPORT_PREFIX}/bin/yarpcar.dll" )

# Import target "YARP::YARP_init" for configuration "Release"
set_property(TARGET YARP::YARP_init APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::YARP_init PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/YARP_init.lib"
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "YARP::YARP_OS"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/YARP_init.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::YARP_init )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::YARP_init "${_IMPORT_PREFIX}/lib/YARP_init.lib" "${_IMPORT_PREFIX}/bin/YARP_init.dll" )

# Import target "YARP::yarpidl_thrift" for configuration "Release"
set_property(TARGET YARP::yarpidl_thrift APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::yarpidl_thrift PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/yarpidl_thrift.exe"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::yarpidl_thrift )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::yarpidl_thrift "${_IMPORT_PREFIX}/bin/yarpidl_thrift.exe" )

# Import target "YARP::yarpidl_rosmsg" for configuration "Release"
set_property(TARGET YARP::yarpidl_rosmsg APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(YARP::yarpidl_rosmsg PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/yarpidl_rosmsg.exe"
  )

list(APPEND _IMPORT_CHECK_TARGETS YARP::yarpidl_rosmsg )
list(APPEND _IMPORT_CHECK_FILES_FOR_YARP::yarpidl_rosmsg "${_IMPORT_PREFIX}/bin/yarpidl_rosmsg.exe" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
