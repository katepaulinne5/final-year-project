﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Windows.Forms;

namespace RNN.NamedPipes
{
    //from tutorial example
    class NamedPipeServer
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern SafeFileHandle CreateNamedPipe(
            String pipeName,
            uint dwOpenMode,
            uint dwPipeMode,
            uint nMaxInstances,
            uint nOutBufferSize,
            uint nInBufferSize,
            uint nDefaultTimeOut,
            IntPtr lpSecurityAttributes);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern int ConnectNamedPipe(
           SafeFileHandle hNamedPipe,
           IntPtr lpOverlapped);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern int DisconnectNamedPipe(
           SafeFileHandle hNamedPipe);

        public const uint DUPLEX = (0x00000003);
        public const uint FILE_FLAG_OVERLAPPED = (0x40000000);

        public class Client
        {
            public SafeFileHandle handle;
            public FileStream stream;
        }

        public const int BUFFER_SIZE = 100;
        public Client clientse = null;

        public string pipeName;
        Thread listenThread;
        SafeFileHandle clientHandle;
        public int ClientType;
        //public Label label;
        public string val = "";
        Thread listenReadThread;

        public NamedPipeServer(string _name, int _mode/*, Label lbl*/)
        {
            pipeName = _name;
            ClientType = _mode;
            //label = lbl;
        }

        public void Start()
        {
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
        }

        private void ListenForClients()
        {
            while(true)
            {
                clientHandle = CreateNamedPipe(this.pipeName, DUPLEX | FILE_FLAG_OVERLAPPED, 0, 255, BUFFER_SIZE, BUFFER_SIZE, 0, IntPtr.Zero);
                if (clientHandle.IsInvalid)
                    return;

                int success = ConnectNamedPipe(clientHandle, IntPtr.Zero);

                if (success == 0)
                    return;

                clientse = new Client();
                clientse.handle = clientHandle;
                clientse.stream = new FileStream(clientse.handle, FileAccess.ReadWrite, BUFFER_SIZE, true);

                if(ClientType==0)
                {
                    Thread readThread = new Thread(new ThreadStart(Read));
                    readThread.Start();
                }
            }
        }

        public void startReadThread()
        {
            this.listenReadThread = new Thread(new ThreadStart(Read));
            this.listenReadThread.Start();
        }

        public void stopReadThread()
        {
            this.listenReadThread.Abort();
        }

        private void Read()
        {
            val = "";
            byte[] buffer = null;
            ASCIIEncoding encoder = new ASCIIEncoding();

            while(true)
            {
                int bytesRead = 0;

                try
                {
                    buffer = new byte[BUFFER_SIZE];
                    bytesRead = clientse.stream.Read(buffer, 0, BUFFER_SIZE);
                }
                catch
                {
                    break;
                }

                int ReadLength = 0;
                for(int i=0;i<BUFFER_SIZE;i++)
                {
                    if(buffer[i].ToString("x2")!="cc")
                    {
                        ReadLength++;
                    }
                    else
                    {
                        break;
                    }
                }
                if(ReadLength>0)
                {
                    byte[] Rc = new byte[ReadLength];
                    Buffer.BlockCopy(buffer, 0, Rc, 0, ReadLength);

                    //Console.WriteLine("C# App: Received " + ReadLength + " Bytes: " + encoder.GetString(Rc, 0, ReadLength));
                    //label.Text = encoder.GetString(Rc, 0, ReadLength);
                    val = encoder.GetString(Rc, 0, ReadLength);
                    // get value received and pass to form -add to queue
                    //encoder.GetString(Rc, 0, ReadLength));
                    buffer.Initialize();
                }
            }

            clientse.stream.Close();
            clientse.handle.Close();
        }

        public void SendMessage(string message, Client client)
        {
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] messageBuffer = encoder.GetBytes(message);

            if(client.stream.CanWrite)
            {
                client.stream.Write(messageBuffer, 0, messageBuffer.Length);
                client.stream.Flush();
            }
        }

        public void StopServer()
        {
            DisconnectNamedPipe(this.clientHandle);

            this.listenThread.Abort();
            //use resume and suspend to pause thread when waiting to send message
        }
    }
}
