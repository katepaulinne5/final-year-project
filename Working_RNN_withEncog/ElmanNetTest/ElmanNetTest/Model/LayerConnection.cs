﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNN.Model
{
    class LayerConnection
    {
        private Layer layerConBeg;
        private Layer layerConEnd;

        public LayerConnection(Layer layerBeg, Layer layerEnd)
        {
            layerConBeg = layerBeg;
            layerConEnd = layerEnd;
        }
    }
}
