﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElmanNetTest
{
    public enum LayerType
    {
        Input,
        Output,
        Hidden,
        Context
    }

    public enum WeightType
    {
        Input,
        Output
    }
}