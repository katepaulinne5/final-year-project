﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNN.Model
{
    public class Weight
    {
        private Node input;
        private float weight;

        public Weight(Node inputNode, float value)
        {
            input = inputNode;
            weight = value;
        }

        public Node Input
        {
            set { input = value; }
            get { return input; }
        }

        public float WeightVal
        {
            set { weight = value; }
            get { return weight; }
        }
    }
}
