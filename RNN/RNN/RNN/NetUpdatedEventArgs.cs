﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RNN.Model;

namespace RNN
{
    public class NetUpdatedEventArgs : EventArgs
    {
        public ElmanNet Net { get; set; }
    }
}
