﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElmanNetTest
{
    public class Node
    {
        private float nodeVal;
        private List<float> weightsVals = new List<float>();
        private float biasWeight;
        Random r = new Random();

        public Node()
        {

        }

        public Node(List<Layer> inputLayers)
        {

            for (int i = 0; i < inputLayers.Count; i++)
            {
                for (int j = 0; j < inputLayers.ElementAt(i).GetNodes.Count; j++)
                {
                    //Weight tmpWeight = new Weight(inputLayers.ElementAt(i).GetNodes.ElementAt(j), 0.0f);
                    //weights.Add(tmpWeight);
                    //gen random weight val
                    float value = GenerateRandomFloat();
                    weightsVals.Add(value);
                }
            }
        }

        private float GenerateRandomFloat()
        {
            double randomN;

            randomN = r.NextDouble() * 0.1 - 0.1;
            return (float)randomN;
        }

        public void ModifyWeight(int idx, float value)
        {
            weightsVals[idx] = value;
        }

        public float NodeValue
        {
            get { return nodeVal; }
            set { nodeVal = value; }
        }

        public float BiasNode
        {
            get { return biasWeight; }
            set { biasWeight = value; }
        }

        public List<float> WeightsVals
        {
            get { return weightsVals; }
        }
    }
}
