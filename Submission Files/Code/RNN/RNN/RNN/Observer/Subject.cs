﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RNN.Model;

namespace RNN.Observer
{
    public class Subject
    {
        ArrayList arrayObservers = new ArrayList();
        private delegate void NotifyHandler(ElmanNet _net);
        private event NotifyHandler _Notify;

        public Subject()
        {
            this._Notify += new NotifyHandler(Notify);
        }

        public void UpdateObserver(ElmanNet _net)
        {
            OnNotify(_net);
        }

        private void OnNotify(ElmanNet _net)
        {
            if (_Notify != null)
            {
                _Notify(_net);
            }
        }

        private void Notify(ElmanNet _net)
        {
            for (int i = 0; i < arrayObservers.Count; i++)
            {
                Observer observer = (Observer)arrayObservers[i];
                observer.Update(_net);
            }
        }

        public void RegisterObserver(Observer observer)
        {
            arrayObservers.Add(observer);
        }
        
    }
}
